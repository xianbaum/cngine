#include "cngine.h"

static CN_Options options;

static void init_options()
{
    options.mode = CN_MODE_WINDOWED;
    options.width = 320;
    options.height = 200;
    options.loggingLevel = CN_LOGGING_LEVEL_ALL;
}

int Test_CN_Options_FromCommandLineArgs()
{
    char *argv1[] = {"", "--cnmode", "1"};
    char *argv2[] = {"", "--cnmode", "20","--cnmodeflags", "21", "--cnwidth",
                    "500", "--cnheight", "600", "--cnloglevel", "2",
                    "--cnmusictype", "5"};
    CN_Options *opt = CN_Options_FromCommandLineArgs(3, argv1);
    CN_Assert(opt->mode == 1);
    CN_Options_Free(opt);
    opt = CN_Options_FromCommandLineArgs(13, argv2);
    CN_Assert(opt->mode == 20 && opt->modeFlags == 21 && opt->width == 500 &&
              opt->height == 600 && opt->loggingLevel == 2 &&
              opt->musicType == 5);
    CN_Options_Free(opt);
    return 0;
}

int Test_CN_Init_CN_Deinit()
{
    init_options();
    CN_Backend_Init(&options);
    CN_Deinit();
    return 0;
}
