#include "../cngine.h"
#include "../datastr/hashmap.h"
#include "../datastr/vlist.h"
#include "datastr.h"
#include "string.h"

int Test_CN_StringDictionary()
{
    CN_StringDictionary *dict = CN_StringDictionary_Create(NULL, 0);
    CN_StringDictionary_Put(dict, "key1", "val1");
    CN_StringDictionary_Put(dict, "key2", "val2");
    CN_StringDictionary_Put(dict, "key3", "val3");
    CN_StringDictionary_Put(dict, "key4", "val4");
    CN_StringDictionary_Put(dict, "key5", "val5");
    CN_StringDictionary_Put(dict, "key6", "val6");
    CN_StringDictionary_Put(dict, "key7", "val7");
    CN_StringDictionary_Put(dict, "key8", "val8");
    CN_StringDictionary_Put(dict, "key9", "val9");
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key1"), "val1") == 0);
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key2"), "val2") == 0);
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key3"), "val3") == 0);
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key4"), "val4") == 0);
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key5"), "val5") == 0);
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key6"), "val6") == 0);
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key7"), "val7") == 0);
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key8"), "val8") == 0);
    CN_Assert(strcmp(CN_StringDictionary_Get(dict, "key9"), "val9") == 0);
    CN_Assert(CN_StringDictionary_Count(dict) == 9);
    CN_Assert(CN_StringDictionary_Get(dict, "key0") == NULL);
    CN_StringDictionary_Remove(dict, "key6");
    CN_Assert(CN_StringDictionary_Get(dict, "key6") == NULL);
    CN_Assert(!CN_StringDictionary_Contains(dict, "key6"));
    CN_Assert(CN_StringDictionary_Count(dict) == 8);
    CN_StringDictionary_Free(dict);
    return 0;
}

int Test_CN_VectorList()
{
    CN_VectorList *vlist = CN_VectorList_Create(NULL, 0);
    int i;
    void *i1 = CN_malloc(sizeof(void*));
    void *i2 = CN_malloc(sizeof(void*));
    void *i3 = CN_malloc(sizeof(void*));
    void *i4 = CN_malloc(sizeof(void*));
    void *i5 = CN_malloc(sizeof(void*));
    void *i6 = CN_malloc(sizeof(void*));
    void *i7 = CN_malloc(sizeof(void*));
    void *i8 = CN_malloc(sizeof(void*));
    void *i9 = CN_malloc(sizeof(void*));
    void *i10 = CN_malloc(sizeof(void*));
    CN_VectorList_Add(vlist, i1);
    CN_VectorList_Add(vlist, i2);
    CN_VectorList_Add(vlist, i3);
    CN_VectorList_Add(vlist, i4);
    CN_VectorList_Add(vlist, i5);
    CN_VectorList_Add(vlist, i6);
    CN_VectorList_Add(vlist, i7);
    CN_VectorList_Add(vlist, i8);
    CN_VectorList_Add(vlist, i9);
    CN_VectorList_Add(vlist, i10);
    CN_Assert(CN_VectorList_Count(vlist) == 10);
    CN_Assert(CN_VectorList_IndexOf(vlist, i1) == 0);
    CN_Assert(CN_VectorList_Get(vlist, 0) == i1);
    CN_Assert(CN_VectorList_Get(vlist, 1) == i2);
    CN_Assert(CN_VectorList_Get(vlist, 2) == i3);
    CN_Assert(CN_VectorList_Get(vlist, 3) == i4);
    CN_Assert(CN_VectorList_Get(vlist, 4) == i5);
    CN_Assert(CN_VectorList_IndexOf(vlist, i5) == 4);
    CN_Assert(CN_VectorList_Get(vlist, 5) == i6);
    CN_Assert(CN_VectorList_Get(vlist, 6) == i7);
    CN_Assert(CN_VectorList_Get(vlist, 7) == i8);
    CN_Assert(CN_VectorList_Get(vlist, 8) == i9);
    CN_Assert(CN_VectorList_Get(vlist, 9) == i10);
    CN_Assert(CN_VectorList_IndexOf(vlist, i10) == 9);
    free(i5);
    i5 = malloc(sizeof(void*));
    CN_VectorList_Set(vlist, i5, 4);
    CN_Assert(CN_VectorList_Get(vlist, 4) == i5);
    CN_Assert(CN_VectorList_IndexOf(vlist, i5) == 4);
    CN_VectorList_Remove(vlist, 3);
    CN_Assert(CN_VectorList_IndexOf(vlist, i5) == 3);
    CN_Assert(CN_VectorList_Count(vlist) == 9);
    for(i = 0; i < CN_VectorList_Count(vlist); i++) {
        CN_free(CN_VectorList_Get(vlist, i));
    }
    CN_VectorList_Free(vlist);
    return 0;
}
