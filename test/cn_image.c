#include "cn_image.h"

/* Testing for CN_Image and related functions */

static CN_Options options;

static void init()
{
    options.mode = CN_MODE_WINDOWED;
    options.width = 320;
    options.height = 200;
    options.loggingLevel = CN_LOGGING_LEVEL_ALL;
    CN_Backend_Init(&options);
}

static void deinit()
{
    CN_Deinit();
}

int Test_CN_Image_Create()
{
    init(); {
        CN_Image *img = CN_Image_Create(3,3);
        int w = CN_Image_GetWidth(img),
            h = CN_Image_GetHeight(img),
                                  i, o;
        CN_Assert(w == 3 && h == 3);
        for(i = 0; i < w; i++) {
            for(o = 0; o < h; o++) {
                unsigned char px = CN_Image_GetPixel(img, i,o);
                CN_Assert(px == 0);
            }
        }
        CN_Image_Free(img);
    } deinit();
    return 0;
}

int Test_CN_Image_PutPixel()
{
    init(); {
        CN_Image *img = CN_Image_Create(3,3);
        int i, o;
        for(i = 0; i < CN_Image_GetWidth(img); i++) {
            for(o = 0; o < CN_Image_GetHeight(img); o++) {
                CN_Assert(CN_Image_GetPixel(img, i, o) == 0);
                CN_Image_PutPixel(img, i, o, 1);
                CN_Assert(CN_Image_GetPixel(img, i, o) == 1);
            }
        }
    } deinit();
    return 0;
}

int Test_CN_Image_Draw()
{
    init(); {
        CN_Image *screen = CN_Image_Create(5,5),
            *image = CN_Image_Create(3,3),
            *hugeImage = CN_Image_Create(1000,1000);
        unsigned char expected [] = {
            0,0,0,0,0,
            0,0,1,1,0,
            0,1,1,1,0,
            0,1,1,1,0,
            0,0,0,0,0};
        int i, o;
        for(i = 0; i < CN_Image_GetWidth(image); i++) {
            for(o = 0; o < CN_Image_GetHeight(image); o++) {
                CN_Image_PutPixel(image, i, o, 1);
            }
        }
        CN_Image_PutPixel(screen, 1, 1, 3);
        CN_Image_PutPixel(image, 0, 0, 0);
        CN_Image_Draw(image, screen, 1,1);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }

        CN_Image_Draw(hugeImage, screen, -100, -100);
        CN_Image_Free(screen);
        CN_Image_Free(image);
    } deinit();
    return 0;
}

int Test_CN_Image_DrawMasked()
{
    init(); {
        CN_Image *screen = CN_Image_Create(3,3),
            *image = CN_Image_Create(3,3);
        unsigned char expected [] = {
            1,1,1,
            1,2,1,
            1,1,1};
        int i, o;
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Image_PutPixel(screen, i, o, 1);
            }
        }
        CN_Image_PutPixel(image, 1, 1, 2);
        CN_Image_DrawMasked(image, screen, 0,0);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
        CN_Image_Free(screen);
        CN_Image_Free(image);
    } deinit();
    return 0;
}

int Test_CN_Image_DrawOffset()
{
    init(); {
        CN_Image *screen = CN_Image_Create(4,4),
            *image = CN_Image_Create(2,2);
        unsigned char expected [] = {
            0,0,0,0,
            0,2,2,0,
            0,2,2,0,
            0,0,0,0};
        int i, o;
        for(i = 0; i < CN_Image_GetWidth(image); i++) {
            for(o = 0; o < CN_Image_GetHeight(image); o++) {
                CN_Image_PutPixel(image, i, o, 1);
            }
        }
        CN_Image_DrawOffset(image, screen, 1, 1, 1);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
        CN_Image_Free(screen);
        CN_Image_Free(image);
    } deinit();
    return 0;
}

int Test_CN_Image_DrawMaskedOffset()
{
    init(); {
        CN_Image *screen = CN_Image_Create(4,4),
            *image = CN_Image_Create(2,2);
        unsigned char expected [] = {
            0,0,0,0,
            0,9,3,0,
            0,3,3,0,
            0,0,0,0};
        int i, o;
        for(i = 0; i < CN_Image_GetWidth(image); i++) {
            for(o = 0; o < CN_Image_GetHeight(image); o++) {
                CN_Image_PutPixel(image, i, o, 1);
            }
        }
        CN_Image_PutPixel(screen, 1, 1, 9);
        CN_Image_PutPixel(image, 0, 0, 0);
        CN_Image_DrawMaskedOffset(image, screen, 1, 1, 2);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
        CN_Image_Free(screen);
        CN_Image_Free(image);
    } deinit();
    return 0;
}

int Test_CN_Image_DrawRect()
{
    init(); {
        CN_Image *screen = CN_Image_Create(4,4),
            *image = CN_Image_Create(4,4);
        unsigned char expected [] = {
            0,0,0,0,
            0,5,9,0,
            0,6,10,0,
            0,0,0,0};
        int i, o, count = 0;
        for(i = 0; i < CN_Image_GetWidth(image); i++) {
            for(o = 0; o < CN_Image_GetHeight(image); o++) {
                CN_Image_PutPixel(image, i, o, count);
                count++;
            }
        }
        CN_Rect p;
        p.x = 1;
        p.y = 1;
        p.w = 2;
        p.h = 2;
        CN_Image_DrawRect(image, screen, p, 1, 1);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
        CN_Image_Free(screen);
        CN_Image_Free(image);
    } deinit();
    return 0;
}

int Test_CN_Image_DrawRectMasked()
{
    init(); {
        CN_Image *screen = CN_Image_Create(4,4),
            *image = CN_Image_Create(4,4);
        unsigned char expected [] = {
            0,0,0,0,
            0,5,9,0,
            0,6,1,0,
            0,0,0,0};
        int i, o, count = 0;
        for(i = 0; i < CN_Image_GetWidth(image); i++) {
            for(o = 0; o < CN_Image_GetHeight(image); o++) {
                CN_Image_PutPixel(image, i, o, count);
                count++;
            }
        }
        CN_Image_PutPixel(screen, 2, 2, 1);
        CN_Image_PutPixel(image, 2, 2, 0);
        CN_Rect p;
        p.x = 1;
        p.y = 1;
        p.w = 2;
        p.h = 2;
        CN_Image_DrawRectMasked(image, screen, p, 1, 1);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
        CN_Image_Free(screen);
        CN_Image_Free(image);
    } deinit();
    return 0;
}

int Test_CN_Image_DrawRectOffset()
{
    init(); {
        CN_Image *screen = CN_Image_Create(4,4),
            *image = CN_Image_Create(4,4);
        unsigned char expected [] = {
            0,0,0,0,
            0,6,10,0,
            0,7,1,0,
            0,0,0,0};
        int i, o, count = 0;
        for(i = 0; i < CN_Image_GetWidth(image); i++) {
            for(o = 0; o < CN_Image_GetHeight(image); o++) {
                CN_Image_PutPixel(image, i, o, count);
                count++;
            }
        }
        CN_Image_PutPixel(screen, 2, 2, 2);
        CN_Image_PutPixel(image, 2, 2, 0);
        CN_Rect p;
        p.x = 1;
        p.y = 1;
        p.w = 2;
        p.h = 2;
        CN_Image_DrawRectOffset(image, screen, p, 1, 1, 1);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
        CN_Image_Free(screen);
        CN_Image_Free(image);
    } deinit();
    return 0;
}

int Test_CN_Image_DrawRectMaskedOffset()
{
    init(); {
        CN_Image *screen = CN_Image_Create(4,4),
            *image = CN_Image_Create(4,4);
        unsigned char expected [] = {
            0,0,0,0,
            0,6,10,0,
            0,7,2,0,
            0,0,0,0};
        int i, o, count = 0;
        for(i = 0; i < CN_Image_GetWidth(image); i++) {
            for(o = 0; o < CN_Image_GetHeight(image); o++) {
                CN_Image_PutPixel(image, i, o, count);
                count++;
            }
        }
        CN_Image_PutPixel(screen, 2, 2, 2);
        CN_Image_PutPixel(image, 2, 2, 0);
        CN_Rect p;
        p.x = 1;
        p.y = 1;
        p.w = 2;
        p.h = 2;
        CN_Image_DrawRectMaskedOffset(image, screen, p, 1, 1, 1);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
        CN_Image_Free(screen);
        CN_Image_Free(image);
    } deinit();
    return 0;
}

int Test_CN_Image_DrawHLineNoGuard()
{
    unsigned char expected[] = {
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,9,10,11,12,13,14,
        0,0,0,0,0,0,0,0
    };
    init(); {
        CN_Image *screen = CN_Image_Create(8,4),
            *image = CN_Image_Create(8,4);
        int i, o;
        for(i=0; i<CN_Image_GetWidth(image)*CN_Image_GetHeight(image); i++) {
            CN_Image_PutPixel(image,
                              i%CN_Image_GetWidth(image),
                              i/CN_Image_GetWidth(image), i);
        }
        CN_Image_DrawHLineNoGuard(image, screen, 2, 2, 1, 1, 6);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
    } deinit();
    return 0;
}


int Test_CN_Image_DrawVLineNoGuard()
{
    unsigned char expected[] = {
        0,0,0,0,
        0,0,0,0,
        0,0,5,0,
        0,0,9,0,
        0,0,13,0,
        0,0,17,0,
        0,0,21,0,
        0,0,25,0
    };
    int i, o;
    init(); {
        CN_Image *screen = CN_Image_Create(4,8),
            *image = CN_Image_Create(4,8);
       for(i=0; i<CN_Image_GetWidth(image)*CN_Image_GetHeight(image); i++) {
            CN_Image_PutPixel(
                image,
                i%CN_Image_GetWidth(image),
                i/CN_Image_GetWidth(image), i);
        }
        CN_Image_DrawVLineNoGuard(image, screen, 2, 2, 1, 1, 6);
        for(i = 0; i < CN_Image_GetWidth(screen); i++) {
            for(o = 0; o < CN_Image_GetHeight(screen); o++) {
                CN_Assert(
                    expected[o*CN_Image_GetWidth(screen)+i] ==
                    CN_Image_GetPixel(screen, i, o));
            }
        }
 
    } deinit();
    return 0;
}

static int resetImage(CN_Image *image)
{
    int i;
    for(i=0; i<CN_Image_GetWidth(image)*CN_Image_GetHeight(image); i++) {
        CN_Image_PutPixel(
            image,
            i%CN_Image_GetWidth(image),
            i/CN_Image_GetWidth(image), 0);
    }
}

static int compareImageToChar(CN_Image *img, char **comp)
{

    unsigned char vals[] = "0123456789abcdef";
    int y, x;
    for(y = 0; y < CN_Image_GetHeight(img); y++) {
        for(x = 0; x < CN_Image_GetWidth(img); x++) {
            unsigned char pixel = CN_Image_GetPixel(img, x, y);
            unsigned char cur = comp[y][x];
            CN_Assert(cur == vals[pixel]);
        }
    }
}

int Test_CN_Image_DrawFlip()
{
    char *expected[10];
    int i, o;
    init(); {
        CN_Image *screen = CN_Image_Create(10,10),
            *image = CN_Image_Create(8,4);
       for(i=0; i<CN_Image_GetWidth(image)*CN_Image_GetHeight(image); i++) {
            CN_Image_PutPixel(
                image,
                i%CN_Image_GetWidth(image),
                i/CN_Image_GetWidth(image), i%16);
        }

       expected[0] = "0000000000";
       expected[1] = "0000000000";
       expected[2] = "0012345670";
       expected[3] = "089abcdef0";
       expected[4] = "0012345670";
       expected[5] = "089abcdef0";
       expected[6] = "0000000000";
       expected[7] = "0000000000";
       expected[8] = "0000000000";
       expected[9] = "0000000000";
       CN_Image_DrawFlip(image, screen, 1, 2, CN_DRAWDIRECTION_NORMAL);
       compareImageToChar(screen, expected);
       resetImage(screen);

       CN_Image_DrawFlip(image, screen, 1, 2,
           CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY);
       compareImageToChar(screen, expected);
       resetImage(screen);

       expected[0] = "0000000000";
       expected[1] = "0000000000";
       expected[2] = "0765432100";
       expected[3] = "0fedcba980";
       expected[4] = "0765432100";
       expected[5] = "0fedcba980";
       expected[6] = "0000000000";
       expected[7] = "0000000000";
       expected[8] = "0000000000";
       expected[9] = "0000000000";
       CN_Image_DrawFlip(image, screen, 1, 2, CN_DRAWDIRECTION_FLIPX);
       compareImageToChar(screen, expected);
       resetImage(screen);

       CN_Image_DrawFlip(image, screen, 1, 2,
           CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPY);
       compareImageToChar(screen, expected);
       resetImage(screen);

       expected[0] = "0000000000";
       expected[1] = "0000000000";
       expected[2] = "089abcdef0";
       expected[3] = "0012345670";
       expected[4] = "089abcdef0";
       expected[5] = "0012345670";
       expected[6] = "0000000000";
       expected[7] = "0000000000";
       expected[8] = "0000000000";
       expected[9] = "0000000000";
       CN_Image_DrawFlip(image, screen, 1, 2, CN_DRAWDIRECTION_FLIPY);
       compareImageToChar(screen, expected);
       resetImage(screen);

       CN_Image_DrawFlip(image, screen, 1, 2,
           CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPX);
       compareImageToChar(screen, expected);
       resetImage(screen);
       
       expected[0] = "0000000000";
       expected[1] = "0000000000";
       expected[2] = "0fedcba980";
       expected[3] = "0765432100";
       expected[4] = "0fedcba980";
       expected[5] = "0765432100";
       expected[6] = "0000000000";
       expected[7] = "0000000000";
       expected[8] = "0000000000";
       expected[9] = "0000000000";
       CN_Image_DrawFlip(image, screen, 1, 2,
                           CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY);
       compareImageToChar(screen, expected);
       resetImage(screen);

       CN_Image_DrawFlip(image, screen, 1, 2,
                           CN_DRAWDIRECTION_ROTATE180);
       compareImageToChar(screen, expected);
       resetImage(screen);

       expected[0] = "0000000000";
       expected[1] = "0008080000";
       expected[2] = "0019190000";
       expected[3] = "002a2a0000";
       expected[4] = "003b3b0000";
       expected[5] = "004c4c0000";
       expected[6] = "005d5d0000";
       expected[7] = "006e6e0000";
       expected[8] = "007f7f0000";
       expected[9] = "0000000000";

       CN_Image_DrawFlip(image, screen, 1, 2, (CN_DRAWDIRECTION_ROTATE90));
       compareImageToChar(screen, expected);
       resetImage(screen);

       CN_Image_DrawFlip(image, screen, 1, 2,
           CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY);
       compareImageToChar(screen, expected);
       resetImage(screen);

       expected[0] = "0000000000";
       expected[1] = "0080800000";
       expected[2] = "0091910000";
       expected[3] = "00a2a20000";
       expected[4] = "00b3b30000";
       expected[5] = "00c4c40000";
       expected[6] = "00d5d50000";
       expected[7] = "00e6e60000";
       expected[8] = "00f7f70000";
       expected[9] = "0000000000";
       CN_Image_DrawFlip(image, screen, 1, 2,
                           (CN_DRAWDIRECTION_ROTATE90 | CN_DRAWDIRECTION_FLIPX));
       compareImageToChar(screen, expected);
       resetImage(screen);

       CN_Image_DrawFlip(image, screen, 1, 2,
                           (CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPY));
       compareImageToChar(screen, expected);
       resetImage(screen);

       expected[0] = "0000000000";
       expected[1] = "007f7f0000";
       expected[2] = "006e6e0000";
       expected[3] = "005d5d0000";
       expected[4] = "004c4c0000";
       expected[5] = "003b3b0000";
       expected[6] = "002a2a0000";
       expected[7] = "0019190000";
       expected[8] = "0008080000";
       expected[9] = "0000000000";
       CN_Image_DrawFlip(image, screen, 1, 2,
                           (CN_DRAWDIRECTION_ROTATE90 | CN_DRAWDIRECTION_FLIPY));
       compareImageToChar(screen, expected);
       resetImage(screen);

       CN_Image_DrawFlip(image, screen, 1, 2,
                           (CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPX));
       compareImageToChar(screen, expected);
       resetImage(screen);

       expected[0] = "0000000000";
       expected[1] = "00f7f70000";
       expected[2] = "00e6e60000";
       expected[3] = "00d5d50000";
       expected[4] = "00c4c40000";
       expected[5] = "00b3b30000";
       expected[6] = "00a2a20000";
       expected[7] = "0091910000";
       expected[8] = "0080800000";
       expected[9] = "0000000000";

       CN_Image_DrawFlip(image, screen, 1, 2,
                           (CN_DRAWDIRECTION_ROTATE270));
       compareImageToChar(screen, expected);
       resetImage(screen);

       CN_Image_DrawFlip(image, screen, 1, 2,
                           (CN_DRAWDIRECTION_ROTATE90 |
                            CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY));
       compareImageToChar(screen, expected);
       resetImage(screen);

       /* test OOB */
       CN_Image_DrawFlip(image, screen, 1, 30000,
                           (CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPX));
       CN_Image_DrawFlip(image, screen, -30000, -30000,
                           (CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPX));
       CN_Image_DrawFlip(image, screen, 8, 8,
                           (CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPX));
    } deinit();
    return 0;
}

