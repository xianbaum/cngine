#include "../cngine.h"
#include <stdio.h>
#include <stdlib.h>
#include "cngine.h"
#include "cn_image.h"
#include "datastr.h"

static int runTest(int testNumber);

int main(int argv, char **argc)
{
    if(argv != 2 ) {
        printf("This program requires 1 numeric argument.");
        return 1;
    }
    return runTest(atoi(argc[1]));
}

static int runTest(int testNumber) {
    switch(testNumber) {
    case 1: return Test_CN_Options_FromCommandLineArgs();
    case 2: return Test_CN_StringDictionary();
    case 3: return Test_CN_VectorList();
    case 4: return Test_CN_Init_CN_Deinit();
    case 5: return Test_CN_Image_Create();
    case 6: return Test_CN_Image_PutPixel();
    case 7: return Test_CN_Image_Draw();
    case 8: return Test_CN_Image_DrawMasked();
    case 9: return Test_CN_Image_DrawOffset();
    case 10: return Test_CN_Image_DrawMaskedOffset();
    case 11: return Test_CN_Image_DrawRect();
    case 12: return Test_CN_Image_DrawRectMasked();
    case 13: return Test_CN_Image_DrawRectOffset();
    case 14: return Test_CN_Image_DrawRectMaskedOffset();
    case 18: return Test_CN_Image_DrawHLineNoGuard();
    case 19: return Test_CN_Image_DrawVLineNoGuard();
    case 20: return Test_CN_Image_DrawFlip();
    }
}
