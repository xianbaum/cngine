#ifndef TEST_CN_IMAGE_H_
#define TEST_CN_IMAGE_H_
#include "../cngine.h"

int Test_CN_Image_Create();
int Test_CN_Image_PutPixel();

int Test_CN_Image_Draw();
int Test_CN_Image_DrawMasked();
int Test_CN_Image_DrawOffset();
int Test_CN_Image_DrawMaskedOffset();
int Test_CN_Image_DrawRect();
int Test_CN_Image_DrawRectMasked();
int Test_CN_Image_DrawRectOffset();
int Test_CN_Image_DrawRectMaskedOffset();
int Test_CN_Image_DrawHLineNoGuard();
int Test_CN_Image_DrawVLineNoGuard();

int Test_CN_Image_DrawFlip();
int Test_CN_Image_DrawMaskedFlip();
int Test_CN_Image_DrawOffsetFlip();
int Test_CN_Image_DrawMaskedOffsetFlip();
int Test_CN_Image_DrawRectFlip();
int Test_CN_Image_DrawRectMaskedFlip();
int Test_CN_Image_DrawRectOffsetFlip();
int Test_CN_Image_DrawRectMaskedOffsetFlip();

int Test_CN_Image_DrawScaled();
int Test_CN_Image_DrawScaledMasked();
int Test_CN_Image_DrawScaledMaskedOffset();

int Test_CN_Image_DrawSheared();
int Test_CN_Image_DrawShearedMasked();
int Test_CN_Image_DrawShearedMaskedOffset();
#endif
