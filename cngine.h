/**
 * @file cngine.h
 * @author Christian Michael Baum
 * @brief Contains definitions mainly for I/O that are (hopefully) consistent
 * across implementations.
 **/

#ifndef CNGINE_H_
#define CNGINE_H_

#include "common/inline.h"
#include "common/cnbool.h"
#include "common/cnrect.h"
#include "common/cnpoint.h"
#include "core/cnmem.h"
#include "core/chelpers.h"
#include "core/cnerror.h"
#include "datastr/hashmap.h"
#include "datastr/llist.h"
#include "datastr/rbbst.h"
#include "datastr/spart.h"
#include "datastr/uitree.h"
#include "datastr/vlist.h"
#include "interf/destruct.h"

#ifdef CN_SDL2
#define NO_STDIO_REDIRECT
#define CN_Image_Clone_DEFINED

#define SDL_MAIN_HANDLED

#include "impl/sdl2.h"
#elif defined CN_SDL1_2
#define CN_Image_Clone_DEFINED
#define NO_STDIO_REDIRECT
#include "impl/sdl1_2.h"
#elif defined CN_ALLEGRO4
#define CN_RGB_Set_DEFINED
#include "impl/allegro4.h"
#elif defined CN_LIBRETRO
#include "impl/retrcore.h"
#elif defined CN_PSX
#include "impl/psx.h"
#else
#include "impl/dummy.h"
#endif

/** @brief An image structure. */
typedef CN_Image_Impl CN_Image;

/** @brief An RGB Color, often used for palette data or pixel color */
typedef CN_RGB_Impl CN_RGB;

/** @brief Just a file, but might use as a wrapper for some implementations in the future */
typedef FILE CN_File;

typedef struct {
    CN_Image *image;
    CN_RGB *pal;
} CN_ImageAndPal;

typedef enum {
    /* ignored */
    CN_DRAWDIRECTION_NONE = 0,
    /* FlipX and FlipY can be combined */
    CN_DRAWDIRECTION_NORMAL = 1,
    CN_DRAWDIRECTION_FLIPX = 2,
    CN_DRAWDIRECTION_FLIPY = 4,
    /* Despite being flags, only one rotate can be used */
    CN_DRAWDIRECTION_ROTATE90 = 8,
    CN_DRAWDIRECTION_ROTATE180 = 16,
    CN_DRAWDIRECTION_ROTATE270 = 32,
} CN_DrawDirection;

#include "img/draw.h"
#include "game/font.h"
#include "game/tilemap.h"
#include "game/anim.h"
#include "game/artmgr.h"l

/**
 * @brief A MOD file.   
 *
 */
typedef CN_MOD_Impl CN_MOD;

/**
 * @brief A Soundbyte.
 *
 */
typedef CN_SoundByte_Impl CN_SoundByte;

/**
 * @brief A music structure. Either a CN_MOD or a CN_MIDI.
 *
 */
typedef CN_Music_Impl CN_Music;

/**
 * @brief A MIDI file
 *
 */
typedef CN_MIDI_Impl CN_MIDI;

/**
 * @brief A key code. Actual values vary based on implementation.
 * You should be using key macros for this. Keyboard A is CN_KEY_A
 */
typedef CN_Key_Impl CN_Key;

/**
 * @brief A mouse button. Actual values vary based on implementation.
 * You should be using button macros for this. Left mouse button is
 * CN_MOUSEBUTTON_LMB
 **/
typedef CN_MouseButton_Impl CN_MouseButton;

typedef enum CN_VideoMode {
    CN_MODE_USE_PROPERTIES = 0,
    CN_MODE_FULLSCREEN = 1,
    CN_MODE_WINDOWED = 2,
    CN_MODE_WINDOWED_BORDERLESS = 3,
    CN_MODE_ALLEGRO_AUTODETECT = 4,
    CN_MODE_ALLEGRO_GFX_SAFE = 5,
    CN_MODE_SDL_FLAGS = 6,
    CN_MODE_TEXT = 7
} CN_VideoMode;

typedef struct CN_Options {
    CN_VideoMode mode;
    int modeFlags;
    int width;
    int height;
    char *windowTitle;
    CN_bool mouseHidden;
    CN_LoggingLevel loggingLevel;
    CN_MusicType musicType;
} CN_Options;

extern CN_Options *CN_options;

/*Backend related*/
/**
 * @brief Creates a CN_Options with default options
 *
 * @return A default CN_Options
 **/
CN_Options *CN_Options_CreateBlank(void);

/**
 * @brief Creates CN_Options from command line arguments.
 * 
 * @return CN_Options created from command line args.
 **/
CN_Options *CN_Options_FromCommandLineArgs(int argc, char **argv);

/**
 * @brief Frees CN_Options.
 *
 **/
void CN_Options_Free(CN_Options *options);

/**
 * @brief Initializes the implementation. To be called before the game starts.
 *
 * If it is unsuccessful, it will return CN_false. The game should be exited in
 * this case.
 *
 * @param options a pointer to CN_Options. Can be NULL.
 *
 * @return CN_bool Whether it was successful or not.
 **/
CN_bool CN_Backend_Init(CN_Options *options);

/**
 * @brief Returns true if the implementation can be restarted.
 *
 * It is possible for this function to return true and CN_Deinit to return false.
 **/
CN_bool CN_Backend_CanReinit(void);

/**
 * @brief Deinitializes the implementation. To be called before quit.
 *        While some implementations may alloy restarting with a Deinit ->
 *        init call, it is not guaranteed. 
 *
 * @return CN_bool Whether the program must restart
 */
CN_bool CN_Backend_Deinit(void);

#ifndef CN_CUSTOM_MAIN

int main(int argc, char* argv[]);

/**
 * @brief Initialization. Implemented by the game, not CNgine.
 * 
 * @param argc Argument count
 * @param argv Double pointer containing arguments
 **/
extern void CN_Init(int argc, char **argv);

/**
 * @brief Deinitialization. Implemented by the game, not CNgine.
 * 
 * @param argc Argument count
 * @param argv Double pointer containing arguments
 **/
extern void CN_Deinit(void);

/**
 * @brief A single frame of play. Implemented by the game, not CNgine.
 *
 * @param dt delta time
 **/
extern void CN_Frame(int dt);

#endif

/**
 * @brief Updates the implementation specifics to be called once per loop
 */
void CN_Backend_Update(void);

/**
 * @brief Updates the implementation specifics to be called ending a frame
 *
 */
void CN_Backend_FinalizeFrame(void);

/**
 * @brief The implementation-defined exit condition
 *
 * @return CN_bool Whether it is time to exit or not.
 */
CN_bool CN_ExitCondition(void);

/**
 * @brief Returns the music type that CN_Music will use.
 *
 * @return CN_MusicType The music type that CN_Music is
 */
CN_MusicType CN_GetMusicType(void);

/*Draw related*/
/**
 * @brief Either draws the buffer image to the screen or does nothing.
 *
 * It depends on the implementation. Either way, this function should be called
 * at the end of a frame.
 *
 */
void CN_Flip(void);

/**
 * @brief Loads an image from a given path. Supports pcx or gif
 *
 * @param path The path to load. PCX or GIF.
 * @return The loaded image. Check the return value to check for success
 */
CN_Image *CN_Image_Load(const char *path);

/**
 * @brief Creates a blank CN_Image from width and size with no palette data.
 *
 * @param width The image width dimension
 * @param height The image height dimension
 * @return CN_Image* The created image or NULL if no image could be created
 **/
CN_Image *CN_Image_Create(int width, int height);

/**
 * @brief Creates a clone of an image input.
 *
 * @param input The input image
 * @return CN_Image* The cloned image or NULL if no image could be created
 **/
CN_Image *CN_Image_Clone(CN_Image *input);

/**
 * @brief Shifts the colors of each image
 *
 * @param image The image to shift
 * @param amount The number amount to shift by.
 **/
void CN_Image_ShiftColors(CN_Image *image, int amount);

/**
 * @brief Gets the width of an image
 *
 * @param bitmap The image to get from
 **/
static CN_inline short CN_Image_GetWidth(const CN_Image *bitmap);

/**
 * @brief Gets the height of an image
 *
 * @param bitmap The image to get from
 **/

static CN_inline short CN_Image_GetHeight(const CN_Image *bitmap);
/**
 * @brief Returns the screen bitmap
 * 
 * @return The screen CN_Image
 */
static CN_inline CN_Image *CN_GetScreen();

/**
 * @brief Gets an RGB pixel from an image at the X and Y coordinates.
 *
 * @param image The image to extract from
 * @param x The x position. Start from 0
 * @param y The y position. Start from 0
 * @return CN_RGB The RGB pixel gotten.
 */
static CN_inline CN_RGB CN_Image_GetRGB(
    const CN_RGB *pal, CN_Image *image, int x, int y);

/**
 * @brief Estimates the approximate size in bytes of the image
 *
 * @param image The image to estimate
 **/
size_t CN_Image_EstimateSizeOf(const CN_Image *image);

/**
 * @brief Creates a single color from an input RGB value.
 *
 * This function is useful for creating palettes programatically.
 *
 * @param r The red value of the color
 * @param g The green value of the color
 * @param b The blue value of the color
 * @return CN_RGB The created color
 */
CN_RGB CN_RGB_Create(unsigned char r, unsigned char g, unsigned char b);

/**
 * @brief Creates a blank 256 color palette
 *
 * @return A pointer to the palette. Must be freed with  CN_RGB_Free.
 **/
CN_RGB *CN_RGB_CreateBlank(void);

/**
 * @brief Extracts a palette from a loaded image.
 *
 * The image is safe to free after a palette is extracted.
 *
 * @param image The input image. The palette is gotten from the image.
 * @return CN_RGB* The extracted palette.
 */
CN_RGB *CN_RGB_ExtractFromImage(const char *image);

/**
 * @brief Frees a palette.
 *
 * @param palette The palette to free. The pointer is no longer valid after this
 */
void CN_RGB_Free(CN_RGB *palette);

/**
 * @brief Compares two pixels
 *
 * @param pixel1 The first pixel to compare
 * @param pixel2 The second pixel to compare
 * @return CN_bool
 */
CN_bool CN_RGB_Compare(const CN_RGB *pixel1, const CN_RGB *pixel2);

/**
 * @brief Clears the screen to the black color.
 *
 */
void CN_Cls(void);

/**
 * @brief Frees a given image.
 *
 * @param image The image to free. The pointer is no longer valid after this.
 */
void CN_Image_Free(CN_Image *image);

/**
 * @brief Replaces the global palette with the provided one
 *
 * @param palette The palette that will replace the global palette
 */
void CN_RGB_Set(CN_RGB *palette);

/**
 * @brief Sets the global palette to the range
 *
 * @param palette The source palette
 * @param from The starting color, zero indexed
 * @param to The ending color.
 */
void CN_RGB_SetRange(CN_RGB *palette, int from, int to);

/**
 * @brief Draws an image without masking.
 *
 * @param image The image to draw
 * @param source The source rectangle from the image
 * @param dest The destination X and Y on the screen. w and h are ignored
 */
void CN_Image_FastDraw(CN_Image *image, const CN_Rect *source,
                       const CN_Rect *dest);

/**
 * @brief Gets the color palette index of the x and y pixel
 *
 * @param image The image to get from
 * @param x The X position to get
 * @param y The Y position to get
 */
static CN_inline unsigned char
CN_Image_GetPixel(CN_Image* image, int x, int y);

/**
 * @brief Sets the color palette index of a pixel
 *
 * @param image The image to modify
 * @param x The X position of the pixel
 * @param y The Y position of the pixel
 * @param index The color index in a 256 CN_RGB palette
 */
static CN_inline void
CN_Image_PutPixel(CN_Image *image, int x, int y, unsigned char index);

/*Timer functions*/
/**
 * @brief Gets the total milliseconds passed
 * @return Total milliseconds as an unsigned int
 */
static CN_inline unsigned int CN_GetMilliseconds(void);

/**
 * @brief Gets the delta time since the last call
 * @return Delta time since last call
 */
static CN_inline unsigned int CN_GetDeltaTime(void);

/**
 * @brief Gets the amount of milliseconds that have passed since the last call.
 *
 * This function should be called at the beginning of a frame.
 *
 * @return int The amount of milliseconds that have passed.
 */
static CN_inline int CN_GetFramesSinceLastCall(void);

/**
 * @brief Increases the global game timer by the current time. Should be used
 * once per frame.
 *
 */
static CN_inline void CN_IncrementTimer(void);

/**
 * @brief Gets the timer
 *
 * @return The timer value in ms
 */
static CN_inline long CN_GetTimer(void);

/**
 * @brief Increased the game ticks counter by one. Should be used once per frame
 *
 */
/* what? */

/**
 * @brief Gets the ticker value
 *
 * @return The number of frames that have passed
 **/
static CN_inline long CN_GetTicker(void);

/**
 * @brief Sleeps (stops everything) for the time in milliseconds.
 *
 * @param time Time in milliseconds
 */
static CN_inline void CN_Sleep(unsigned int time);

/*Audio functions*/
/**
 * @brief Loads a soundbyte from a given path. WAV is supported.
 *
 * @param path The exact file path to the soundbyte. Must be WAV.
 * @return CN_SoundByte* Returns a soundbyte from the path.
 */
CN_SoundByte *CN_SoundByte_Load(const char *path);

/**
 * @brief Frees a soundbyte.
 *
 * @param soundbyte The soundbyte to free. The pointer is invalid afterwards.
 */
void CN_SoundByte_Free(CN_SoundByte *soundbyte);

/**
 * @brief Loads a music given a path. If it is an IT or MOD and the option for
 * loading MID music is set, it changes the file extension to MID. MID should
 * not be loaded directly but it can be.
 *
 * @param path The path (IT or MOD) of the music to load.
 * @return CN_Music* The loaded CN_Music or NULL if there is an error.
 */
CN_Music *CN_Music_Load(const char* path);

/**
 * @brief Frees the music. The input pointer is no longer valid after this call.
 *
 * @param music The music to be freed.
 */
void CN_Music_Free(CN_Music *music);

/**
 * @brief Plays a music. If a music is already being played, it stops the music
 * first.
 *
 * @param music The music to play.
 */
void CN_Music_Play(CN_Music *music);

/**
 * @brief Stops the currently played song. Does not free it.
 *
 */
void CN_Music_Stop(void);

/****************************************************************************
 * Keyboard
 ****************************************************************************/

/**
 * @brief Given a key code, returns a CN_bool of whether it is currently down
 *
 * @param key_code A CN_Key (one of the input defines)
 * @return CN_bool CN_true if they key is down. CN_false if they key is up
 */
static CN_inline CN_bool CN_IsKeyDown(CN_Key key_code);

/**
 * @brief Given a key code, returns a CN_bool of whether it was initially pressed
 * 
 * @param key_code a CN_Key (one of the input defines)
 * @return CN_bool CN_true if the key was pressed this frame.
 */
static CN_inline CN_bool CN_IsKeyPressed(CN_Key key_code);

/**
 * @brief Given a key code, returns a CN_bool of whether it was just unpressed.
 *
 * @param key_code a CN_Key (one of the input defines)
 * @return CN_bool CN_true if the key was up this frame.
 */
static CN_inline CN_bool CN_IsKeyUp(CN_Key key_code);

/**
 * @brief Returns the char character of the key pressed, or returns a NULL char.
 *
 * @return char the key pressed or NULL ('\0') if no key is pressed.
 */
static CN_inline char CN_CharKeyDown(void);

/****************************************************************************
 * Mouse
 ****************************************************************************/

static CN_inline void CN_GetMouseCoords(int *x, int *y);

static CN_inline CN_bool CN_IsMouseButtonDown(CN_MouseButton button);

/****************************************************************************
 * Global variables
 ****************************************************************************/

extern CN_bool CN_exit;

/****************************************************************************
 * Key defines
 *
 * The values of these can change depending on implementation
 ****************************************************************************/

/** @brief The 0 key on a keyboard */
#define CN_KEY_0 CN_KEY__0
/** @brief The 1 key on a keyboard */
#define CN_KEY_1 CN_KEY__1
/** @brief The 2 key on a keyboard */
#define CN_KEY_2 CN_KEY__2
/** @brief The 3 key on a keyboard */
#define CN_KEY_3 CN_KEY__3
/** @brief The 4 key on a keyboard */
#define CN_KEY_4 CN_KEY__4
/** @brief The 5 key on a keyboard */
#define CN_KEY_5 CN_KEY__5
/** @brief The 6 key on a keyboard */
#define CN_KEY_6 CN_KEY__6
/** @brief The 7 key on a keyboard */
#define CN_KEY_7 CN_KEY__7
/** @brief The 8 key on a keyboard */
#define CN_KEY_8 CN_KEY__8
/** @brief The 9 key on a keyboard */
#define CN_KEY_9 CN_KEY__9
/** @brief The A key on a keyboard */
#define CN_KEY_A CN_KEY__A
/** @brief The B key on a keyboard */
#define CN_KEY_B CN_KEY__B
/** @brief The BACKSLASH key on a keyboard */
#define CN_KEY_BACKSLASH CN_KEY__BACKSLASH
/** @brief The BACKSPACE key on a keyboard */
#define CN_KEY_BACKSPACE CN_KEY__BACKSPACE
/** @brief The C key on a keyboard */
#define CN_KEY_C CN_KEY__C
/** @brief The CAPSLOCK key on a keyboard */
#define CN_KEY_CAPSLOCK CN_KEY__CAPSLOCK
/** @brief The COMMA key on a keyboard */
#define CN_KEY_COMMA CN_KEY__COMMA
/** @brief The D key on a keyboard */
#define CN_KEY_D CN_KEY__D
/** @brief The DELETE key on a keyboard */
#define CN_KEY_DELETE CN_KEY__DELETE
/** @brief The DOWN key on a keyboard */
#define CN_KEY_DOWN CN_KEY__DOWN
/** @brief The E key on a keyboard */
#define CN_KEY_E CN_KEY__E
/** @brief The END key on a keyboard */
#define CN_KEY_END CN_KEY__END
/** @brief The EQUALS key on a keyboard */
#define CN_KEY_EQUALS CN_KEY__EQUALS
/** @brief The ESC key on a keyboard */
#define CN_KEY_ESC CN_KEY__ESC
/** @brief The  key on a keyboard */
#define CN_KEY_TILDE CN_KEY__TILDE
/** @brief The F1 key on a keyboard */
#define CN_KEY_F1  CN_KEY__F1 
/** @brief The F10 key on a keyboard */
#define CN_KEY_F10 CN_KEY__F10
/** @brief The F11 key on a keyboard */
#define CN_KEY_F11 CN_KEY__F11
/** @brief The F12 key on a keyboard */
#define CN_KEY_F12 CN_KEY__F12
/** @brief The F2 key on a keyboard */
#define CN_KEY_F2 CN_KEY__F2
/** @brief The F3 key on a keyboard */
#define CN_KEY_F3 CN_KEY__F3
/** @brief The F4 key on a keyboard */
#define CN_KEY_F4 CN_KEY__F4
/** @brief The F5 key on a keyboard */
#define CN_KEY_F5 CN_KEY__F5
/** @brief The F6 key on a keyboard */
#define CN_KEY_F6 CN_KEY__F6
/** @brief The F7 key on a keyboard */
#define CN_KEY_F7 CN_KEY__F7
/** @brief The F8 key on a keyboard */
#define CN_KEY_F8 CN_KEY__F8
/** @brief The F9 key on a keyboard */
#define CN_KEY_F9 CN_KEY__F9
/** @brief The G key on a keyboard */
#define CN_KEY_G CN_KEY__G
/** @brief The H key on a keyboard */
#define CN_KEY_H CN_KEY__H
/** @brief The HOME key on a keyboard */
#define CN_KEY_HOME CN_KEY__HOME
/** @brief The I key on a keyboard */
#define CN_KEY_I CN_KEY__I
/** @brief The INSERT key on a keyboard */
#define CN_KEY_INSERT CN_KEY__INSERT
/** @brief The J key on a keyboard */
#define CN_KEY_J CN_KEY__J
/** @brief The K key on a keyboard */
#define CN_KEY_K CN_KEY__K
/** @brief The 0_PAD key on a keyboard */
#define CN_KEY_0_PAD CN_KEY__0_PAD
/** @brief The 1_PAD key on a keyboard */
#define CN_KEY_1_PAD CN_KEY__1_PAD
/** @brief The 2_PAD key on a keyboard */
#define CN_KEY_2_PAD CN_KEY__2_PAD
/** @brief The 3_PAD key on a keyboard */
#define CN_KEY_3_PAD CN_KEY__3_PAD
/** @brief The 4_PAD key on a keyboard */
#define CN_KEY_4_PAD CN_KEY__4_PAD
/** @brief The 5_PAD key on a keyboard */
#define CN_KEY_5_PAD CN_KEY__5_PAD
/** @brief The 6_PAD key on a keyboard */
#define CN_KEY_6_PAD CN_KEY__6_PAD
/** @brief The 7_PAD key on a keyboard */
#define CN_KEY_7_PAD CN_KEY__7_PAD
/** @brief The 8_PAD key on a keyboard */
#define CN_KEY_8_PAD CN_KEY__8_PAD
/** @brief The 9_PAD key on a keyboard */
#define CN_KEY_9_PAD CN_KEY__9_PAD
/** @brief The ENTER_PAD key on a keyboard */
#define CN_KEY_ENTER_PAD CN_KEY__ENTER_PAD
/** @brief The EQUALS_PAD key on a keyboard */
#define CN_KEY_EQUALS_PAD CN_KEY__EQUALS_PAD
/** @brief The SLASH_PAD key on a keyboard */
#define CN_KEY_SLASH_PAD CN_KEY__SLASH_PAD
/** @brief The ASTERICK_PAD key on a keyboard */
#define CN_KEY_ASTERICK_PAD CN_KEY__ASTERICK_PAD
/** @brief The MINUS_PAD key on a keyboard */
#define CN_KEY_MINUS_PAD CN_KEY__MINUS_PAD
/** @brief The PERIOD_PAD key on a keyboard */
#define CN_KEY_PERIOD_PAD CN_KEY__PERIOD_PAD
/** @brief The PLUS_PAD key on a keyboard */
#define CN_KEY_PLUS_PAD CN_KEY__PLUS_PAD
/** @brief The L key on a keyboard */
#define CN_KEY_L CN_KEY__L
/** @brief The LALT key on a keyboard */
#define CN_KEY_LALT CN_KEY__LALT
/** @brief The LCTRL key on a keyboard */
#define CN_KEY_LCTRL CN_KEY__LCTRL
/** @brief The LEFT key on a keyboard */
#define CN_KEY_LEFT CN_KEY__LEFT
/** @brief The LBRACKET key on a keyboard */
#define CN_KEY_LBRACKET CN_KEY__LBRACKET
/** @brief The LSUPER key on a keyboard */
#define CN_KEY_LSUPER CN_KEY__LSUPER
/** @brief The LSHIFT key on a keyboard */
#define CN_KEY_LSHIFT CN_KEY__LSHIFT
/** @brief The M key on a keyboard */
#define CN_KEY_M CN_KEY__M
/** @brief The MENU key on a keyboard */
#define CN_KEY_MENU CN_KEY__MENU
/** @brief The MINUS key on a keyboard */
#define CN_KEY_MINUS CN_KEY__MINUS
/** @brief The N key on a keyboard */
#define CN_KEY_N CN_KEY__N
/** @brief The O key on a keyboard */
#define CN_KEY_O CN_KEY__O
/** @brief The P key on a keyboard */
#define CN_KEY_P CN_KEY__P
/** @brief The PAGEDOWN key on a keyboard */
#define CN_KEY_PAGEDOWN CN_KEY__PAGEDOWN
/** @brief The PAGEUP key on a keyboard */
#define CN_KEY_PAGEUP CN_KEY__PAGEUP
/** @brief The PAUSE key on a keyboard */
#define CN_KEY_PAUSE CN_KEY__PAUSE
/** @brief The PERIOD key on a keyboard */
#define CN_KEY_PERIOD CN_KEY__PERIOD
/** @brief The PRINTSCREEN key on a keyboard */
#define CN_KEY_PRINTSCREEN CN_KEY__PRINTSCREEN
/** @brief The Q key on a keyboard */
#define CN_KEY_Q CN_KEY__Q
/** @brief The R key on a keyboard */
#define CN_KEY_R CN_KEY__R
/** @brief The RALT key on a keyboard */
#define CN_KEY_RALT CN_KEY__RALT
/** @brief The RCTRL key on a keyboard */
#define CN_KEY_RCTRL CN_KEY__RCTRL
/** @brief The ENTER key on a keyboard */
#define CN_KEY_ENTER CN_KEY__ENTER
/** @brief The RSUPER key on a keyboard */
#define CN_KEY_RSUPER CN_KEY__RSUPER
/** @brief The RIGHT key on a keyboard */
#define CN_KEY_RIGHT CN_KEY__RIGHT
/** @brief The RBRACKET key on a keyboard */
#define CN_KEY_RBRACKET CN_KEY__RBRACKET
/** @brief The RSHIFT key on a keyboard */
#define CN_KEY_RSHIFT CN_KEY__RSHIFT
/** @brief The S key on a keyboard */
#define CN_KEY_S CN_KEY__S
/** @brief The SCROLLOCK key on a keyboard */
#define CN_KEY_SCROLLOCK CN_KEY__SCROLLOCK
/** @brief The SEMICOLON key on a keyboard */
#define CN_KEY_SEMICOLON CN_KEY__SEMICOLON
/** @brief The SLASH key on a keyboard */
#define CN_KEY_SLASH CN_KEY__SLASH
/** @brief The SPACE key on a keyboard */
#define CN_KEY_SPACE CN_KEY__SPACE
/** @brief The T key on a keyboard */
#define CN_KEY_T CN_KEY__T
/** @brief The TAB key on a keyboard */
#define CN_KEY_TAB CN_KEY__TAB
/** @brief The U key on a keyboard */
#define CN_KEY_U CN_KEY__U
/** @brief The UP key on a keyboard */
#define CN_KEY_UP CN_KEY__UP
/** @brief The V key on a keyboard */
#define CN_KEY_V CN_KEY__V
/** @brief The W key on a keyboard */
#define CN_KEY_W CN_KEY__W
/** @brief The X key on a keyboard */
#define CN_KEY_X CN_KEY__X
/** @brief The Y key on a keyboard */
#define CN_KEY_Y CN_KEY__Y
/** @brief The Z key on a keyboard */
#define CN_KEY_Z CN_KEY__Z
/** @brief Left mouse button or mouse1 */
#define CN_MOUSE1 CN__MOUSE1
/** @brief Right mouse button or mouse2 */
#define CN_MOUSE2 CN__MOUSE2
/** @brief Middle mouse button or mouse3 */
#define CN_MOUSE3 CN__MOUSE3

/*  */
#ifndef CN_NO_INLINE

#if (defined CN_SDL2) || (defined CN_SDL1_2)
#include "impl/part/sdlinl.h"
#elif defined CN_ALLEGRO4
#include "impl/part/alleginl.h"
#elif defined CN_LIBRETRO
#include "impl/part/retroinl.h"
#elif !defined(CN_PSX)
#include "impl/part/dummyinl.h"
#endif

#endif
#endif /*CNGINE_H_*/
