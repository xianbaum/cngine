#include "cnmem.h"

#ifndef DEBUG
#ifndef CN_DISABLE_ERROR_CHECKING

void *CN__malloc(size_t size)
{
    void *pointer = malloc(size);
    CN_Expect(pointer != NULL, "Memory allocation error");
    return pointer;
}

void *CN__calloc(size_t nitems, size_t size)
{
    void *pointer = calloc(nitems, size);
    CN_Expect(pointer != NULL, "Memory allocation error");
    return pointer;
}

void *CN__realloc(void *pointer, size_t count)
{
    void *new_pointer = realloc(pointer, count);
    CN_Expect(new_pointer != NULL, "Memory re-allocation error");
    return new_pointer;
}
#endif /*CN_DISABLE_ERROR_CHECKING*/
#else /*DEBUG_*/
static CN_MemVector CN___allocations;

void CN___register_pointer(void *pointer, const char *file, int line)
{
    CN_MemoryInfo* memory_info;
    if(pointer == NULL) return;
    memory_info = malloc(sizeof(CN_MemoryInfo));
    CN_Assert(memory_info != NULL);
    CN_Assert(pointer != NULL);
    memory_info->pointer = pointer;
    memory_info->filename = file;
    memory_info->line_number = line;
    if(CN___allocations.size == 0) {
        CN___allocations.size = 8;
        CN___allocations.data = malloc(sizeof(void*) * CN___allocations.size);
        CN_Assert(CN___allocations.data != NULL);
    }
    if(CN___allocations.size == CN___allocations.count) {
        CN___allocations.size *= 2;
        CN___allocations.data =
          realloc(CN___allocations.data, sizeof(void*) * CN___allocations.size);
        CN_Assert(CN___allocations.data != NULL);
    }
    CN___allocations.data[CN___allocations.count] = memory_info;
    CN___allocations.count++;
}

void CN___unregister_pointer(void *pointer)
{
    int i;
    CN_bool was_freed = CN_false;
    CN_Assert(CN___allocations.data != NULL);
    for(i = 0; i < CN___allocations.count; i++) {
        if(CN___allocations.data[i]->pointer == pointer) {
            free(CN___allocations.data[i]);
            for(; i < CN___allocations.count-1; i++) {
                CN___allocations.data[i] = CN___allocations.data[i+1];
            }
            CN___allocations.count--;
            was_freed = CN_true;
            break;
        }
    }
    CN_Try(was_freed, "Double-free or pointer was never registered!!");
    if(CN___allocations.count == 0) {
        free(CN___allocations.data);
        CN___allocations.data = NULL;
        CN___allocations.size = 0;
    }
}

void *CN___malloc(size_t size, const char * file, int line)
{
    void *pointer = malloc(size);
    CN_Expect(pointer != NULL, "Memory allocation error");
    CN___register_pointer(pointer, file, line);
    return pointer;
}

void *CN___calloc(size_t nitems, size_t size, const char *file, int line)
{
    void *pointer = calloc(nitems, size);
    CN_Expect(pointer != NULL, "Memory allocation error");
    CN___register_pointer(pointer, file, line);
    return pointer;
}

void *CN___realloc(void *pointer, size_t size, const char *file, int line)
{
    int i;
    void *new_pointer = realloc(pointer, size);
    CN_Expect(new_pointer != NULL,
              "Memory re-allocation error");
    if(pointer == NULL) {
        CN___register_pointer(pointer, file, line);
        return new_pointer;
    }
    CN_Expect(CN___allocations.data != NULL,
              "allocations.data is null. Can this happen?");
    for(i = 0; i < CN___allocations.count; i++) {
        if(CN___allocations.data[i]->pointer == pointer) {
            CN___allocations.data[i]->pointer = new_pointer;
        }
    }
    return new_pointer;
}

void CN___free (void *pointer)
{
    CN___unregister_pointer(pointer);
    free(pointer);
}

void CN_print_allocations(void)
{
    int i;
    CN_MemoryInfo current_mem_info;
    (void)current_mem_info;
    if(CN___allocations.count > 0 &&
       CN___allocations.data != NULL) {
        for(i = 0; i < CN___allocations.count; i++) {
            current_mem_info = *CN___allocations.data[i];
            CN_LogError(("Forgot to free; Malloc in: %s, line: %d",
                         current_mem_info.filename,
                         current_mem_info.line_number));
        }
    } else {
        CN_LogInfo(("Congratulations, there were no memory leaks!"));
    }
}

void CN_memory_atexit(void)
{
    CN_print_allocations();
    if(CN___allocations.data != NULL) {
        free(CN___allocations.data);
        CN___allocations.data = NULL;
    }
}
#endif
