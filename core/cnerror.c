#include "cnerror.h"

#ifdef USE_SDL2_
#include <SDL.h>
#endif
#if CN_DEFINED_LOGGING_LEVEL > 0
static CN_LoggingLevel CN__allowed_debug_levels = CN_DEFINED_LOGGING_LEVEL;
static char *DEFAULT_LOGPATH = "logfile.txt";
static char *CN__logging_file_path = NULL;
#endif
#ifndef CN_DEBUG
#ifndef CN_DISABLE_ERROR_CHECKING
void CN__expect(CN_bool condition, const char *message)
{
    if (!condition) {
#if (CN_DEFINED_LOGGING_LEVEL & 32) != 32 /*CN_LOGGING_LEVEL_CRITICAL*/
        /*Figured if it crashes, printing is useful*/
#ifdef USE_SDL2_
        SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Fatal: %s", message);
#else
        printf("Fatal: %s", message);
#endif
#endif
        CN_LogCritical(("%s", message));
        exit(EXIT_FAILURE);
    }
}

CN_bool CN__check(CN_bool condition)
{
    if (!condition) CN_LogError(("Condition failed"));
    return condition;
}

void CN__assert(CN_bool condition)
{
    if (!condition) {
#if (CN_DEFINED_LOGGING_LEVEL & 32) != 32 /*CN_LOGGING_LEVEL_CRITICAL*/
        /*Figured if it crashes, printing is useful*/
#ifdef USE_SDL2_
      SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Fatal: Condition failed");
#else
        printf("Fatal: Condition failed");
#endif
#endif
        CN_LogCritical(("Condition failed"));
        exit(EXIT_FAILURE);
    }
}

CN_bool CN__try(CN_bool condition, const char *message)
{
#if (CN_DEFINED_LOGGING_LEVEL & 16) != 16 /*CN_LOGGING_LEVEL_ERROR*/
    (void)message;                        /*suppress warning*/
#endif
    if (!condition) CN_LogError(("%s", message));
    return condition;
}

#endif
#else

void CN___expect(CN_bool condition, const char *message,
                 const char *expression)
{
    if (!condition) {
#if (CN_DEFINED_LOGGING_LEVEL & 32) != 32 /*CN_LOGGING_LEVEL_CRITICAL*/
        /*Figured if it critical, printing is useful anyway*/
#ifdef USE_SDL2_
        SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                        "Fatal: Condition %s failed: %s", expression, message);
#else
        printf("Fatal: Condition %s failed: %s", expression, message);
#endif
#endif
        CN_LogCritical(("Condition %s failed: %s", expression, message));
        exit(EXIT_FAILURE);
    }
}

void CN___assert(CN_bool condition, const char *expression)
{
    if (!condition) {
#if (CN_DEFINED_LOGGING_LEVEL & 32) != 32 /*CN_LOGGING_LEVEL_CRITICAL*/
        /*Figured if it crashes, printing is useful*/
#ifdef USE_SDL2_
        SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                        "Fatal: Condition %s failed", expression);
#else
        printf("Fatal: Condition %s failed", expression);
#endif
#endif
        CN_LogCritical(("Condition %s failed", expression));
        exit(EXIT_FAILURE);
    }
}

CN_bool CN___check(CN_bool condition, const char *expression)
{
    if (!condition) {
#if (CN_DEFINED_LOGGING_LEVEL & 16) != 16 /*CN_LOGGING_LEVEL_ERROR*/
        /*Hide warning*/
        (void)expression;
#endif
        CN_LogError(("Condition %s failed", expression));
    }
    return condition;
}

CN_bool CN___try(CN_bool condition, const char *message,
                 const char *expression)
{
    if (!condition) {
#if (CN_DEFINED_LOGGING_LEVEL & 16) != 16 /*CN_LOGGING_LEVEL_ERROR*/
        /*Hide warning*/
        (void)expression;
        (void)message;
#endif
        CN_LogError(("Condition %s failed: %s", expression, message));
    }
    return condition;
}
#endif

#if CN_DEFINED_LOGGING_LEVEL > 0
static char *CN_logging_level_string(CN_LoggingLevel logging_level)
{
    switch (CN_LOGGING_LEVEL_ALL & logging_level) {
    case CN_LOGGING_LEVEL_TRACE: return "TRACE: ";
    case CN_LOGGING_LEVEL_INFO: return "INFO: ";
    case CN_LOGGING_LEVEL_DEBUG: return "DEBUG: ";
    case CN_LOGGING_LEVEL_WARNING: return "WARNING: ";
    case CN_LOGGING_LEVEL_ERROR: return "ERROR: ";
    case CN_LOGGING_LEVEL_CRITICAL: return "CRITICAL: ";
    default: return "(What?): ";
    }
}

static void CN_vlog(CN_LoggingLevel logging_level,
                    const char *format,
                    va_list file_args, va_list stdio_args)
{
    const char *const
        logging_level_string = CN_logging_level_string(logging_level);
#ifdef USE_SDL2_
    SDL_LogPriority sdl_logging_level;
    switch (logging_level) {
    case CN_LOGGING_LEVEL_CRITICAL:
        sdl_logging_level = SDL_LOG_PRIORITY_CRITICAL;
        break;
    case CN_LOGGING_LEVEL_ERROR:
        sdl_logging_level = SDL_LOG_PRIORITY_ERROR;
        break;
    case CN_LOGGING_LEVEL_WARNING:
        sdl_logging_level = SDL_LOG_PRIORITY_WARN;
        break;
    case CN_LOGGING_LEVEL_INFO:
        sdl_logging_level = SDL_LOG_PRIORITY_INFO;
        break;
    case CN_LOGGING_LEVEL_DEBUG:
        sdl_logging_level = SDL_LOG_PRIORITY_DEBUG;
        break;
    case CN_LOGGING_LEVEL_TRACE:
    default:
        sdl_logging_level = SDL_LOG_PRIORITY_VERBOSE;
    }
#endif
    /*Ignore lower logging level*/
    if ((CN__allowed_debug_levels & logging_level) != logging_level) return;
    if (CN__logging_file_path != NULL) {
        FILE *file = CN_fopen(CN__logging_file_path, "a+");
        if (file != NULL) {
#ifndef NO_TIME
            fprintf(file, "%d ", (int)time(NULL));
#endif
            fprintf(file, logging_level_string, NULL);
            (void)vfprintf(file, format, file_args);
            fprintf(file, "\n");
            if (fclose(file) != 0) {
                fprintf(stderr, "Fatal: unable to close logfile???");
                exit(EXIT_FAILURE);
            }
        }
    }
#ifdef USE_SDL2_
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, sdl_logging_level, format,
                    stdio_args);
#else
    if ((logging_level & CN_LOGGING_LEVEL_ERROR) == CN_LOGGING_LEVEL_ERROR ||
     (logging_level & CN_LOGGING_LEVEL_CRITICAL) == CN_LOGGING_LEVEL_CRITICAL) {
        fprintf(stderr, logging_level_string, NULL);
        (void)vfprintf(stderr, format, stdio_args);
        fprintf(stderr, "\n");
    } else {
        fprintf(stdout, logging_level_string, NULL);
        vfprintf(stdout, format, stdio_args);
        fprintf(stdout, "\n");
    }
#endif
}

void CN__log(CN_LoggingLevel logging_level, const char *format,
             ...)
{
    va_list file_args, stdio_args;
    char *logging_level_string = CN_logging_level_string(logging_level);
    /*Ignore lower logging level*/
    if ((CN__allowed_debug_levels & logging_level) != logging_level) return;
    if (CN__logging_file_path != NULL) {
        FILE *file = CN_fopen(CN__logging_file_path, "a+");
        if (file != NULL) {
            /*@ignore@*/ /*ignores variadic constant error*/
            fprintf(file, logging_level_string, NULL);
            /*@end@*/
            va_start(file_args, format);
            (void)vfprintf(file, format, file_args);
            va_end(file_args);
            fprintf(file, "\n");
            if (fclose(file) != 0) {
                fprintf(stderr, "Fatal: unable to close logfile???");
                exit(EXIT_FAILURE);
            }
        }
    }
    if ((logging_level & CN_LOGGING_LEVEL_ERROR) == CN_LOGGING_LEVEL_ERROR ||
     (logging_level & CN_LOGGING_LEVEL_CRITICAL) == CN_LOGGING_LEVEL_CRITICAL) {
        fprintf(stderr, logging_level_string, NULL);
        va_start(stdio_args, format);
        (void)vfprintf(stderr, format, stdio_args);
        va_end(stdio_args);
        fprintf(stdout, "\n");
    } else {
        fprintf(stdout, logging_level_string, NULL);
        va_start(stdio_args, format);
        (void)vfprintf(stdout, format, stdio_args);
        va_end(stdio_args);
        fprintf(stdout, "\n");
    }
}

static CN_bool loggerInitFromFile(void)
{
    FILE *properties_file = CN_fopen("logger.csv", "r");
    char buffer[1024];
    char *substr;
    if (CN__logging_file_path != NULL) return CN_false;
    if (properties_file == NULL) {
        CN__allowed_debug_levels = 0;
        return CN_true;
    }
    fgets(buffer, sizeof(buffer), properties_file);
    substr = strtok(buffer, ",");
    CN__allowed_debug_levels = (CN_LoggingLevel)strtod(substr, NULL);
    fgets(buffer, sizeof(buffer), properties_file);
    if(strcmp(buffer, DEFAULT_LOGPATH) != 0) {
        CN__logging_file_path = malloc(sizeof(char) * (strlen(buffer) + 1));
        strcpy(CN__logging_file_path, buffer);
    } else CN__logging_file_path = DEFAULT_LOGPATH;
    if (fclose(properties_file) != 0) {
        fprintf(stderr, "Fatal: Unable to closer logger init file ???\n");
    }
    return CN_true;
}

CN_bool CN_Logger_Init(CN_LoggingLevel loggingLevel)
{
    if(loggingLevel == CN_LOGGING_LEVEL_LOAD_FROM_FILE) return loggerInitFromFile();
    CN__allowed_debug_levels = loggingLevel;
    CN__logging_file_path = DEFAULT_LOGPATH;
    return CN_true;
}

void CN_Logger_Destroy(void)
{
    if(CN__logging_file_path != DEFAULT_LOGPATH) free(CN__logging_file_path);
    CN__allowed_debug_levels = CN_LOGGING_LEVEL_NONE;
    CN__logging_file_path = NULL;
}

#endif

#if (CN_DEFINED_LOGGING_LEVEL & 1) == 1 /*CN_LOGGING_LEVEL_TRACE*/
void CN__log_trace(const char *format, ...)
{
    va_list file_args, stdio_args;
    va_start(file_args, format);
    va_start(stdio_args, format);
    CN_vlog(CN_LOGGING_LEVEL_TRACE, format, file_args, stdio_args);
    va_end(stdio_args);
    va_end(file_args);
}
#endif

#if (CN_DEFINED_LOGGING_LEVEL & 2) == 2 /*CN_LOGGING_LEVEL_INFO*/
void CN__log_info(const char *format, ...)
{
    va_list file_args, stdio_args;
    va_start(file_args, format);
    va_start(stdio_args, format);
    CN_vlog(CN_LOGGING_LEVEL_INFO, format, file_args, stdio_args);
    va_end(stdio_args);
    va_end(file_args);
}
#endif

#if (CN_DEFINED_LOGGING_LEVEL & 4) == 4 /*CN_LOGGING_LEVEL_DEBUG*/
void CN__log_debug(const char *format, ...)
{
    va_list file_args, stdio_args;
    va_start(file_args, format);
    va_start(stdio_args, format);
    CN_vlog(CN_LOGGING_LEVEL_DEBUG, format, file_args, stdio_args);
    va_end(stdio_args);
    va_end(file_args);
}
#endif

#if (CN_DEFINED_LOGGING_LEVEL & 8) == 8 /*CN_LOGGING_LEVEL_WARNING*/
void CN__log_warning(const char *format, ...)
{
    va_list file_args, stdio_args;
    va_start(file_args, format);
    va_start(stdio_args, format);
    CN_vlog(CN_LOGGING_LEVEL_WARNING, format, file_args, stdio_args);
    va_end(stdio_args);
    va_end(file_args);
}
#endif

#if (CN_DEFINED_LOGGING_LEVEL & 16) == 16 /*CN_LOGGING_LEVEL_ERROR*/
void CN__log_error(const char *format, ...)
{
    va_list file_args, stdio_args;
    va_start(file_args, format);
    va_start(stdio_args, format);
    CN_vlog(CN_LOGGING_LEVEL_ERROR, format, file_args, stdio_args);
    va_end(stdio_args);
    va_end(file_args);
}
#endif

#if (CN_DEFINED_LOGGING_LEVEL & 32) == 32 /*CN_LOGGING_LEVEL_CRITICAL*/
void CN__log_critical(const char *format, ...)
{
    va_list file_args, stdio_args;
    va_start(file_args, format);
    va_start(stdio_args, format);
    CN_vlog(CN_LOGGING_LEVEL_CRITICAL, format, file_args, stdio_args);
    va_end(stdio_args);
    va_end(file_args);
}
#endif
