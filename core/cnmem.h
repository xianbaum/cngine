/**
 * @file cnmem.h
 * @author Christian Michael Baum
 * @brief Wrapper for C's memory functions
 **/

#ifndef CN_MEM_H_
#define CN_MEM_H_

#include <stdlib.h>
#include "../common/cnbool.h"
#include "cnerror.h"

#ifndef DEBUG

#define CN_register_pointer(x)
#define CN_unregister_pointer(x)

#define CN_free free
#define CN_memory_atexit()
#ifdef CN_DISABLE_ERROR_CHECKING
#define CN_malloc malloc
#define CN_realloc realloc
#define CN_free free
#define CN_calloc calloc
#else /*CN_DISABLE_ERROR_CHECKING*/
void *CN__malloc(size_t size);
void *CN__calloc(size_t nitems, size_t size);
void *CN__realloc(void *pointer, size_t count);
#define CN_malloc CN__malloc
#define CN_realloc CN__realloc
#define CN_calloc CN__calloc
#endif /*DISABLE_ERROR_CHECKING*/

#else /*DEBUG*/

typedef struct
{
    void *pointer;
    const char *filename;
    int line_number;
} CN_MemoryInfo;

typedef CN_MemoryInfo *CN_MemoryInfoList;

typedef struct
{
    int size;
    int count;
    CN_MemoryInfoList *data;
} CN_MemVector;

void *CN___malloc(size_t size, const char *file, int line);
void *CN___calloc(size_t nitems, size_t size, const char *file, int line);
void *CN___realloc(void *pointer, size_t count, const char *file, int line);
void CN___free (void *pointer);
void CN_memory_atexit();
void CN_print_allocations(void);
void CN___register_pointer(void *pointer,
                           const char *file, int line);
void CN___unregister_pointer(void *pointer);
#define CN_malloc(size) CN___malloc(size, __FILE__, __LINE__)
#define CN_free CN___free
#define CN_realloc(ptr, size) CN___realloc(ptr, size, __FILE__, __LINE__)
#define CN_calloc(nitems, size) CN___calloc(nitems, size, __FILE__, __LINE__)
#define CN_register_pointer(pointer) CN___register_pointer(pointer, __FILE__, \
                                                           __LINE__)
#define CN_unregister_pointer CN___unregister_pointer

#endif /*DEBUG*/

#endif /*CN_MEM_H_*/
