/**
 * @file chelpers.h
 * @author Christian Michael Baum
 * @brief Contains various C types and functions.
 * File to be removed and split up.
 **/

#ifndef _CHELPERS_H
#define _CHELPERS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CN_concat_macro(a, b) a##b
#define CN_using_malloc(var,size)                       \
    char CN_concat_macro(CN_mallocint,__LINE__) = 1;    \
    var = malloc(size);                                 \
    for(;CN_concat_macro(CN_mallocint,__LINE__) ||      \
            (free(var),0);                              \
        CN_concat_macro(CN_mallocint,__LINE__)=0)
#define CN_with_file(file, filename, mode)              \
    char CN_concat_macro(CN_fileint,__LINE__) = 1;      \
    file = fopen(filename, mode);                       \
    for(;CN_concat_macro(CN_fileint,__LINE__) ||        \
            (fclose(file), 0);                          \
        CN_concat_macro(CN_fileint,__LINE__)=0)

typedef enum
{
    CN_AUDIO_MIDI = 0,
    CN_AUDIO_MOD,
    CN_AUDIO_NONE
} CN_MusicType;

typedef void (*CN_func_generic)(void);
typedef void (*CN_void_freer)(void *thing_to_free);

typedef union
{
    void* pointer;
    int integer;
    char character;
} CN_data;

typedef struct {
    CN_func_generic function;
    CN_void_freer free_function;
    CN_data data;
} CN_Callback;

void CN_free_callback_standard(CN_Callback *callback);

int CN_int_scale(int const initial_size, int const percentage);
int CN_int_ScaleRoundingUp(int const initial_size, int const percentage);
int CN_int_GetSign(int number);
void CN_NoOp();
void *CN_null_return();
FILE *CN_fopen(const char *filename, const char *mode);
void CN_set_directory(const char *directory);
char *CN_create_temp_buffer(const char *filename);
void CN_free_temp_buffer(char *buffer);

#ifndef CN_OVERRIDE_CHELPERS_FOPEN
#define CN_fopen fopen
#endif

#endif /*_CHELPERS_H*/
