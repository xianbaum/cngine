#include "../cngine.h"

CN_Options *CN_options = NULL;

CN_RGB *CN_RGB_CreateBlank(void)
{
    const int paletteSize = 256;
    int i = 0;
    CN_RGB *pal = CN_malloc(sizeof(CN_RGB) * paletteSize);
    for(; i < paletteSize; i++) pal[i] = CN_RGB_Create(0,0,0);
    return pal;
}

void CN_Image_Print(CN_Image *image)
{
    int x, y;
    for(y = 0; y < CN_Image_GetHeight(image); y++) {
        for(x = 0; x < CN_Image_GetWidth(image); x++) {
            printf("%x", CN_Image_GetPixel(image, x, y) % 16);
        }
        printf("\n");
    }
}

#ifndef CN_Image_Clone_DEFINED
CN_Image *CN_Image_Clone(CN_Image *image)
{
    CN_Image *new;
    int w = CN_Image_GetWidth(image),
        h = CN_Image_GetHeight(image);
    new = CN_Image_Create(image->w, image->h);
    CN_Image_Draw(image, new, 0, 0);
    return new;
}
#endif

CN_Options *CN_Options_CreateBlank(void)
{
    CN_Options *options = CN_malloc(sizeof(CN_Options));
    options->mode = CN_MODE_WINDOWED;
    options->modeFlags = -1;
    options->width = 320;
    options->height = 240;
    options->windowTitle = "CNgine";
    options->mouseHidden = CN_false;
    options->loggingLevel = CN_LOGGING_LEVEL_LOAD_FROM_FILE;
    options->musicType = CN_AUDIO_MOD;
    return options;
}

static CN_inline CN_bool _checkArgCount(int argc, int i)
{
    return argc > i;
}

static CN_inline CN_Options *_processArgs(int argc, char **argv)
{
    CN_Options *options = CN_Options_CreateBlank();
    int i;
    for(i = 0; i < argc; i++) {
        /* yeah, I know, OK */
        if(strcmp(argv[i], "--cnmode") == 0 && argc > i+1) {
            options->mode = atoi(argv[i+1]);
        } else if(strcmp(argv[i], "--cnmodeflags") == 0 && argc > i+1) {
            options->modeFlags = atoi(argv[i+1]);
        } else if(strcmp(argv[i], "--cnwidth") == 0 && argc > i+1) {
            options->width = atoi(argv[i+1]);
        } else if(strcmp(argv[i], "--cnheight") == 0 && argc > i+1) {
            options->height = atoi(argv[i+1]);
        } else if(strcmp(argv[i], "--cnloglevel") == 0 && argc > i+1) {
            options->loggingLevel = atoi(argv[i+1]);
        } else if(strcmp(argv[i], "--cnmusictype") == 0 && argc > i+1) {
            options->musicType = atoi(argv[i+1]);
        }
    }
    return options;
}

CN_Options *CN_Options_FromCommandLineArgs(int argc, char **argv)
{
    if(argc < 3) return NULL;
    return _processArgs(argc, argv);
}

void CN_Options_Free(CN_Options *options)
{
    CN_free(options);
}

#ifndef CN_RGB_Set_DEFINED
void CN_RGB_Set(CN_RGB * palette)
{
    CN_RGB_SetRange(palette, 0, 255);
}
#endif

#ifdef CN_NO_INLINE

#if (defined CN_SDL2) || (defined CN_SDL1_2)
#include "shared/sdlinl.h"
#endif

#endif
