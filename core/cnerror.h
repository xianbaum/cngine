/**
 * @file cnerror.h
 * @author Christian Michael Baum
 * @brief Error checking and logging
 **/

#ifndef CN_ERROR_H_
#define CN_ERROR_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#ifndef NO_TIME
#include <time.h>
#endif

#include "../common/cnbool.h"
#include "chelpers.h"

/** @brief All logging levels flagged together, except the log to file flag */
#define CN_LOGGING_LEVEL_ALL (1 | 2 | 4 | 8 | 16 | 32)

/** @brief Enum flags logging level to display */
typedef enum
{
    CN_LOGGING_LEVEL_NONE = 0, /**< Do not log */
    CN_LOGGING_LEVEL_TRACE = 1, /**< If flagged, log trace */
    CN_LOGGING_LEVEL_DEBUG = 2, /**< If flagged, log debug */
    CN_LOGGING_LEVEL_INFO = 4, /**< If flagged, log info */
    CN_LOGGING_LEVEL_WARNING = 8, /**< If flagged, log warning */
    CN_LOGGING_LEVEL_ERROR = 16, /**< If flagged, log error */
    CN_LOGGING_LEVEL_CRITICAL = 32, /**< If flagged, log critical */
    CN_LOGGING_LEVEL_LOAD_FROM_FILE = 4096 /**< If flagged export log to file*/
} CN_LoggingLevel;

#ifndef CN_DEFINED_LOGGING_LEVEL
/**
 * @brief During compilatin, this is can be used to enable/disable logging.
 * Default: CN_LOGGING_LEVEL_ALL */
#define CN_DEFINED_LOGGING_LEVEL CN_LOGGING_LEVEL_ALL
#endif /*CN_DEFINED_LOGGING_LEVEL*/


#ifndef CN_DEBUG

#ifdef CN_DISABLE_ERROR_CHECKING
#define CN_Expect(a,b)
#define CN_Assert(a)
#define CN_Try(a,b) (a)
#define CN_Check(a) (a)
#else /*CN_DISABLE_ERROR_CHECKING*/

/**
 * @brief Similar to assert, exits with an error message on condition fail. 
 * Do not call directly as it may not be there. Use @ref CN_Expect instead.
 * @param condition The condition that must pass
 * @param message A special message to display upon failure
 * @see CN_Expect
 */
void CN__expect(CN_bool condition, const char * message);

/**
 * @brief Similar to assert, exits with an error message on condition fail. 
 * Do not call directly as it may not be there. Use @ref CN_Assert instead.
 * @param condition The condition that must pass
 * @see CN_Assert
 */
void CN__assert(/*@sef@*/ CN_bool condition);

/**
 * @brief Logs an error message on condition fail and continues
 * Do not call directly as it may not be there. Use @ref CN_Check instead.
 * @param condition The condition that must pass
 * @see CN_Check
 */
CN_bool CN__check(CN_bool condition);

/**
 * @brief Logs an error message on condition fail and continues
 * Do not call directly as it may not be there. Use @ref CN_Try instead.
 * @param condition The condition that must pass
 * @param message A special message to display upon failure
 * @see CN_Try
 */
CN_bool CN__try(CN_bool condition, const char * message);

/**
 * @brief Similar to assert, exits with an error message on condition fail
 *
 * Parenthesis and parameters containing condition and message are expected
 * Afterwards. See @ref CN__expect for more info on parameters
 * @see CN__expect
 * @see CN_Try
 * @see CN_Check
 * @see CN_Assert */
#define CN_Expect CN__expect

/**
 * @brief Logs an error message on condition fail and continues
 *
 * Parenthesis and parameters containing condition and message are expected
 * Afterwards. See @ref CN__try for more info on parameters
 * @see CN__try
 * @see CN_Expect
 * @see CN_Check
 * @see CN_Assert */
#define CN_Try CN__try

/**
 * @brief Logs an error message on condition fail and continues
 *
 * Parenthesis and parameters containing condition is expected
 * Afterwards. See @ref CN__check for more info on parameters
 * @see CN__check
 * @see CN_Expect
 * @see CN_Try
 * @see CN_Assert */
#define CN_Check CN__check

/**
 * @brief Similar to assert, exits with an error message on condition fail
 *
 * Parenthesis and parameters containing condition is expected
 * Afterwards. See @ref CN__assert for more info on parameters
 * @see CN__assert
 * @see CN_Expect
 * @see CN_Check
 * @see CN_Try */
#define CN_Assert CN__assert

#endif /*CN_DISABLE_ERROR_CHECKING*/

#else /*CN_DEBUG*/
void CN___expect(CN_bool condition, const char * message,
                 const char * expression);
void CN___assert(CN_bool condition, const char * expression);

CN_bool CN___check(CN_bool condition, const char * expressoin);
CN_bool CN___try(CN_bool condition, const char * message,
                 const char * expression);
#define CN_Expect(condition, message) CN___expect(condition, message,   \
                                                  #condition)
#define CN_Assert(condition) CN___assert(condition, #condition)
#define CN_Check(condition) CN___check(condition, #condition)
#define CN_Try(condition, message) CN___try(condition, message, #condition)
#endif /*CN_DEBUG*/

#if CN_DEFINED_LOGGING_LEVEL == CN_LOGGING_LEVEL_NONE
#define CN_Logger_Init(x) (1)
#define CN_Logger_Destroy()
#define CN_log(x)
#else /*CN_DEFINED_LOGGING_LEVEL == CN_LOGGING_LEVEL_NONE*/

/**
 * @brief Initialization routines for logging.
 * @param loggingLevel The enum flagged logging level to display / log
 */
CN_bool CN_Logger_Init(CN_LoggingLevel loggingLevel);

/** @brief Clean up for logging routines */
void CN_Logger_Destroy(void);

/**
 * @brief Adds a trace log. Do not call directly.
 * Use @ref CN_Log instead
 * @param loggingLevel The logging level
 * @param format the format string
 * @param ... The arguments corresponding to the format string.
 * @see CN_Log
 */
void CN__log(CN_LoggingLevel loggingLevel, const char *format,
             ...);

/**
 * @brief Macro for logging, or nothing if compiled with logging off.
 * See @ref CN__log for parenthetical parameter expansion
 * @param x Parenthetical expansion for logging level,
 * string and format. Wrap in parenthesis.
 * @see CN__log
 * @see CN_LogTrace
 * @see CN_LogDebug
 * @see CN_LogInfo
 * @see CN_LogWarning
 * @see CN_LogError
 * @see CN_LogCritical
 */
#define CN_Log(x) CN__log x
#endif /*CN_DEFINED_LOGGING_LEVEL == CN_LOGGING_LEVEL_NONE*/

#if (CN_DEFINED_LOGGING_LEVEL & 1) == 1 /*CN_LOGGING_LEVEL_TRACE*/

/**
 * @brief Adds a trace log. Do not call directly.
 * Use @ref CN_LogTrace instead
 * @param format the format string
 * @param ... The arguments corresponding to the format string.
 * @see CN_LogTrace
 */
void CN__log_trace(const char *format, ...);

/**
 * @brief Macro for logging trace, or nothing if compiled with logging off.
 * See @ref CN__log_trace for parenthetical parameter expansion
 * @param x Parenthetical expansion for string and format. Wrap in parenthesis.
 * @see CN__log_trace
 * @see CN_Log
 */
#define CN_LogTrace(x) CN__log_trace x
#else /*CN_LOGGING_LEVEL_TRACE*/
#define CN_LogTrace(x)
#endif /*CN_LOGGING_LEVEL_TRACE*/

#if (CN_DEFINED_LOGGING_LEVEL & 4) == 4 /*CN_LOGGING_LEVEL_INFO*/
/**
 * @brief Adds an information log. Do not call directly.
 * Use @ref CN_LogInfo instead
 * @param format the format string
 * @param ... The arguments corresponding to the format string.
 * @see CN_LogInfo
 */
void CN__log_info(const char *format, ...);

/**
 * @brief Macro for logging info, or nothing if compiled with logging off.
 * See @ref CN__log_info for parenthetical parameter expansion
 * @param x Parenthetical expansion for string and format. Wrap in parenthesis.
 * @see CN__log_info
 * @see CN_Log
 */
#define CN_LogInfo(x) CN__log_info x
#else /*CN_LOGGING_LEVEL_INFO*/
#define CN_LogInfo(x)
#endif /*CN_LOGGING_LEVEL_INFO*/

#if (CN_DEFINED_LOGGING_LEVEL & 2) == 2 /*CN_LOGGING_LEVEL_DEBUG*/

/**
 * @brief Adds a debug log. Do not call directly.
 * Use @ref CN_LogDebug instead.
 * @param format the format string
 * @param ... The arguments corresponding to the format string.
 * @see CN_LogDebug
 */
void CN__log_debug(const char *format, ...);

/**
 * @brief Macro for logging debug, or nothing if compiled with logging off.
 * See @ref CN__log_debug for parenthetical parameter expansion
 * @param x Parenthetical expansion for string and format. Wrap in parenthesis.
 * @see CN__log_debug
 * @see CN_Log
 */
#define CN_LogDebug(x) CN__log_debug x

#else /*CN_LOGGING_LEVEL_DEBUG*/
#define CN_LogDebug(x)
#endif /*CN_LOGGING_LEVEL_DEBUG*/

#if (CN_DEFINED_LOGGING_LEVEL & 8) == 8 /*CN_LOGGING_LEVEL_WARNING*/

/**
 * @brief Adds a Warning log. Do not call directly.
 * Use @ref CN_LogWarning instead.
 * @param format the format string
 * @param ... The arguments corresponding to the format string.
 * @see CN_LogWarning
 * @see CN_Log
 */
void CN__log_warning(const char *format, ...);

/**
 * @brief Macro for logging warnings, or nothing if compiled with logging off.
 * See @ref CN__log_warning for parenthetical parameter expansion
 * @param x Parenthetical expansion for string and format. Wrap in parenthesis.
 * @see CN__log_warning
 * @see CN_Log
 */
#define CN_LogWarning(x) CN__log_warning x

#else /*CN_LOGGING_LEVEL_WARNING*/
#define CN_LogWarning(x)
#endif /*CN_LOGGING_LEVEL_WARNING*/

#if (CN_DEFINED_LOGGING_LEVEL & 16) == 16 /*CN_LOGGING_LEVEL_ERROR*/

/**
 * @brief Adds an error log. Do not call directly.
 * Use @ref CN_LogError instead.
 * @param format the format string
 * @param ... The arguments corresponding to the format string.
 * @see CN_LogError
 */
void CN__log_error(const char *format, ...);

/**
 * @brief Macro for logging errors, or nothing if compiled with logging off.
 * See @ref CN__log_error for parenthetical parameter expansion
 * @param x Parenthetical expansion for string and format. Wrap in parenthesis.
 * @see CN__log_error
 * @see CN_Log
 */
#define CN_LogError(x) CN__log_error x
#else /*CN_LOGGING_LEVEL_ERROR*/
#define CN_LogError(x)
#endif /*CN_LOGGING_LEVEL_ERROR*/

#if (CN_DEFINED_LOGGING_LEVEL & 32) == 32 /*CN_LOGGING_LEVEL_CRITICAL*/

/**
 * @brief Adds a critical log. Do not call directly.
 * Use @ref CN_LogCritical instead.
 * @param format the format string
 * @param ... The arguments corresponding to the format string.
 * @see CN_LogCritical
 */
void CN__log_critical(const char *format, ...);

/**
 * @brief Macro for logging critcal, or nothing if compiled with logging off.
 * See @ref CN__log_critical for parenthetical parameter expansion
 * @param x Parenthetical expansion for string and format. Wrap in parenthesis.
 * @see CN__log_critical
 * @see CN_Log
 */
#define CN_LogCritical(x) CN__log_critical x
#else /*CN_LOGGING_LEVEL_CRITICAL*/
#define CN_LogCritical(x)
#endif /*CN_LOGGING_LEVEL_CRITICAL*/

#endif /* CN_ERROR_H_ */
