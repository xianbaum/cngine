#include "chelpers.h"
#include "cnerror.h"

#define MAX_DIR_LENGTH 10
#define MAX_FILESIZE_0x50000 0x50000
static char main_dir[MAX_DIR_LENGTH] = "";

int CN_int_scale(int const initial_size, int const percentage)
{
    return initial_size * ((percentage * 1000) / 10) / 10000;
}

int CN_int_ScaleRoundingUp(int const initial_size, int const percentage)
{
    return ((initial_size * ((percentage * 1000) / 10)) + (CN_int_GetSign(initial_size) * (10000 - 1))) / 10000;
}

int CN_int_GetSign(int number)
{
    if(number == 0)
    {
        return 0;
    }
    return number / abs(number);
}

void *CN_null_return(void)
{
    return NULL;
}

void CN_NoOp(void)
{
}
#ifdef CN_OVERRIDE_CHELPERS_FOPEN
FILE *CN_fopen(const char *filename, const char *mode)
{
    char *buffer = CN_create_temp_buffer(filename);
    int filesize;
    FILE *file = fopen(buffer, mode);
    if(file == NULL) return NULL;
    CN_free_temp_buffer(buffer);
    fseek(file, 0, SEEK_END);
    filesize = ftell(file);
    fseek(file, 0, SEEK_SET);
    CN_Assert(filesize < MAX_FILESIZE_0x50000);
    return file;
}
#endif

char *CN_create_temp_buffer(const char *filename)
{
    char *buffer =
        calloc(strlen(filename) + MAX_DIR_LENGTH, sizeof(char));
    strcat(buffer, main_dir);
    strcat(buffer, filename);
    return buffer;
}
void CN_free_temp_buffer(char *buffer)
{
    free(buffer);
}

void CN_set_directory(const char *directory)
{
    CN_Assert(strlen(directory) <= MAX_DIR_LENGTH);
    strcpy(main_dir, directory);
}
