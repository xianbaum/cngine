#ifndef CN_CSV_H_
#define CN_CSV_H_

#include "../cngine.h"

/* TODO: handle IO errors */

enum CN_FileReadStatus {
    CN_FILEREADSTATUS_NOT_STARTED,
    CN_FILEREADSTATUS_READING,
    CN_FILEREADSTATUS_EOF
};

/** @brief A CSV parser */
struct CN_CsvParser {
    int filepos;
    int status;
    unsigned char bufpos;
    unsigned char read;
    CN_File *file;
    char delimiter;
    char buffer[256];
};


/**
 * @brief Creates a CSV parser based on file
 * 
 * @param file The file to read
 * @param delimiter The delimiter e.g. ',' for comma
 * @returns A newly created CsvParser. You must destory it using CN_CsvParser_Destroy.
 **/
struct CN_CsvParser *CN_CsvParser_CreateFromFile(CN_File *file, char delimiter);

void CN_CsvParser_Destroy(struct CN_CsvParser *parser);

/** 
 * @brief Gets the next char in the CSV parser and advances it
 * 
 * @param parser Pointer to the CSV parser
 * @return First char of csv
 **/
char CN_CsvParser_NextChar(struct CN_CsvParser *parser);

/** 
 * @brief Gets the next int in the CSV parser and advances it
 * 
 * @param parser Pointer to the CSV parser
 * @return Next integer of csv
 **/
int CN_CsvParser_NextInt(struct CN_CsvParser *parser);

/** 
 * @brief Gets the next C string in the CSV parser and advances it
 * 
 * @param parser Pointer to the CSV parser
 * @param out The cstring to output to
 * @return Next C String 
 **/
void CN_CsvParser_NextCStr(struct CN_CsvParser *parser, char *out);

#endif
