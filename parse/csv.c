#include "csv.h"

const int FREAD_SIZE_CHUNK = 256;
/* Returns false on EOF */
static CN_bool UpdateBuffer(struct CN_CsvParser *parser)
{
    if(parser->bufpos < parser->read) {
        return CN_true;
    }
    parser->bufpos = 0;
    parser->read = fread(
        parser->buffer,
        1,
        FREAD_SIZE_CHUNK,
        parser->file);
    if(parser->read == 0) {
        parser->status = CN_FILEREADSTATUS_EOF;
    }
    return parser->read != 0;
}

struct CN_CsvParser *CN_CsvParser_CreateFromFile(CN_File *file, char delimiter)
{
    struct CN_CsvParser *parser = CN_malloc(sizeof(struct CN_CsvParser));
    parser->file = file;
    parser->filepos = 0;
    parser->status = CN_FILEREADSTATUS_NOT_STARTED;
    parser->bufpos = 0;
    parser->read = 0;
    parser->delimiter = delimiter;
    parser->buffer[0] = 0;
    return parser;
}

void CN_CsvParser_Destroy(struct CN_CsvParser *parser)
{
    CN_free(parser);
}

char CN_CsvParser_NextChar(struct CN_CsvParser *parser)
{
    char charToReturn = 0;
    while(UpdateBuffer(parser)) {
        for(; parser->bufpos < parser->read; parser->bufpos++) {
            if(parser->buffer[parser->bufpos] == parser->delimiter) {
                return charToReturn;
            }
            else if(charToReturn == 0) {
                charToReturn = parser->buffer[parser->bufpos];
            }
        }
    }
    return charToReturn;
}

int CN_CsvParser_NextInt(struct CN_CsvParser *parser)
{
    char numbuf[16];
    int numbufpos = 0;
    while(UpdateBuffer(parser)) {
        for(; parser->bufpos < parser->read; parser->bufpos++, numbufpos++) {
            if(parser->buffer[parser->bufpos] == parser->delimiter) {
                numbuf[numbufpos] = 0;
                return atoi(numbuf);
            }
            numbuf[numbufpos] = parser->buffer[parser->bufpos];
        }
    }
    /* TODO: add error code */
    if(numbufpos == 0) {
        return 0;
    }
    return atoi(numbuf);
}

void CN_CsvParser_NextCStr(struct CN_CsvParser *parser, char *out)
{
    int outbufpos = 0;
    while(UpdateBuffer(parser)) {
        for(; parser->bufpos < parser->read; parser->bufpos++, outbufpos++) {
            if(parser->buffer[parser->bufpos] == parser->delimiter) {
                out[outbufpos] = 0;
                return;
            }
            out[outbufpos] = parser->buffer[parser->bufpos];
        }
    }
    if(outbufpos == 0) {
        out[0] = 0;
    }
}
