#ifndef CN_VECTORLIST_H_
#define CN_VECTORLIST_H_

#include "../interf/destruct.h"
#include "../core/chelpers.h"
#include "../common/inline.h"
#include "../common/cnbool.h"

extern const int CN_VECTOR_START_SIZE;

#define CN_DefineList(TYPENAME, TYPE)                                   \
    typedef struct TYPENAME {                                           \
        CN_Destructable base;                                           \
        int size;                                                       \
        int count;                                                      \
        TYPE *data;                                                     \
    } TYPENAME;                                                         \
                                                                        \
    TYPENAME *TYPENAME##_Create(void *allocatedMemory,int size);        \
    int TYPENAME##_Count(TYPENAME *vector);								\
    void TYPENAME##_Add(TYPENAME *vector, TYPE element);                \
    void TYPENAME##_Set(TYPENAME *vector, TYPE element, int index);		\
    TYPE TYPENAME##_Get(TYPENAME *vector, int index);					\
    void TYPENAME##_Remove(TYPENAME *vector, int index);                \
    void TYPENAME##_Free(TYPENAME *vector);                             \
    void TYPENAME##_AddFromArray(TYPENAME *vector, TYPE *array, int count); \
    int TYPENAME##_IndexOf(TYPENAME *vector, TYPE element);

#define CN_ImplementList(TYPENAME, TYPE, EQUALS_FUNC_NAME)              \
    TYPENAME *TYPENAME##_Create(void *allocatedMemory, int size)        \
    {                                                                   \
        TYPENAME *vector;                                               \
        if(allocatedMemory == NULL) vector = CN_malloc(sizeof(TYPENAME)); \
        else vector = allocatedMemory;                                  \
        vector->base.Destruct = (CN_Destruct)TYPENAME##_Free;           \
        vector->count = 0;                                              \
        vector->size = size;                                            \
        if(size > 0) vector->data = CN_malloc(sizeof(TYPE) * size);     \
        else vector->data = NULL;                                       \
        return vector;                                                  \
    }                                                                   \
                                                                        \
    int TYPENAME##_Count(TYPENAME *vector)								\
    {                                                                   \
        return vector->count;                                           \
    }                                                                   \
                                                                        \
    void TYPENAME##_Add(TYPENAME *vector, TYPE element)                 \
    {                                                                   \
        if(vector->size == 0)                                           \
        {                                                               \
            vector->size = CN_VECTOR_START_SIZE;                        \
            vector->data = CN_malloc(sizeof(TYPE) * vector->size);      \
        }                                                               \
        else if(vector->size == vector->count)                          \
        {                                                               \
            vector->size *= 2;                                          \
            vector->data = CN_realloc(vector->data, vector->size*sizeof(TYPE));\
        }                                                               \
        vector->data[vector->count] = element;                          \
        vector->count++;                                                \
    }                                                                   \
                                                                        \
    void TYPENAME##_Set(TYPENAME *vector, TYPE element, int index)		\
    {                                                                   \
        CN_Assert(index < vector->count);                               \
        vector->data[index] = element;                                  \
    }                                                                   \
                                                                        \
    TYPE TYPENAME##_Get(TYPENAME *vector, int index)					\
    {                                                                   \
        CN_Assert(index < vector->count);                               \
        return vector->data[index];                                     \
    }                                                                   \
                                                                        \
    void TYPENAME##_Remove(TYPENAME *vector, int index)                 \
    {                                                                   \
        CN_Assert(index < vector->count);                               \
        for(; index < vector->count-1; index++) {                       \
            vector->data[index] = vector->data[index+1];                \
        }                                                               \
        vector->count--;                                                \
    }                                                                   \
    void TYPENAME##_AddFromArray(TYPENAME *vector, TYPE *array, int count) \
    {                                                                   \
        if(array == NULL) return;                                       \
        if(vector->size == 0) {                                         \
            vector->data = CN_malloc(sizeof(TYPE) * vector->size);      \
        }                                                               \
        vector->count += count;                                         \
        if(vector->count > vector->size) {                              \
            count = vector->count;                                      \
            count |= count >> 1;                                        \
            count |= count >> 2;                                        \
            count |= count >> 4;                                        \
            count |= count >> 8;                                        \
            count |= count >> 16;                                       \
            count++;                                                    \
            if(vector->size == 0) vector->data=CN_malloc(count*sizeof(TYPE)); \
            else vector->data = CN_realloc(vector->data,count * sizeof(TYPE)); \
            vector->size = count;                                       \
        }                                                               \
    }                                                                   \
    int TYPENAME##_IndexOf(TYPENAME *vector, TYPE type)                 \
    {                                                                   \
        int i;                                                          \
        for(i = 0; i < vector->count; i++) {                            \
            if(EQUALS_FUNC_NAME(vector->data[i], type)) return i;       \
        }                                                               \
        return -1;                                                      \
    }                                                                   \
    void TYPENAME##_Free(TYPENAME *vector)                              \
    {                                                                   \
        if(vector->data != NULL) CN_free(vector->data);                 \
        CN_free(vector);                                                \
    }

#define  CN_DefineListDestruct(TYPENAME)                                \
    void TYPENAME##_DestructAll(TYPENAME *vector);                      \
    void TYPENAME##_FreeAll(TYPENAME *vector, CN_void_freer freer);

/*
  Implemented as the first thing any has in its base is a destructor.
  This makes things easier.
*/
#define CN_ImplementListDestruct(TYPENAME)                              \
    void TYPENAME##_DestructAll(TYPENAME *vector)                       \
    {                                                                   \
        int i;                                                          \
        for(i = vector->count-1; i > 0; i--) {                          \
            if(vector->data[i] != NULL) {                               \
              ((CN_Destructable*)vector->data[i])->Destruct(vector->data[i]); \
            }                                                           \
        }                                                               \
        CN_free(vector->data);                                          \
        CN_free(vector);                                                \
    }                                                                   \
    void TYPENAME##_FreeAll(TYPENAME *vector, CN_void_freer freer)      \
    {                                                                   \
        int i;                                                          \
        for(i = vector->count-1; i > 0; i--) {                          \
            if(freer == NULL) CN_free(vector->data[i]);                 \
            else freer(vector->data[i]);                                \
        }                                                               \
        CN_free(vector->data);                                          \
        CN_free(vector);                                                \
    }

CN_DefineList(CN_VectorList, void*)
CN_DefineListDestruct(CN_VectorList)

#endif /*CN_VECTORLIST_H_*/
