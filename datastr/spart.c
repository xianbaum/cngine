#ifndef CN_SPART_C_
#define CN_SPART_C_

#include "spart.h"

void CN_SparttableRect_CheckCollision_Callback(
    CN_SpartGrid *grid, CN_Sparttable *a, CN_Sparttable *b)
{
    struct CN_SparttableRect *aR = (struct CN_SparttableRect*)a,
        *bR = (struct CN_SparttableRect*)b;
    if(aR->rect.x < bR->rect.x + bR->rect.w &&
       aR->rect.x + aR->rect.w > bR->rect.x &&
       aR->rect.y < bR->rect.y + bR->rect.h &&
       aR->rect.y + aR->rect.h > bR->rect.y) {
        a->onCollision(grid, b);
    }
}

void CN_Sparttable_Insert(CN_SpartGrid *grid, CN_Rect bounds, void *existing)
{
    int x = bounds.x / grid->cellW,
        y = bounds.y / grid->cellH;
    while(x * grid->cellW < bounds.x+bounds.w) {
        while(y * grid->cellH < bounds.y+bounds.h) {
            /*TODO
//            cells[y*w+x] insert cell*/
        }
    }
          
}

struct CN_SparttableLL *CN_SparttableRect_GetCell(
    CN_SpartGrid *grid, int time)
{
    
}

void CN_SparttableRect_CheckCollision(CN_SpartGrid *grid, CN_Sparttable *a)
{
    
}

#endif
