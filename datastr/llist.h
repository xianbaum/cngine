/**
 * @file chelpers.h
 * @author Christian Michael Baum
 * @brief Linked list implementation
 **/

#ifndef LLIST_H_
#define LLIST_H_

#include <stdio.h>

#include "../core/cnerror.h"
#include "../core/cnmem.h"

typedef struct CN_LinkedNode CN_LinkedNode;
typedef struct CN_DoubleNode CN_DoubleNode;

struct CN_LinkedNode
{
    CN_LinkedNode *next;
    void *data;
};

struct CN_DoubleNode
{
    CN_DoubleNode *next;
    CN_DoubleNode *prev;
    void *data;
};

CN_LinkedNode *CN_LinkedNode_create(void  *data);
CN_LinkedNode *CN_LinkedNode_shift(CN_LinkedNode *last_node,
                                   CN_LinkedNode *node);
CN_LinkedNode *CN_LinkedNode_push(CN_LinkedNode *first_node,
                                  CN_LinkedNode *node);
void CN_LinkedNode_delete_next(CN_LinkedNode *prev,
                               CN_LinkedNode *node_to_delete);
void CN_LinkedNode_delete_no_references(CN_LinkedNode *node);
void CN_LinkedNode_send_to_back(CN_LinkedNode *back_node,
                                CN_LinkedNode *prev_node,
                                CN_LinkedNode *node_to_send);
void CN_LinkedNode_send_to_front(CN_LinkedNode *first_node,
                                 CN_LinkedNode *prev_node,
                                 CN_LinkedNode *node_to_send);
CN_DoubleNode *CN_DoubleNode_create(void *data);
void CN_DoubleNode_place_after(CN_DoubleNode *prev_node,
                               CN_DoubleNode *node);
void CN_DoubleNode_place_before(CN_DoubleNode *next_node,
                                CN_DoubleNode *node);
void CN_DoubleNode_send_to_back(CN_DoubleNode *back_node,
                                CN_DoubleNode *node_to_send);
void CN_DoubleNode_send_to_front(CN_DoubleNode *first_node,
                                 CN_DoubleNode *node_to_send);
void CN_DoubleNode_delete(CN_DoubleNode *node);

#endif /*LLIST_H_*/

