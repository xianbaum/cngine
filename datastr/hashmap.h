/**
 * @file hashmap.h
 * @author Christian Michael Baum
 * @brief Hashmap generic definition
 **/

#ifndef CN_HASHMAP_H
#define CN_HASHMAP_H

#include <string.h>
#include <stdlib.h>

#include "../core/cnmem.h"
#include "../core/cnerror.h"
#include "../core/chelpers.h"
#include "../interf/destruct.h"
#include "../common/cnbool.h"
#include "../common/inline.h"

/* Based on Robert Sedgewick's LinearProbingHashST */
/** @brief Defines a hashmap. */
#define CN_DefineHashMap(TYPENAME, KEYTYPE, VALUETYPE)                  \
    typedef struct TYPENAME {                                           \
        CN_Destructable base;                                           \
        int count;                                                      \
        unsigned int size;                                              \
        KEYTYPE *keys;                                                  \
        VALUETYPE *values;                                              \
    } TYPENAME;                                                         \
    TYPENAME *TYPENAME##_Create(void *allocatedMemory, int size);       \
    CN_inline int TYPENAME##_Size(TYPENAME *hashmap);                   \
    CN_inline CN_bool TYPENAME##_Contains(TYPENAME *hashmap, KEYTYPE key); \
    void TYPENAME##_Put(TYPENAME *hashmap, KEYTYPE key, VALUETYPE val); \
    VALUETYPE TYPENAME##_Get(TYPENAME *hashmap, KEYTYPE key);           \
    void TYPENAME##_Remove(TYPENAME *hashmap, KEYTYPE key);             \
    void TYPENAME##_Free(TYPENAME *hashmap);

#define CN_ImplementHashMap(TYPENAME, KEYTYPE, VALUETYPE,               \
                            HASH_FUNC_NAME, EQUALS_FUNC_NAME, KEYNIL, VALNIL) \
    TYPENAME *TYPENAME##_Create(void *allocatedMemory, int size)        \
    {                                                                   \
        TYPENAME *hashmap;                                              \
        unsigned int i;                                                 \
        if(allocatedMemory == NULL) hashmap = CN_malloc(sizeof(TYPENAME)); \
        else hashmap = allocatedMemory;                                 \
        hashmap->base.Destruct = (CN_Destruct)TYPENAME##_Free;          \
        hashmap->count = 0;                                             \
        hashmap->size = size;                                           \
        if(size > 0) {                                                  \
            hashmap->keys = CN_malloc(size * sizeof(KEYTYPE*));         \
            for(i = size-1; i != (unsigned int)-1; i--) {               \
                hashmap->keys[i] = KEYNIL;                              \
            }                                                           \
            hashmap->values = CN_malloc(size * sizeof(VALUETYPE*));     \
            for(i = size-1; i != (unsigned int)-1; i--) {               \
                hashmap->values[i] = VALNIL;                            \
            }                                                           \
        } else {                                                        \
            hashmap->keys = NULL;                                       \
            hashmap->values = NULL;                                     \
        }                                                               \
        return hashmap;                                                 \
    }                                                                   \
                                                                        \
    CN_inline int TYPENAME##_Count(TYPENAME *hashmap)                   \
    {                                                                   \
        return hashmap->count;                                          \
    }                                                                   \
                                                                        \
    CN_inline CN_bool TYPENAME##_Contains(TYPENAME *hashmap, KEYTYPE key) \
    {                                                                   \
        return EQUALS_FUNC_NAME(TYPENAME##_Get(hashmap, key), VALNIL);  \
    }                                                                   \
                                                                        \
    static void TYPENAME##_Resize(TYPENAME *hashmap, int newSize)       \
    {                                                                   \
        KEYTYPE *keysTemp = hashmap->keys;                              \
        VALUETYPE *valsTemp = hashmap->values;                          \
        int i, oldSize = hashmap->size;                                 \
        hashmap->size = newSize;                                        \
        hashmap->keys = CN_malloc(hashmap->size * sizeof(KEYTYPE*));    \
        for(i = 0; i < hashmap->size; i++) {                            \
            hashmap->keys[i] = KEYNIL;                                  \
        }                                                               \
        hashmap->values = CN_malloc(hashmap->size * sizeof(VALUETYPE*));\
        for(i = 0; i < hashmap->size; i++) {                            \
            hashmap->values[i] = VALNIL;                                \
        }                                                               \
        for(i = 0; i < oldSize; i++) {                                  \
            if(keysTemp[i] != KEYNIL) {                                 \
                hashmap->count--;                                       \
                TYPENAME##_Put(hashmap, keysTemp[i], valsTemp[i]);      \
            }                                                           \
        }                                                               \
        CN_free(keysTemp);                                              \
        CN_free(valsTemp);                                              \
    }                                                                   \
                                                                        \
    void TYPENAME##_Put(TYPENAME *hashmap, KEYTYPE key, VALUETYPE val)  \
    {                                                                   \
        if(hashmap->count >= hashmap->size/2) {                         \
            TYPENAME##_Resize(hashmap,                                  \
                              hashmap->size <= 0 ? 8 : hashmap->size*2); \
        }                                                               \
        {unsigned int i;                                                \
            for(i = HASH_FUNC_NAME(key) % hashmap->size;                \
                hashmap->keys[i] != KEYNIL;                             \
                i = (i + 1) % hashmap->size) {                          \
                if(EQUALS_FUNC_NAME(key, hashmap->keys[i])) {           \
                    hashmap->values[i] = val;                           \
                    return;                                             \
                }                                                       \
            }                                                           \
            hashmap->keys[i] = key;                                     \
            hashmap->values[i] = val;                                   \
        }                                                               \
        hashmap->count++;                                               \
    }                                                                   \
                                                                        \
    VALUETYPE TYPENAME##_Get(TYPENAME *hashmap, KEYTYPE key)            \
    {                                                                   \
        unsigned int i;                                                 \
        for(i = HASH_FUNC_NAME(key) % hashmap->size;                    \
            hashmap->keys[i] != KEYNIL; i = (i+1)%hashmap->size) {      \
            if(EQUALS_FUNC_NAME(hashmap->keys[i], key)) {               \
                return hashmap->values[i];                              \
            }                                                           \
        }                                                               \
        return (VALUETYPE)VALNIL;                                       \
    }                                                                   \
                                                                        \
    void TYPENAME##_Remove(TYPENAME *hashmap, KEYTYPE key)              \
    {                                                                   \
        if(EQUALS_FUNC_NAME(TYPENAME##_Get(hashmap, key), VALNIL)) return; \
        {unsigned int i = HASH_FUNC_NAME(key) % hashmap->size;          \
            while(!(EQUALS_FUNC_NAME(key, hashmap->keys[i]))) {         \
                i = (i + 1) % hashmap->size;                            \
            }                                                           \
            hashmap->keys[i] = KEYNIL;                                  \
            hashmap->values[i] = VALNIL;                                \
            hashmap->count--;                                           \
            i = (i + 1) % hashmap->size;                                \
            while(hashmap->keys[i] != KEYNIL) {                         \
                KEYTYPE keyToRehash = hashmap->keys[i];                 \
                VALUETYPE valueToRehash = hashmap->values[i];           \
                hashmap->keys[i] = KEYNIL;                              \
                hashmap->values[i] = VALNIL;                            \
                hashmap->count--;                                       \
                TYPENAME##_Put(hashmap, keyToRehash, valueToRehash);    \
                i = (i + 1) % hashmap->size;                            \
            }                                                           \
        }                                                               \
        if(hashmap->count > 0 && hashmap->count <= hashmap->size/8) {   \
            TYPENAME##_Resize(hashmap, hashmap->size/2);                \
        }                                                               \
    }                                                                   \
                                                                        \
    void TYPENAME##_Free(TYPENAME *hashmap)                             \
    {                                                                   \
        if(hashmap == NULL) return;                                     \
        if(hashmap->keys != NULL) CN_free(hashmap->keys);               \
        if(hashmap->values != NULL) CN_free(hashmap->values);           \
        CN_free(hashmap);                                               \
    }


unsigned int CN_HashString(char *string);
CN_bool CN_StringEquals(char* s1, char *s2);
CN_DefineHashMap(CN_StringDictionary, char*, char*)
        
#endif /*CN_HASHMAP_H*/
