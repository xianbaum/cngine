#include "uitree.h"

static CN_inline CN_bool
CN_UIRectNode_PointCollision(struct CN_UINode *node, int x, int y)
{
    return CN_Rect_PointCollision(((struct CN_UIRectNode*)node)->rect, x, y);
}

static CN_bool CN_UINode_Collision(struct CN_UINode *node, int x, int y)
{
    switch(node->type) {
    case CN_UINODETYPE_RECT:
       return CN_UIRectNode_PointCollision(node, x, y);
    case CN_UINODETYPE_ROOT:
        return CN_true;
    default:
        return CN_false;
    }
}

/* On hold */
/*struct CN_UINode *CN_UINode_DeserializeAndAllocate(char *data)
{
    
    return NULL;
}


char *CN_UINode_Serialize(struct CN_UINode *root, char *str)
{
    if(root == NULL) return NULL;
    if(str == NULL) {
        str = CN_malloc(sizeof(char)*10000);
    }
}*/

static const int delayMS = 13;
static int ticks = 0;
static CN_Point lastMousePos;

void CN_MouseEvent_Detect()
{
    if(ticks > CN_GetMilliseconds() / delayMS) {
        
    }
}

void CN_UINode_ProcessMouseEvent(
    struct CN_UINode *node, struct CN_MouseEvent *event)
{
    if(node->eventCount > 0 &&
       CN_UINode_Collision(node, event->x, event->y)) {
        int i;
        for(i = 0; i < node->childCount; i++) {
            CN_UINode_ProcessMouseEvent(node->children+i, event);
        }
        for(i = 0; i < node->eventCount; i++) {
            /* (node->subscribers + i)(event, node->data[i]); */
        }
    }
}
