#include "llist.h"

CN_LinkedNode *CN_LinkedNode_create( void *data)
{
    CN_LinkedNode *node = CN_malloc(sizeof(CN_LinkedNode));
    node->next = NULL;
    node->data = data;
    return node;
}

CN_LinkedNode *CN_LinkedNode_shift(CN_LinkedNode *last_node,
                                   CN_LinkedNode *node)
{
    CN_Assert(last_node != NULL && node != NULL);
    last_node->next = node;
    return node;
}

CN_LinkedNode *CN_LinkedNode_push(CN_LinkedNode *first_node,
                                  CN_LinkedNode *node)
{
    CN_Assert(first_node != NULL && node != NULL);
    node->next = first_node;
    return node;
}

void CN_LinkedNode_delete_next(CN_LinkedNode *prev,
                               CN_LinkedNode *node_to_delete)
{
    CN_Assert(prev != NULL && node_to_delete != NULL);
    prev->next = node_to_delete->next;
    free(node_to_delete);
}

void CN_LinkedNode_delete_no_references(CN_LinkedNode *node)
{
    CN_Assert(node != NULL);
    if(node->next != NULL)
    {
        node->data = node->next->data;
        free(node->next);
    }
    else
    {
        free(node);
    }
}

void CN_LinkedNode_send_to_back(CN_LinkedNode *back_node,
                                CN_LinkedNode *prev_node,
                                CN_LinkedNode *node_to_send)
{
    CN_Assert(back_node != NULL &&
              prev_node != NULL &&
              node_to_send != NULL);
    CN_Assert(prev_node->next == node_to_send);
    prev_node->next = node_to_send->next;
    back_node->next = node_to_send;
    node_to_send->next = NULL;
}

void CN_LinkedNode_send_to_front(CN_LinkedNode *first_node,
                                 CN_LinkedNode *prev_node,
                                 CN_LinkedNode *node_to_send)
{
    CN_Assert(first_node != NULL &&
              prev_node != NULL &&
              node_to_send != NULL);
    CN_Assert(prev_node->next == node_to_send);
    prev_node->next = node_to_send->next;
    node_to_send->next = first_node;

}

CN_DoubleNode *CN_DoubleNode_create(void *data)
{
    CN_DoubleNode *node = CN_malloc(sizeof(CN_DoubleNode));
    node->next = NULL;
    node->prev = NULL;
    node->data = data;
    return node;
}

void CN_DoubleNode_place_after(CN_DoubleNode *prev_node,
                               CN_DoubleNode *node)
{
    CN_Assert(prev_node != NULL && node != NULL);
    if(prev_node->next != NULL)
    {
        prev_node->next->prev = node;
    }
    node->next = prev_node->next;
    node->prev = prev_node;
    prev_node->next = node;
}

void CN_DoubleNode_place_before(CN_DoubleNode *next_node,
                                CN_DoubleNode *node)
{
    CN_Assert(next_node != NULL && node != NULL);
    if(next_node->prev != NULL)
    {
        next_node->prev->next = node;
    }
    node->prev = next_node->prev;
    node->next = next_node;
    next_node->prev = node;
}

void CN_DoubleNode_send_to_back(CN_DoubleNode *back_node,
                                CN_DoubleNode *node_to_send)
{
    CN_Assert(back_node != NULL &&
              node_to_send != NULL);
    if(node_to_send->prev != NULL)
    {
        node_to_send->prev->next = node_to_send->next;
    }
    if(node_to_send->next != NULL)
    {
        node_to_send->next->prev = node_to_send->prev;
    }
    back_node->next = node_to_send;
    node_to_send->next = NULL;
    node_to_send->prev = back_node;
}

void CN_DoubleNode_send_to_front(CN_DoubleNode *first_node,
                                 CN_DoubleNode *node_to_send)
{
    CN_Assert(first_node != NULL &&
              node_to_send != NULL);
    if(node_to_send->prev != NULL)
    {
        node_to_send->prev->next = node_to_send->next;
    }
    if(node_to_send->next != NULL)
    {
        node_to_send->next->prev = node_to_send->prev;
    }
    first_node->prev = node_to_send;
    node_to_send->next = first_node;
    node_to_send->prev = NULL;
}

void CN_DoubleNode_delete(CN_DoubleNode *node)
{
    CN_Assert(node != NULL);
    if(node->next != NULL)
    {
        node->next->prev = node->prev;
    }
    if(node->prev != NULL)
    {
        node->prev->next = node->next;
    }
    CN_free(node);
}
