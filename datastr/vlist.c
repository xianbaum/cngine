#include "vlist.h"
#include <stdlib.h>
#include "../core/cnmem.h"

const int CN_VECTOR_START_SIZE = 4;

static CN_inline CN_bool CN_VectorList_Equals(void *v1, void *v2)
{
    return v1 == v2;
}

CN_ImplementList(CN_VectorList, void*, CN_VectorList_Equals)
CN_ImplementListDestruct(CN_VectorList)
