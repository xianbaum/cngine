/**
 * @file spart.h
 * @author Christian Michael Baum
 * @brief Contains definitions and functions for spatial partitioning
 **/

#ifndef CN_SPART_H_
#define CN_SPART_H_

#include "stddef.h"
#include "../common/cnrect.h"

typedef struct CN_Sparttable CN_Sparttable;
typedef struct CN_SpartGrid CN_SpartGrid;

typedef void (*CN_SpartOnCollisionCallback)(CN_SpartGrid*, CN_Sparttable*);
typedef void (*CN_SpartCheckCollisionCallback)(
    CN_SpartGrid *,CN_Sparttable*, CN_Sparttable*);
typedef struct CN_SparttableLL *(*CN_SpartGetCell)(CN_SpartGrid *grid, int time);


struct CN_Sparttable {
    CN_SpartOnCollisionCallback onCollision;
    CN_SpartCheckCollisionCallback checkCollision;
    CN_SpartGetCell getCell;
};

struct CN_SparttableLL {
    CN_Sparttable *sparttable;
    struct CN_SparttableLL *next;
};

struct CN_SparttableRect {
    CN_Sparttable base;
    CN_Rect rect;
};

#define CN_Sparttable_To(casting) (casting->parttable)
#define CN_Sparttable_From(type, sparttable) \
    ((type*)((char*)sparttable - offsetof(type, destructable)))


struct CN_SpartGrid {
    int w, h, cellW, cellH, cellCount;
    struct CN_Sparttable_LL *cells;
};

void CN_SpatCell_CheckCollision_All();

#endif
