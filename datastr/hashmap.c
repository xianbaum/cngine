#include "hashmap.h"

CN_inline unsigned int CN_HashString(char *string)
{
    int hash = 5381;
    int c;
    while(c = *string++) {
        hash = ((hash << 5) + hash) + c;
    }
    return hash;
}

CN_inline CN_bool CN_StringEquals(char* s1, char *s2)
{
    if(s2 == NULL) return CN_false;
    return strcmp(s1, s2) == 0;
}

CN_ImplementHashMap(CN_StringDictionary, char*, char*, CN_HashString,
                    CN_StringEquals, NULL, NULL)
