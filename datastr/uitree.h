/**
 * @file uitree.h
 * @author Christian Michael Baum
 * @brief Contains data structures and functions for UI and mouse
 **/

#ifndef CN_UITREE_H_
#define CN_UITREE_H_

#include "../cngine.h"

enum CN_MouseEventType {
    CN_MOUSEEVENT_MOUSEOVER,
    CN_MOUSEEVENT_MOUSEOUT,
    CN_MOUSEEVENT_DOWN,
    CN_MOUSEEVENT_UP,
    CN_MOUSEEVENT_CLICK
};

struct CN_MouseEvent
{
    enum CN_MouseEventType type;
    int x;
    int y;
};

enum CN_UINodeType {
    CN_UINODETYPE_ROOT,
    CN_UINODETYPE_RECT,
};

typedef void (*CN_MouseEventCallback)(struct CN_MouseEvent event, void *data);

struct CN_UINode {
    enum CN_UINodeType type;
    unsigned char childCount;
    unsigned char eventCount;
    struct CN_UINode *children;
    void *data;
    CN_MouseEventCallback *subscribers;
};

struct CN_UIRectNode {
    struct CN_UINode base;
    CN_Rect rect;
};

int RegisterClickEvent(
    struct CN_UINode *node,
    int (*subscriber)(struct CN_MouseEvent event));


#endif
