/**
 * @file anim.h
 * @author Christian Michael Baum
 * @brief Animation functions and helpers
 **/

#ifndef ANIMATION_H_
#define ANIMATION_H_

#include "../cngine.h"
#include "artmgr.h"

#define CN_2DAnimPlayData_Reset(anim) anim.current_frame = 0;anim.timer = 0

typedef struct CN_2DFrame
{
    CN_Rect framebox;
    CN_Rect hitbox;
    CN_Rect hurtbox;
    unsigned short frametime;
    char x_offset;
    char y_offset;
} CN_2DFrame;

typedef struct CN_2DAnim
{
    CN_Rect colbox;
    CN_Art *art;
    CN_2DFrame *frames;
    unsigned char framecount;
} CN_2DAnim;

typedef struct CN_2DAnimPlayData
{
    unsigned char current_frame;
    unsigned char padding;
    unsigned short timer;
} CN_2DAnimPlayData;

CN_2DAnim *CN_2DAnim_Load(const char *path, CN_ArtManager *artmgr,CN_Art *art);
void CN_2DAnim_UpdatePlayData(CN_2DAnim *animation, CN_2DAnimPlayData *data,
                              int delta_time);
void CN_2DAnim_Draw(CN_2DAnim *animation, unsigned char current_frame,
                    CN_Rect *dest);
CN_Rect CN_2DAnim_GetHitbox(CN_2DAnim *animation, unsigned char current_frame);
CN_Rect CN_2DAnim_GetFramebox(CN_2DAnim *animation,
                              unsigned char current_frame);
CN_Rect CN_2DAnim_GetHurtbox(CN_2DAnim *animation, unsigned char current_frame);
void CN_2DAnim_DrawScaled(CN_2DAnim *animation, CN_Rect *dest,
                          unsigned char current_frame,
                          int horizon_distance,
                          int scale_percentage);
void CN_2DAnim_Free(CN_2DAnim *anim, CN_ArtManager *artmgr);

typedef CN_2DAnim *CN_2DAnimList;

#endif /*ANIMATION_H_*/
