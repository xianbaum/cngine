/**
 * @file chelpers.h
 * @author Christian Michael Baum
 * @brief Font structure and routines
 **/

#ifndef _FONT_H_
#define _FONT_H_

#include "../cngine.h"

typedef struct CN_Font {
    CN_Rect letters[256];
    CN_Image *image;
} CN_Font;

CN_Font* CN_Font_Create(const char *path, const char *text);
void CN_DrawText(CN_Font *font, const char *text, int x, int y);
void CN_DrawTextFixed(CN_Font *font, const char *text, int x, int y,int length);
void CN_DrawTextConsole(CN_Font *font, const char *text);
void CN_Font_Free(CN_Font *font);

#endif /*_FONT_H_*/
