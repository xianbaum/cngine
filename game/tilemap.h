/**
 * @file tilemap.h
 * @author Christian Michael Baum
 * @brief Definition and functions useful for a common
 * tilemap implementation
 **/

#ifndef _CN_TILEMAP_H_
#define _CN_TILEMAP_H_

#include "../cngine.h"
#include "../core/chelpers.h"
#include "anim.h"

typedef unsigned char CN_Tile;

typedef enum {
    CN_TileMapFlags_IS_OUTSIDE = 1,
    CN_TileMapFlags_HAS_FG = 2,
    CN_TileMapFlags_IS_SNOWING = 4
} CN_TileMapFlags;

typedef enum {
    TOP_DOWN = 0,
    FRONT_VIEW = 1
} CN_PerspectiveModes;

typedef struct CN_TileMapExit
{
    unsigned int baseno;
    int x;
    int y;
    unsigned int mapno;
} CN_TileMapExit;

typedef struct CN_TileMapProperties
{
    unsigned short int map_w;
    unsigned short int map_h;
    unsigned char tile_w;
    unsigned char tile_h;
    unsigned int tileset;
    CN_TileMapFlags flags;
    unsigned char music;
    CN_PerspectiveModes modes;
    unsigned char mode_options;
    int camera_distance;
    int horizon_offset;
    int number_of_exits;
    unsigned int baseno;
    unsigned int mapno;
    int scale;
    CN_TileMapExit *exits;
} CN_TileMapProperties;

typedef enum {
    SQUARE = 0,
    TRIANGLE_TOP_LEFT,
    TRIANGLE_TOP_RIGHT,
    TRIANGLE_BOTTOM_LEFT,
    TRIANGLE_BOTTOM_RIGHT,
    HALFLEFT,
    HALFRIGHT,
    HALFTOP,
    HALFBOTTOM

} CN_TileType;

typedef struct CN_TileMap
{
    CN_TileMapProperties props;
    CN_Image *image;
    CN_Tile *bg;
    CN_Tile *fg;
    CN_Tile *col;
    CN_Callback *map_callback;
    int snow_timer;
} CN_TileMap;

CN_TileMap *CN_TileMap_Load(unsigned int map_num);
int CN_TileMap_LoadProperties(const char *path,
                              CN_TileMapProperties *props);
void CN_TileMapProperties_Print(CN_TileMapProperties prooperties);

CN_Tile *CN_TileMap_LoadTiles(char *path, unsigned short map_w,
                              unsigned short map_h);
void CN_TileMap_DrawLayer(CN_TileMap *map, CN_Rect screen, CN_Tile *tiles);
void CN_TileMap_Free(CN_TileMap *map);

#endif /*_CN_TILEMAP_H_*/
