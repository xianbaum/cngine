/**
 * @file chelpers.h
 * @author Christian Michael Baum
 * @brief Defintions for handling art files in  memory
 **/

#ifndef ART_MANAGER_H_
#define ART_MANAGER_H_

#include "../datastr/vlist.h"
#include "../datastr/hashmap.h"

typedef enum
{
    CN_ROBIN_WALK = 0,
    CN_RONALD_WALK = 1,
    CN_TOM_WALK = 2,
    CN_FIREBALL = 3,
    CN_ROBIN_STRUGGLE = 4,
    CN_SNOW_ART = 5
} CN_AnimArt;

typedef struct CN_Art
{
    void *art;
    int referenceCount;
    int id;
    int size;
    char garbageIndex;
} CN_Art;

CN_DefineHashMap(CN_ArtDictionary, int, CN_Art*)
    
typedef struct CN_ArtManager
{
    CN_ArtDictionary dict;
    int totalSize;
    CN_Art *garbage[8];
} CN_ArtManager;

CN_Art *CN_ArtManager_Get(CN_ArtManager *artmgr, int id);
void CN_ArtManager_Dereference(CN_ArtManager *artmgr, CN_Art *art);
CN_ArtManager *CN_ArtManager_Create(void *allocatedMemory);
void CN_ArtManager_CollectGarbage(CN_ArtManager *artmgr);
void CN_ArtManager_Destroy(CN_ArtManager *artmgr);

#endif /*ART_MANAGER_H_*/
