#include "font.h"

CN_Font *CN_Font_Create(const char *path, const char *charlist)
{
    CN_Font *cn_font = CN_malloc(sizeof(CN_Font));
    int letter_count = (int)strlen(charlist), char_x = 0, image_width,
        image_height, border_color, current_pixel, current_letter = 0;
    unsigned char current_char;
    CN_Rect current_letter_rect;
    if(!CN_Check(cn_font != NULL)) return NULL;
    cn_font->image = CN_Image_Load((char*)path);
    if(!CN_Check(cn_font->image != NULL)) {
        CN_free(cn_font);
        return NULL;
    }
    image_width = CN_Image_GetWidth(cn_font->image);
    image_height = CN_Image_GetHeight(cn_font->image);
    border_color = CN_Image_GetPixel(cn_font->image, 0, 0);
    current_letter_rect.x = 0;
    current_letter_rect.y = 0;
    current_letter_rect.w = 0;
    current_letter_rect.h = image_height;
    for(char_x = 0; char_x < image_width && current_letter < letter_count;
        char_x++) {
        current_pixel = CN_Image_GetPixel(cn_font->image, char_x, 0);
        if(border_color == current_pixel && current_letter_rect.x != 0) {
            current_letter_rect.w = char_x - current_letter_rect.x;
            current_char = (unsigned char)charlist[current_letter];
            memcpy(&cn_font->letters[(int)current_char],
                   &current_letter_rect, sizeof(CN_Rect));
            current_letter_rect.x = 0;
            current_letter++;
        }
        else if(current_letter_rect.x == 0) current_letter_rect.x = char_x;
    }
    return cn_font;
}

void CN_DrawText(CN_Font *cn_font, const char *text, int x, int y)
{
    int i = 0;
    int image_height= CN_Image_GetHeight(cn_font->image);
    unsigned char current_letter;
    CN_Rect current_letter_rect;
    CN_Rect pos = {0, 0, 0, 0};
    for(; text[i] != '\0'; i++) {
        current_letter = (unsigned char)text[i];
        if(text[i] == '\n') {
            y = y + image_height + 1;
			x = 0;
            continue;
        }
        current_letter_rect = cn_font->letters[current_letter];
        CN_Image_DrawRect(
            cn_font->image,CN_GetScreen(), current_letter_rect, x, y);
        x = x + current_letter_rect.w + 1;
    }
}

void CN_DrawTextFixed(CN_Font *cn_font, const char *text, int x, int y,
                      int length)
{
    int i = 0;
    int image_height = CN_Image_GetHeight(cn_font->image);
    unsigned char current_letter;
    CN_Rect current_letter_rect;
    CN_Rect pos = {0, 0, 0, 0};
    for(; text[i] < length; i++) {
        current_letter = (unsigned char)text[i];
        if(text[i] == '\n') {
            y = y + image_height + 1;
			x = 0;
            continue;
        }
        current_letter_rect = cn_font->letters[current_letter];
        CN_Image_DrawRect(
            cn_font->image, CN_GetScreen(), current_letter_rect, x, y);
        x = x + current_letter_rect.w + 1;
    }
}

void CN_DrawTextConsole(CN_Font *cn_font, const char *text)
{
    int i = 0, x = 0, y = 0;
    int image_height = CN_Image_GetHeight(cn_font->image);
    unsigned char current_letter;
    CN_Rect current_letter_rect;
    for(; text[i] != '\0'; i++) {
        current_letter = (unsigned char)text[i];
        current_letter_rect = cn_font->letters[current_letter];
        if(text[i] == '\n' ||
           x + current_letter_rect.w > CN_options->width) {
            y = y + image_height + 1;
			x = 0;
            if(text[i] == '\n') continue;
        }
        CN_Image_DrawRect(
            cn_font->image, CN_GetScreen(), current_letter_rect, x, y);
        x = x + current_letter_rect.w + 1;
    }
}

void CN_Font_Free(CN_Font *cn_font)
{
    if(cn_font == NULL) return;
    if(cn_font->image != NULL) CN_Image_Free(cn_font->image);
    CN_free(cn_font);
}
