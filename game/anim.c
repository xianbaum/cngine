#include "anim.h"

#include <stdio.h>

static char ANIM_FAILED_TO_LOAD[] = "Animation failed to load";

CN_2DAnim *CN_2DAnim_Load(const char *path, CN_ArtManager *artmgr, CN_Art *art)
{
    CN_2DAnim *anim = CN_malloc(sizeof(CN_2DAnim));
    char buffer[256];
    FILE *animation_file = CN_fopen(path, "r");
    char *substr;
    int i;
    anim->frames = NULL;
    CN_Assert(anim != NULL);
    if (!CN_Try(animation_file != NULL, ANIM_FAILED_TO_LOAD))
    {
        goto error;
    }
    if (!CN_Try(fgets(buffer,sizeof(buffer), animation_file) != NULL,
                ANIM_FAILED_TO_LOAD))
    {
        goto error;
    }
    if (buffer[0] != '\n') /*first number ignored if image provided*/
    {
        substr = strtok(buffer, "\n");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->art = CN_ArtManager_Get(artmgr, (int) strtod(substr, NULL));
    }
    else
    {
        anim->art = art;
        art->referenceCount++;
    }
    if (!CN_Try(fgets(buffer,sizeof(buffer), animation_file) != NULL,
                ANIM_FAILED_TO_LOAD))
    {
        goto error;
    }
    anim->framecount = (unsigned char) strtod(buffer, NULL);
    anim->frames = CN_malloc(sizeof(CN_2DFrame) * anim->framecount);
    /*collision box*/
    if (!CN_Try(fgets(buffer,sizeof(buffer), animation_file)!= NULL,
                ANIM_FAILED_TO_LOAD))
    {
        goto error;
    }
    substr = strtok(buffer, ",");
    if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
    {
        goto error;
    }
    anim->colbox.x = (short) strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
    {
        goto error;
    }
    anim->colbox.y = (short) strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
    {
        goto error;
    }
    anim->colbox.w = (short) strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
    {
        goto error;
    }
    anim->colbox.h = (short) strtod(substr, NULL);
    for (i = 0; i < anim->framecount; i++) /*woah*/
    {
        if (!CN_Check(fgets(buffer,sizeof(buffer),animation_file) != NULL))
        {
            goto error;
        }
        substr = strtok(buffer, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].x_offset = (char) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].y_offset = (char) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].frametime = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].framebox.x = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].framebox.y = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].framebox.w = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].framebox.h = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].hurtbox.x = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].hurtbox.y = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].hurtbox.w = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].hurtbox.h = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].hitbox.x = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].hitbox.y = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].hitbox.w = (short) strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Try(substr != NULL, ANIM_FAILED_TO_LOAD))
        {
            goto error;
        }
        anim->frames[i].hitbox.h = (short) strtod(substr, NULL);
    }
    CN_Check(fclose(animation_file) == 0);
    return anim;

error: CN_2DAnim_Free(anim, artmgr);
    CN_Check(fclose(animation_file) == 0);
    return NULL;
}

void CN_2DAnim_UpdatePlayData(CN_2DAnim *animation, CN_2DAnimPlayData *data,
                              int delta_time)
{
    CN_Expect(animation != NULL, "Null animation passed to CN_Anim_GetFrame");
    {
        CN_2DFrame frame = animation->frames[data->current_frame];
        data->timer += delta_time;
        while (data->timer >= frame.frametime)
        {
            data->timer -= frame.frametime;
            data->current_frame++;
            if(data->current_frame >= animation->framecount)
            {
                data->current_frame = 0;
            }
            frame = animation->frames[data->current_frame];
        }
    }
}

void CN_2DAnim_Draw(CN_2DAnim *animation, unsigned char current_frame,
                    CN_Rect *dest)
{
    if (animation == NULL || animation->frames == NULL
        || animation->art == NULL)
    {
        return;
    }
    {
        CN_2DFrame frame = animation->frames[current_frame];
        CN_Image_DrawRectMasked(
            (CN_Image*) animation->art->art,
            CN_GetScreen(),
            frame.framebox,
            dest->x + frame.x_offset,
            dest->y + frame.y_offset);
    }
}

void CN_2DAnim_DrawScaled(CN_2DAnim *animation, CN_Rect *dest,
            unsigned char frameno, int horizon_distance, int scale_percentage)
{

    if (animation == NULL || animation->frames == NULL
        || animation->art == NULL)
    {
        return;
    }
    {
        CN_Rect dest_rect_scaled;
        CN_2DFrame frame = animation->frames[frameno];
        /*hmmmmm*/
        (void) horizon_distance;
        dest_rect_scaled.x = dest->x
            + CN_int_scale(frame.x_offset, scale_percentage);
        dest_rect_scaled.y = dest->y
            + CN_int_scale(frame.y_offset, scale_percentage);
        dest_rect_scaled.w = CN_int_scale(frame.framebox.w, scale_percentage);
        dest_rect_scaled.h = CN_int_scale(frame.framebox.h, scale_percentage);
        CN_Image_DrawScaledMasked(
            (CN_Image*)animation->art->art,
            CN_GetScreen(),
            frame.framebox,
            dest_rect_scaled);
    }
}

CN_Rect CN_2DAnim_GetHitbox(CN_2DAnim *animation, unsigned char frame)
{
    CN_Assert(animation != NULL && animation->frames != NULL);
    return animation->frames[frame].hitbox;
}

CN_Rect CN_2DAnim_GetFramebox(CN_2DAnim *animation, unsigned char frame)
{
    CN_Assert(animation != NULL && animation->frames != NULL);
    return animation->frames[frame].framebox;
}

CN_Rect CN_2DAnim_GetHurtbox(CN_2DAnim *animation, unsigned char frame)
{
    CN_Assert(animation != NULL && animation->frames != NULL);
    return animation->frames[frame].hurtbox;
}

void CN_2DAnim_Free(CN_2DAnim *anim, CN_ArtManager *artmgr)
{
    if (anim == NULL)
    {
        return;
    }
    if (anim->art != NULL)
    {
        CN_ArtManager_Dereference(artmgr, anim->art);
    }
    if (anim->frames != NULL)
    {
        CN_free(anim->frames);
    }
    CN_free(anim);
}
