#include "tilemap.h"

/* Map format (subject to change):
   Folder has 4 files:
   mapinfo.csv
   map_bg.csv
   map_cl.csv
   map_fg.csv
   tiles.pcx
   mapinfo.csv:
   (THIS INFO IS NOT UP TO DATE!)
   map_width,map_height,tile_width,tile_height,tileset num,map flags
   default music track, map perspective mode, mode options
   Example:
   25,25,8,8,1,0,1,0,0

   map perspective modes (subject to change)
   0 earthbound style -- has collision map
   1 monkey island style -- has collision map

   mode options:
   1: depth the depth of the map/how far it takes to travel to the back

   PROPERTIES FILE:
   Width,Height,TileW,TileH,Tileset,Flags,Music,PerspectiveMode,ModeOptions, ->
   CameraDistance,HorizonOffset
   NumberOfExits,Scale
   Exitbaseno,ExitX,ExitY
   ....
*/
CN_TileMap *CN_TileMap_Load(unsigned int map_number)
{
    CN_TileMap *map = CN_malloc(sizeof(CN_TileMap));
    char path[100];
    char art_dir[] = "./art/";
    /*char image_path[] = "imgs/tiles";*/
    char map_dir[] = "maps/";
    /*Set to null*/
    map->bg = NULL;
    map->fg = NULL;
    map->col = NULL;
    /*map props*/
    if (!CN_Check(sprintf(path, "%s%s%u/props.csv", art_dir, map_dir,
                          map_number) > 1))
    {
        goto error;
    }
    map->props.mapno = map_number;
    if (!CN_Check(CN_TileMap_LoadProperties(path, &map->props) == 0))
    {
        goto error;
    }
    if ((map->props.flags & CN_TileMapFlags_HAS_FG) == CN_TileMapFlags_HAS_FG)
    {
        if (!CN_Check(sprintf(path, "%s%s%u/map_fg.csv", art_dir, map_dir,
                              map->props.baseno) > 1))
        {
        }
        map->fg = CN_TileMap_LoadTiles(
            path, map->props.map_w, map->props.map_h);
    }
    if (!CN_Check(sprintf(path, "%s%s%u/map_bg.csv", art_dir, map_dir,
                          map->props.baseno) > 1))
    {
        goto error;
    }
    map->bg = CN_TileMap_LoadTiles(path, map->props.map_w,
                                   map->props.map_h);
    if (!CN_Check(sprintf(path, "%s%s%u/map_col.csv", art_dir, map_dir,
                          map->props.baseno) > 1))
    {
        goto error;
    }
    map->col = CN_TileMap_LoadTiles(path, map->props.map_w, map->props.map_h);
    if (!CN_Check(sprintf(path, "%simages/%u.pcx", art_dir,
                          map->props.tileset) > 1))
    {
        goto error;
    }
    map->image = CN_Image_Load(path);
    if (!CN_Check(map->image != NULL))
    {
        goto error;
    }
    if ((map->props.flags & CN_TileMapFlags_IS_SNOWING) ==
        CN_TileMapFlags_IS_SNOWING)
    {

    }
    return map;
error:
    CN_TileMap_Free(map);
    return NULL;
}

int CN_TileMap_LoadProperties(const char *path, CN_TileMapProperties *props)
{
    FILE *properties_file = CN_fopen(path, "r");
    char buffer[1024];
    char *substr;
    int i;
    if (!CN_Try(properties_file != NULL,
                path))
    {
        return -100;
    }
    if (!CN_Check(fgets(buffer, sizeof(buffer), properties_file) != NULL))
    {
        CN_Check(fclose(properties_file) == 0);
        return -1;
    }
    substr = strtok(buffer, ",");
    if (!CN_Check(substr != NULL))
    {
        CN_Check(fclose(properties_file) == 0);
        return -2;
    }
    props->map_w = (unsigned short)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        CN_Check(fclose(properties_file) == 0);
        return -3;
    }
    props->map_h = (unsigned short)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        CN_Check(fclose(properties_file) == 0);
        return -4;
    }
    props->tile_w = (unsigned char)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        CN_Check(fclose(properties_file) == 0);
        return -5;
    }
    props->tile_h = (unsigned char)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -6;
    }
    props->tileset = (unsigned char)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -7;
    }
    props->flags = (CN_TileMapFlags)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -8;
    }
    props->music = (unsigned char)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -9;
    }
    props->modes = (CN_PerspectiveModes)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -10;
    }
    props->mode_options = (unsigned char)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -11;
    }
    props->camera_distance = (int)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -12;
    }
    props->horizon_offset = (int)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -13;
    }
    props->scale = (int)strtod(substr, NULL);
    /*start here dummy*/
    substr = strtok(NULL, ",");
    if (!CN_Check(substr != NULL))
    {
        return -13;
    }
    props->baseno = (int)strtod(substr, NULL);
    /*end just before this line dummy*/
    /*Keep this as the last one on 1st line dummy*/
    if (!CN_Check(fgets(buffer, sizeof(buffer), properties_file) != NULL))
    {
        return -15;
    }
    /*******************************************ADD VARS TO THE FIRST LINE HERE
   ALSO LIKE DON'T USE THE LAST STATEMENT, USE THE STATEMENT BEFORE IT*/

    substr = strtok(buffer, ",");
    if (!CN_Check(substr != NULL))
    {
        return -14;
    }
    props->number_of_exits = (int)strtod(substr, NULL);
    props->exits = NULL;
    if (props->number_of_exits > 0)
    {
        props->exits = CN_malloc(sizeof(CN_TileMapExit) *
                                 props->number_of_exits);
        CN_Assert(props->exits != NULL);
    }
    for (i = 0; i < props->number_of_exits; i++)
    {
        if (!CN_Check(fgets(buffer, sizeof(buffer), properties_file) != NULL))
        {
            goto exits_parse_error;
        }
        substr = strtok(buffer, ",");
        if (!CN_Check(substr != NULL))
        {
            goto exits_parse_error;
        }
        props->exits[i].baseno = (unsigned int)strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Check(substr != NULL))
        {
            goto exits_parse_error;
        }
        props->exits[i].x = (int)strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (!CN_Check(substr != NULL))
        {
            goto exits_parse_error;
        }
        props->exits[i].y = (int)strtod(substr, NULL);
        substr = strtok(NULL, ",");
        if (substr != NULL)
        {
            props->exits[i].mapno = (int)strtod(substr, NULL);
        }
        else
        {
            props->exits[i].mapno = props->exits[i].baseno;
        }
    }
    CN_Check(fclose(properties_file) == 0);
    return 0;
exits_parse_error:
    CN_free(props->exits);
    props->number_of_exits = 0;
    props->exits = NULL;
    return -18;
}

CN_Tile *CN_TileMap_LoadTiles(char *path, unsigned short map_w,
                              unsigned short map_h)
{
    CN_Tile *tiles = CN_malloc(sizeof(CN_Tile) * map_w * map_h);
    FILE *tiles_file = CN_fopen(path, "r");
    char buffer[2048];
    char *substr;
    unsigned short w, h;
    CN_Assert(tiles_file != NULL);
    for (h = 0; h < map_h; h++)
    {
        if (!CN_Try(fgets(buffer, sizeof(buffer), tiles_file) != NULL,
                    "Error parsing props file"))
        {
            CN_free(tiles);
            return NULL;
        }
        substr = strtok(buffer, ",");
        if (!CN_Check(substr != NULL))
        {
            CN_free(tiles);
            return NULL;
        }
        for (w = 0; w < map_w; w++)
        {
            tiles[map_w * h + w] = (CN_Tile)strtod(substr, NULL);
            substr = strtok(NULL, ",");
        }
    }
    CN_Check(fclose(tiles_file) == 0);
    return tiles;
}

void CN_TileMapProperties_Print(CN_TileMapProperties props)
{
    printf("Map width:          %u\n"
           "Map_height:         %u\n"
           "Tile width:         %u\n"
           "Tile height:        %u\n"
           "Tileset:            %u\n"
           "Flags:              %u\n"
           "Music:              %u\n"
           "Modes:              %d\n"
           "Mode options:       %u\n",
           props.map_w, props.map_h, props.tile_w, props.tile_h,
           props.tileset, props.flags, props.music,
           props.modes, props.mode_options);
}

void CN_TileMap_DrawLayer(CN_TileMap *map, CN_Rect screen, CN_Tile *tiles)
{
    int image_width;
    short x, y, dest_x, dest_y,
        tile_w = map->props.tile_w,
        tile_h = map->props.tile_h,
        y_offset = screen.y / tile_h,
        x_offset = screen.x / tile_w,
        w = (CN_options->width / tile_w) + x_offset,
        h = (CN_options->height / tile_h) + y_offset,
        h2 = map->props.map_h,
        w2 = map->props.map_w;
    CN_Rect source_rect;
    CN_Tile current_tile;
    if (map == NULL || map->image == NULL || tiles == NULL) return;
    image_width = CN_Image_GetWidth(map->image);
    source_rect.w = tile_w;
    source_rect.h = tile_h;
    for (y = y_offset; y <= h && y < h2; y++)
    {
        dest_y = y * tile_w - screen.y;
        for (x = x_offset; x <= w && x < w2; x++)
        {
            current_tile = tiles[y * w2 + x];
            source_rect.y=(current_tile/(image_width / source_rect.w)) * tile_h;
            source_rect.x=(current_tile%(image_width / source_rect.w)) * tile_w;
            dest_x = x * tile_w - screen.x;
            CN_Image_DrawRect(
                map->image, CN_GetScreen(), source_rect, dest_x, dest_y);
        }
    }
}

void CN_TileMap_Free(CN_TileMap *map)
{
    if (map == NULL) return;
    if (map->props.exits != NULL) CN_free(map->props.exits);
    if (map->image != NULL) CN_Image_Free(map->image);
    if (map->bg != NULL) CN_free(map->bg);
    if (map->fg != NULL) CN_free(map->fg);
    if (map->col != NULL) CN_free(map->col);
    CN_free(map);
}
