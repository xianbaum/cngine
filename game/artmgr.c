#include "artmgr.h"

#include "../cngine.h"
#include <string.h>

#define hashNumber(a) a
#define numberEquals(a, b) a == b

/* TODO: to macro */
static CN_Art *ART_LOAD_FUNC_NAME(int id)
{
    char str[40] = "art/images/";
    sprintf(str+11, "%d", id);
    strcat(str, ".pcx");
    {
        CN_Image *img = CN_Image_Load(str);
        CN_Art *art = CN_malloc(sizeof(CN_Art));
        art->art = img;
        art->referenceCount = 0;
        art->id = id;
        art->size = CN_Image_EstimateSizeOf(img) + sizeof(art);
        art->garbageIndex = -1;
        return art;
    }
}

static void ART_FREE_FUNC_NAME(CN_Image *img)
{
    CN_Image_Free(img);
}

CN_ImplementHashMap(
    CN_ArtDictionary, int, CN_Art*, hashNumber,numberEquals, 0, NULL)

CN_Art *CN_ArtManager_Get(CN_ArtManager *artmgr, int id)
{
    CN_Art *art = CN_ArtDictionary_Get((CN_ArtDictionary*)artmgr, id);
    if(art != NULL) {
        if(art->referenceCount == 0 && art->garbageIndex > -1) {
            artmgr->garbage[art->garbageIndex] = NULL;
        }
        art->referenceCount++;
        return art;
    }
    art = ART_LOAD_FUNC_NAME(id);
    artmgr->totalSize += art->size;
    CN_Assert(art != NULL);
    CN_ArtDictionary_Put((CN_ArtDictionary*)artmgr, id, art);
    return art;
}

void CN_ArtManager_Dereference(CN_ArtManager *artmgr, CN_Art *art)
{
    art->referenceCount--;
    if(art->referenceCount <= 0) {
        int i;
        art->referenceCount = 0;
        for(i = 0; i < 8; i++) {
            if(artmgr->garbage[i] == NULL) {
                artmgr->garbage[i] = art;
                return;
            }
        }
        if(i == 8) {
            CN_ArtManager_CollectGarbage(artmgr);
            artmgr->garbage[0] = art;
        }
    }
}

CN_ArtManager *CN_ArtManager_Create(void *allocatedMemory)
{
    CN_ArtManager *artmgr;
    if(allocatedMemory == NULL) artmgr = CN_malloc(sizeof(CN_ArtManager));
    else artmgr = allocatedMemory;
    CN_ArtDictionary_Create((CN_ArtDictionary*)artmgr, 16);
    artmgr->totalSize = 0;
    memset(artmgr->garbage, 0, sizeof(CN_Art*)*8);
    return artmgr;
}

void CN_ArtManager_CollectGarbage(CN_ArtManager *artmgr)
{
    int i;
    for(i = 0; i < 8; i++) {
        CN_Art *trash = artmgr->garbage[i];
        if(trash != NULL) {
            CN_ArtDictionary_Remove((CN_ArtDictionary*)artmgr, trash->id);
            artmgr->totalSize -= trash->size;
            ART_FREE_FUNC_NAME(trash->art);
            CN_free(trash);
            artmgr->garbage[i] = NULL;
        }
    }
}

void CN_ArtManager_Destroy(CN_ArtManager *artmgr)
{
    CN_ArtManager_CollectGarbage(artmgr);
    if(artmgr->dict.count > 0) { /* ugh */
        int i;
        for(i = 0; artmgr->dict.count > 0 && i < artmgr->dict.size; i++) {
            CN_Art *art = artmgr ->dict.values[i];
            if(artmgr->dict.values[i] != NULL) {
                ART_FREE_FUNC_NAME(art->art);
                CN_free(art);
                artmgr->dict.count--;
            }
        }
    }
}
