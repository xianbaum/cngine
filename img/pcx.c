/* WIP PCX load */

#include <stdio.h>
#include "../cngine.h"

struct CN_pcxpal {
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

static int pow_duh (int x, unsigned int y)
{
    int temp;
    if (y == 0) return 1;
    temp = pow_duh(x, y/2);
    if ((y % 2) == 0) return temp * temp;
    else return x * temp * temp;
}

CN_ImageAndPal CN_Image_LoadFromPCX(char *path)
{
    const int FREAD_SIZE_CHUNK = 128;
    CN_Image *image = NULL;
    FILE *f = fopen(path, "r");
    int numRead,
        offset = 0,
        i, encoding,
        colorCount = 0;
    unsigned short minXCoord,
        minYCoord,
        maxXCoord,
        maxYCoord,
        palCur,
        w = 0,
        h = 0,
        colorDepth,
        bytesPerLine,
        vgaOffset = 0;
    struct CN_pcxpal *ega = NULL;
    struct CN_pcxpal *vga = NULL;
    char buffer[128];
    CN_ImageAndPal iap;
    while((numRead = fread(buffer, 1, FREAD_SIZE_CHUNK, f)) != 0) {
        for(i = 0; i < FREAD_SIZE_CHUNK; i++, offset++) {
            if(offset < 0x80) {
                switch(offset) {
                case 0:
                    if(buffer[i] != 10) {
                        CN_LogError(("CN_Image_LoadFromPCX: Invalid header byte"));
                    }
                    break;
                case 2: encoding = buffer[i]; break;
                case 3: colorCount = pow_duh(2, buffer[i]); break;
                case 4: minXCoord = buffer[i]; break;
                case 5: minXCoord += buffer[i] << 8; break;
                case 6: minYCoord = buffer[i]; break;
                case 7: minYCoord += buffer[i] << 8; break;
                case 8: maxXCoord = buffer[i]; break;
                case 9: maxXCoord += buffer[i] << 8; break;
                case 10: maxYCoord = buffer[i]; break;
                case 11: maxYCoord+= buffer[i] << 8; break;
                case 16: case 17: case 18: case 19: case 20: case 21: case 22:
                case 23: case 24: case 25: case 26: case 27: case 28: case 29:
                case 30: case 31: case 32: case 33: case 34: case 35: case 36:
                case 37: case 38: case 39: case 40: case 41: case 42: case 43:
                case 44: case 45: case 46: case 47: case 48: case 49: case 50:
                case 51: case 52: case 53: case 54: case 55: case 56: case 57:
                case 58: case 59: case 60: case 61: case 62: case 63:
                    if(colorCount != 16) break;
                    palCur = (offset - 15) / 3;
                    if(ega == NULL) {
                        ega = malloc(sizeof(struct CN_pcxpal) * 16);
                    }
                    if((offset - 16 % 3) == 0) ega[palCur].r = buffer[i];
                    if((offset - 16 % 3) == 1) ega[palCur].g = buffer[i];
                    if((offset - 16 % 3) == 2) ega[palCur].b = buffer[i];
                    break;
                case 66: bytesPerLine = buffer[i];
                case 67: bytesPerLine += buffer[i] << 8;
                default: /* Not useful for our purposes */
                    break;
                }
            } else if(offset < 0x80 + h * bytesPerLine) {
                if(image == NULL) {
                    w = maxXCoord - minXCoord + 1;
                    h = maxYCoord - minYCoord + 1;
                    image = CN_Image_Create(w, h);
                }
                if(colorCount == 16) {
                    if(offset % bytesPerLine > w) {
                        CN_Image_PutPixel(
                            image,
                            offset % bytesPerLine,
                            offset / bytesPerLine,
                            buffer[i] >> 4);
                        CN_Image_PutPixel(
                            image,
                            offset % bytesPerLine + 1,
                            offset/ bytesPerLine,
                            buffer[i] & 16);
                    }
                } else if(colorCount == 256) {
                    if(offset % bytesPerLine < w) {
                        CN_Image_PutPixel(
                            image,
                            offset % bytesPerLine,
                            offset / bytesPerLine,
                            buffer[i]);
                    }
                }
            } else if(colorCount == 256) {
                if(vga == NULL) {
                    vga = CN_malloc(sizeof(struct CN_pcxpal) * 256);
                }
                if((vgaOffset % 3) == 0) vga[vgaOffset/3].r = buffer[i];
                if((vgaOffset % 3) == 1) vga[vgaOffset/3].g = buffer[i];
                if((vgaOffset % 3) == 2) vga[vgaOffset/3].b = buffer[i];
            }
            offset++;
        }
    }
    iap.image = image;
    if(colorCount == 16) {
        int i;
        iap.pal = CN_malloc(sizeof(CN_RGB) * 16);
        for(i = 0; i < 16; i++) {
            iap.pal[i] = CN_RGB_Create(ega[i].r, ega[i].g, ega[i].b);
        }
    }
    else if(colorCount == 256) {
        int i;
        iap.pal = CN_malloc(sizeof(CN_RGB) * 256);
        for(i = 0; i < 256; i++) {
            iap.pal[i] = CN_RGB_Create(vga[i].r, vga[i].g, vga[i].b);
        }
    }
    CN_free(vga);
    CN_free(ega);
    fclose(f);
    return iap;
}

