/** Note: This file is not to be included by itself **/

/**
 * @brief Prints an image to the console represented as base 16
 * 
 * @param image The image to print
 **/
void CN_Image_Print(CN_Image *image);

/**
 * @brief Draws a whole image
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 */
void CN_Image_Draw(CN_Image *src, CN_Image *dest, short x, short y);

/**
 * @brief Draws a whole image, rotated. Flip X and/or Y can be combined with one Rotate
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 * @param dir The direction in which to draw
 */ 
void CN_Image_DrawFlip(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    CN_DrawDirection dir);

/**
 * @brief Draws a whole image, masked
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 */ 
void CN_Image_DrawMasked(CN_Image *src, CN_Image *dest, short x, short y);

/**
 * @brief Draws a whole image, masked, rotated
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 * @param dir The draw direction. Flip X/Y can be combined with one Rotate
 */ 
void CN_Image_DrawMaskedFlip(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    CN_DrawDirection dir);

/**
 * @brief Draws a whole image, offsetted
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 */ 
void CN_Image_DrawOffset(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    unsigned char offset);

/**
 * @brief Draws a whole image, offsetted
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 * @param dir The draw direction. Flip X/Y can be combined with one Rotate
 */ 
void CN_Image_DrawOffsetFlip(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    unsigned char offset,
    CN_DrawDirection dir);

/**
 * @brief Draws a whole image, masked and offsetted
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 */
void CN_Image_DrawMaskedOffset(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    unsigned char offset);

/**
 * @brief Draws a whole image, masked, offsetted, and rotated
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 * @param dir The draw direction. Flip X/Y can be combined with one Rotate
 */
void CN_Image_DrawMaskedOffsetFlip(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    unsigned char offset,
    CN_DrawDirection dir);

/**
 * @brief Draws any part of an image
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param rect The rectangle area to draw
 * @param x The x position
 * @param y The y position
 */
void CN_Image_DrawRect(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect rect,
    short x,
    short y);

/**
 * @brief Draws any part of an image, rotated
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param rect The rectangle area to draw
 * @param x The x position
 * @param y The y position
 * @param dir The draw direction. Flip X/Y can be combined with one Rotate
 */
void CN_Image_DrawRectFlip(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect rect,
    short x,
    short y,
    CN_DrawDirection dir);

/**
 * @brief Draws any part of an image, masked
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param srcRect The rectangle area to draw from
 * @param destRect The rectangle area to draw to
 */ 
void CN_Image_DrawRectMasked(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect rect,
    short x,
    short y);

/**
 * @brief Draws any part of an image, masked, rotated
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param srcRect The rectangle area to draw from
 * @param destRect The rectangle area to draw to
 * @param dir The draw direction. Flip X/Y can be combined with one Rotate
 */ 
void CN_Image_DrawRectMaskedFlip(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect rect,
    short x,
    short y,
    CN_DrawDirection dir);

/**
 * @brief Draws any part of an image, with a shifted palette
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param rect The rectangle area to draw
 * @param x The x position
 * @param y The y position
 * @param offset number of colors in pallete to shift by
 */ 
void CN_Image_DrawRectOffset(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect rect,
    short x,
    short y,
    unsigned char offset);

/**
 * @brief Draws any part of an image, with a shifted palette, rotated
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param rect The rectangle area to draw
 * @param x The x position
 * @param y The y position
 * @param offset number of colors in pallete to shift by
 * @param dir The draw direction. Flip X/Y can be combined with one Rotate
 */ 
void CN_Image_DrawRectOffsetFlip(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect rect,
    short x,
    short y,
    unsigned char offset,
    CN_DrawDirection dir);

/**
 * @brief Draws any part of an image, color masked, with a shifted palette
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param rect The rectangle area to draw
 * @param x The x position
 * @param y The y position
 * @param offset number of colors in pallete to shift by
 */
void CN_Image_DrawRectMaskedOffset(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect rect,
    short x,
    short y,
    unsigned char offset);

/**
 * @brief Draws any part of an image, color masked, with a shifted palette, rotated
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param rect The rectangle area to draw
 * @param x The x position
 * @param y The y position
 * @param offset number of colors in pallete to shift by
 * @param dir The draw direction. Flip X/Y can be combined with one Rotate
 */
void CN_Image_DrawRectMaskedOffsetFlip(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect rect,
    short x,
    short y,
    unsigned char offset,
    CN_DrawDirection dir);

/**
 * @brief Draws any part of an image, scaled
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param srcRect The rectangle area to draw from
 * @param destRect The rectangle area to draw to
 */
void CN_Image_DrawScaled(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect srcRect,
    CN_Rect destRect);

/**
 * @brief Draws any part of an image, scaled, color masked
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param rect The rectangle area to draw
 * @param x The x position
 * @param y The y position
 */ 
void CN_Image_DrawScaledMasked(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect srcRect,
    CN_Rect destRect);

/**
 * @brief Draws any part of an image, scaled, with a shifted palette
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param srcRect The rectangle area to draw from
 * @param destRect The rectangle area to draw to
 * @param offset number of colors in pallete to shift by
 */ 
void CN_Image_DrawScaledOffset(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect srcRect,
    CN_Rect destRect,
    unsigned char offset);

/**
 * @brief Draws any part of an image, scaled, color masked with a shifted palette
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param srcRect The rectangle area to draw from
 * @param destRect The rectangle area to draw to
 * @param offset number of colors in pallete to shift by
 */ 
void CN_Image_DrawScaledMaskedOffset(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect srcRect,
    CN_Rect destRect,
    unsigned char offset);

/**
 * @brief Draws a single horizontal line of an image. Does not check bounds.
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 * @param srcx The x position to start
 * @param srcy The source Y position
 * @param w the width
 */
static CN_inline void CN_Image_DrawHLineNoGuard(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    short srcx,
    short srcy,
    short w);

/**
 * @brief Draws a single horizontal line of an image. Does not check bounds.
 *
 * @param src The image to draw
 * @param dest The image to draw
 * @param x The x position
 * @param y The y position
 * @param srcx The x position to start
 * @param srcy The source Y position
 * @param h the height
 */
static CN_inline void CN_Image_DrawVLineNoGuard(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    short srcx,
    short srcy,
    short h);

void CN_Rect_DrawSolid(CN_Image *dest, CN_Rect rect, unsigned char color);
