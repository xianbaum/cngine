#ifndef CN_PIXMAP_C
#define CN_PIXMAP_C

#ifndef COLOR_NONE
#define COLOR_NONE              0
#define COLOR_WHITE		1
#define COLOR_YELLOW		2
#define COLOR_ORANGE		3
#define COLOR_RED		4
#define COLOR_DARKRED		5
#define COLOR_DARKBLUE		6
#define COLOR_LIGHTBLUE		7
#define COLOR_BROWN		8
#define COLOR_LIGHTGREEN	9
#define COLOR_DARKGREEN		10
#define COLOR_OLIVE		11
#define COLOR_LIGHTBROWN	12
#define COLOR_LIGHTGRAY		13
#define COLOR_MEDIUMGRAY	14
#define COLOR_DARKGRAY		15
#define COLOR_BLACK		16
#endif

#include "../datastr/hashmap.h"
#include "../cngine.h"

CN_DefineHashMap(ColorHashMap, char*, int)
CN_ImplementHashMap(ColorHashMap, char*, int, CN_HashString,
                    CN_StringEquals, NULL, -1)

/* So, to explain this, I literally only needed the Pixmaps for one 
   purpose, so I hard-coded the color values. If you need different color
   values, feel free to put them in. */
static ColorHashMap *hm = NULL;
ColorHashMap *getHashMap()
{
    if(hm != NULL) return hm;
    hm = ColorHashMap_Create(NULL, 512);
    ColorHashMap_Put(hm, "None", COLOR_NONE);
    ColorHashMap_Put(hm, "#ffffffffffff", COLOR_WHITE);
    ColorHashMap_Put(hm, "#ffffffff0000", COLOR_YELLOW);
    ColorHashMap_Put(hm, "#ffff7f7f0000", COLOR_ORANGE);
    ColorHashMap_Put(hm, "#ffff00000000", COLOR_RED);
    ColorHashMap_Put(hm, "#bfbf00000000", COLOR_DARKRED);
    ColorHashMap_Put(hm, "#00000000e6e6", COLOR_DARKBLUE);
    ColorHashMap_Put(hm, "#66666666e6e6", COLOR_LIGHTBLUE);
    ColorHashMap_Put(hm, "#cccc4c4c4c4c", COLOR_BROWN);
    ColorHashMap_Put(hm, "#0000e6e60000", COLOR_LIGHTGREEN);
    ColorHashMap_Put(hm, "#00007f7f0000", COLOR_DARKGREEN);
    ColorHashMap_Put(hm, "#99997f7f4c4c", COLOR_OLIVE);
    ColorHashMap_Put(hm, "#cccc7f7f6666", COLOR_LIGHTBROWN);
    ColorHashMap_Put(hm, "#bfbfbfbfbfbf", COLOR_LIGHTGRAY);
    ColorHashMap_Put(hm, "#7f7f7f7f7f7f", COLOR_MEDIUMGRAY);
    ColorHashMap_Put(hm, "#3f3f3f3f3f3f", COLOR_DARKGRAY);
    ColorHashMap_Put(hm, "#000000000000", COLOR_BLACK);

    ColorHashMap_Put(hm, "#FFFFFFFFFFFF", COLOR_WHITE);
    ColorHashMap_Put(hm, "#FFFFFFFF0000", COLOR_YELLOW);
    ColorHashMap_Put(hm, "#FFFF7F7F0000", COLOR_ORANGE);
    ColorHashMap_Put(hm, "#FFFF00000000", COLOR_RED);
    ColorHashMap_Put(hm, "#BFBF00000000", COLOR_DARKRED);
    ColorHashMap_Put(hm, "#00000000E6E6", COLOR_DARKBLUE);
    ColorHashMap_Put(hm, "#66666666E6E6", COLOR_LIGHTBLUE);
    ColorHashMap_Put(hm, "#CCCC4C4C4C4C", COLOR_BROWN);
    ColorHashMap_Put(hm, "#0000E6E60000", COLOR_LIGHTGREEN);
    ColorHashMap_Put(hm, "#00007F7F0000", COLOR_DARKGREEN);
    ColorHashMap_Put(hm, "#99997F7F4C4C", COLOR_OLIVE);
    ColorHashMap_Put(hm, "#CCCC7F7F6666", COLOR_LIGHTBROWN);
    ColorHashMap_Put(hm, "#BFBFBFBFBFBF", COLOR_LIGHTGRAY);
    ColorHashMap_Put(hm, "#7F7F7F7F7F7F", COLOR_MEDIUMGRAY);
    ColorHashMap_Put(hm, "#3F3F3F3F3F3F", COLOR_DARKGRAY);
    ColorHashMap_Put(hm, "#000000000000", COLOR_BLACK);
    return hm;
}
    
CN_Image *customLoadPixmap(char *path)
{
    CN_Image *img;
    FILE *f = fopen(path, "r");
    ColorHashMap *hashmap = getHashMap();
    const int FREAD_SIZE_CHUNK = 128;
    char buffer[128];
    CN_bool isInQuotes = CN_false, widthWarned = CN_false;
    int w=0,h=0,ncolors=0, cpp=1, curColors = 0,
        headerProgress = 0, size, numBufferCur = 0,
        x = 0, y = 0, colorProgress = 0, maxX, cur = 0, numRead;
    char numBuffer[18], colors[256], currentColorChar;
    memset(colors, 0, 256);
    if(f == NULL) return NULL;
    while((numRead = fread(buffer, 1, FREAD_SIZE_CHUNK, f)) != 0) {
        for(cur = 0; cur < numRead; cur++) {
            if(!isInQuotes && buffer[cur] != '"') continue;
            else if(buffer[cur] == '}') {
                goto finish;
            } else if(isInQuotes && buffer[cur] == '"') {
                isInQuotes = CN_false;
                headerProgress++;
                if(headerProgress > (4 + ncolors)) {
                    x = 0;
                    y++;
                } else if(headerProgress > 4) {
                    if(colorProgress == 3) {
                        numBuffer[numBufferCur] = '\0';
                        colors[currentColorChar] =
                            ColorHashMap_Get(hashmap, numBuffer);
                    }
                    colorProgress = 0;
                }
                continue;
            }
            else if(!isInQuotes && buffer[cur] == '"'){
                isInQuotes = CN_true;
                continue;
            }
            
            /* Header */
            if(headerProgress < 4) {
                if(buffer[cur] == ' ') {
                    int num;
                    numBuffer[numBufferCur] = '\0';
                    num = atoi(numBuffer);
                    numBufferCur = 0;
                    switch(headerProgress) {
                    case 0:
                        w = num;
                        maxX = w - 1;
                        break;
                    case 1: h = num; break;
                    case 2: ncolors = num; break;
                    case 3: cpp = num; break;
                    default: continue;
                    }
                    if(headerProgress > 1) {
                        img = CN_Image_Create(w, h);
                    }
                    headerProgress++;
                    continue;
                }
                numBuffer[numBufferCur] = buffer[cur];
                numBufferCur++;
                continue;

            /* Colors */
            } else if(headerProgress >= 4 &&
                      headerProgress < 4 + ncolors) {
                if(colorProgress == 0) {
                    numBufferCur = 0;
                    currentColorChar = buffer[cur];
                    curColors++;
                    colorProgress = 1;
                }  else if(colorProgress == 1) {
                    if(buffer[cur] == 'c') colorProgress = 2;
                } else {
                    if(buffer[cur] == ' ') {
                        continue;
                    }
                    colorProgress = 3;
                    numBuffer[numBufferCur] = buffer[cur];
                    numBufferCur++;
                }
            }
            /* Image */
            if(headerProgress >= (4 + ncolors)) {
                if(x < w && y < h) {
                    CN_Image_PutPixel(img, x, y, colors[buffer[cur]]);
                } else if(x > maxX) {
                    maxX = x;
                }
                x++;
                
            }
        }
    }
    finish:
    /* The program increments Y one extra time, so "y > h" accounts for that */
    if(y > h) {
        CN_LogError(("Actual height (%i) larger than metadata (%i) in file %s", y, h, path));
    }
    if(maxX >= w) {
        CN_LogError(("Actual width (%i) larger than metadata (%i) in file %s", maxX+1, w, path));
    }
    fclose(f);
    return img;
}

#endif /* CN_PIXMAP_C */