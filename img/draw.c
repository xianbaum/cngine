#define draw_impl(src, dest, srcx, srcy, srcw, srch,                    \
                  destx, desty, offset, mask, has_mask, dir, u)         \
    {                                                                   \
    int x##u, y##u;                                                     \
    for(y##u = 0; y##u < srch; y##u++) {                                \
    for(x##u = 0; x##u < srcw; x##u++) {                                \
        char pixel##u = CN_Image_GetPixel(src, srcx +x##u, srcy+y##u);  \
        if(has_mask && pixel##u == mask) continue;                      \
        if((dir) == CN_DRAWDIRECTION_NORMAL ||                            \
           (dir) == (CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPX |\
                   CN_DRAWDIRECTION_FLIPY)) {                           \
            CN_Image_PutPixel(dest, destx + x##u, desty + y##u, pixel##u + offset); \
        }                                                               \
        if((dir) == CN_DRAWDIRECTION_FLIPX ||                             \
           (dir) ==  (CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPY)) { \
            CN_Image_PutPixel(dest, destx + srcw - 1 - x##u, desty + y##u, pixel##u + offset); \
        }                                                               \
        if((dir) == CN_DRAWDIRECTION_FLIPY ||                             \
           (dir) == (CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPX)) { \
            CN_Image_PutPixel(dest, destx + x##u, desty + srch - 1 - y##u, pixel##u + offset); \
        }                                                               \
        if((dir) == (CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY) ||  \
            (dir) == (CN_DRAWDIRECTION_ROTATE180)) {                      \
            CN_Image_PutPixel(dest, destx + srcw - 1 - x##u, desty + srch - 1 - y##u, pixel##u + offset); \
        }                                                               \
        if((dir) == (CN_DRAWDIRECTION_ROTATE90) ||                        \
           (dir) == (CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPY |\
                   CN_DRAWDIRECTION_FLIPX)) {                           \
            CN_Image_PutPixel(dest, desty + y##u, destx + x##u, pixel##u + offset); \
        }                                                               \
        if((dir) == (CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_ROTATE90) ||\
           (dir) == (CN_DRAWDIRECTION_FLIPY | CN_DRAWDIRECTION_ROTATE270)) { \
            CN_Image_PutPixel(dest, desty + srch - 1 - y##u, destx + x##u, pixel##u + offset); \
        }                                                               \
        if((dir) == (CN_DRAWDIRECTION_FLIPY | CN_DRAWDIRECTION_ROTATE90) || \
            (dir) == (CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_ROTATE270)) { \
            CN_Image_PutPixel(dest, desty + y##u, destx + srcw - 1 - x##u, pixel##u + offset); \
        }                                                               \
        if((dir) == (CN_DRAWDIRECTION_FLIPX |                           \
                     CN_DRAWDIRECTION_FLIPY | CN_DRAWDIRECTION_ROTATE90) || \
           (dir) == CN_DRAWDIRECTION_ROTATE270) {                       \
           CN_Image_PutPixel(dest, desty + srch - 1 - y##u, destx + srcw - 1 - x##u, pixel##u + offset); \
        }                                                               \
        }                                                               \
    }                                                                   \
}

#define bounds_check_impl_src(drawX, drawY, drawW, drawH, imgW, imgH)   \
{                                                                   \
    if(drawX < 0) drawX = 0;                                        \
    if(drawY < 0) drawY = 0;                                        \
    if(drawX + drawW > imgW) drawW = imgW - drawX;                  \
    if(drawY + drawH > imgH) drawH = imgH - drawY;                  \
}

#define bounds_check_impl_dest(srcX, srcY, destX, destY, drawW, drawH, destW, destH) \
    {                                                                \
    if(destX < 0) {                                                  \
        drawW += destX;                                              \
        srcX -= destX;                                               \
        destX = 0;                                                   \
    }                                                                \
    if(destY < 0) {                                                  \
        drawH += destY;                                              \
        srcY -= destY;                                               \
        destY = 0;                                                   \
    }                                                                \
    if(destX + drawW > destW) drawW = destW - destX;                  \
    if(destY + drawH > destH) drawH = destH - destY;                  \
}

#define bounds_check_impl_rect(destX, destY, drawW, drawH, destW, destH) \
    {                                                                \
    if(destX < 0) {                                                  \
        drawW += destX;                                              \
        destX = 0;                                                   \
    }                                                                \
    if(destY < 0) {                                                  \
        drawH += destY;                                              \
        destY = 0;                                                   \
    }                                                                \
    if(destX + drawW > destW) drawW = destW - destX;                  \
    if(destY + drawH > destH) drawH = destH - destY;                  \
}


/* This does not work for 90 degree or 270 degree rotations or non-scaled */
#define bounds_draw_impl(src, dest, srcx, srcy, srcw, srch,             \
                         destx, desty, offset, mask, has_mask, dir, u, check_src) \
                                                                        \
{                                                                       \
    int srcX = srcx, srcY = srcy, srcW = srcw, srcH = srch,             \
         destX = destx, destY = desty;                                  \
    if(check_src) {                                                     \
        bounds_check_impl_src(srcX, srcY, srcW, srcH,                   \
                      CN_Image_GetWidth(src), CN_Image_GetHeight(src))  \
    }                                                                   \
    bounds_check_impl_dest(srcX, srcY, destX, destY, srcW, srcH,        \
                      CN_Image_GetWidth(dest), CN_Image_GetHeight(dest))\
    draw_impl(src, dest, srcX, srcY, srcW, srcH,                        \
              destX, destY, offset, mask, has_mask, dir, u)             \
}

/* This only works for 90 degree, non-scaled */
#define bounds_draw_impl_90(src, dest, srcx, srcy, srcw, srch,          \
                         destx, desty, offset, mask, has_mask, dir, u, check_src) \
                                                                        \
{                                                                       \
    int srcX = srcx, srcY = srcy, srcW = srcw, srcH = srch,             \
         destX = destx, destY = desty;                                  \
    if(check_src) {                                                     \
        bounds_check_impl_src(srcX, srcY, srcW, srcH,                   \
                      CN_Image_GetWidth(src), CN_Image_GetHeight(src))  \
    }                                                                   \
    bounds_check_impl_dest(srcX, srcY, destX, destY, srcH, srcW,        \
                      CN_Image_GetWidth(dest), CN_Image_GetHeight(dest))\
    draw_impl(src, dest, srcX, srcY, srcW, srcH,                        \
              destX, destY, offset, mask, has_mask, dir, u)             \
}

#define rotate_impl(src, dest, srcx, srcy, srcw, srch, destx, desty, offset, mask, has_mask, dir, check_src_bounds) \
    {                                                                   \
    switch(dir)                                                         \
    {                                                                   \
    case CN_DRAWDIRECTION_NORMAL:                                       \
    case CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY: \
    bounds_draw_impl(src, dest, srcx, srcy,                                 \
                     srcw, srch, \
                     destx, desty, offset, mask, has_mask, CN_DRAWDIRECTION_NORMAL, Normal, check_src_bounds); \
        break;\
    case CN_DRAWDIRECTION_FLIPX:\
    case CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPY:\
        bounds_draw_impl(src, dest, 0, 0,\
                     srcw, srch, \
                         destx, desty, offset, mask, has_mask, CN_DRAWDIRECTION_FLIPX, FlipX, check_src_bounds); \
        break;\
    case CN_DRAWDIRECTION_FLIPY:\
    case CN_DRAWDIRECTION_ROTATE180 | CN_DRAWDIRECTION_FLIPX:\
        bounds_draw_impl(src, dest, 0, 0,\
                     srcw, srch, \
                         destx, desty, offset, mask, has_mask, CN_DRAWDIRECTION_FLIPY, FlipY, check_src_bounds); \
        break;\
    case CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY:\
    case CN_DRAWDIRECTION_ROTATE180:\
        bounds_draw_impl(src, dest, 0, 0,\
                     srcw, srch, \
                     destx, desty, offset, mask, has_mask,\
                         (CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY), FlipXY, check_src_bounds); \
        break;\
    case CN_DRAWDIRECTION_ROTATE90:\
    case CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY:\
        bounds_draw_impl_90(src, dest, 0, 0,\
                     srcw, srch, \
                     destx, desty, offset, mask, has_mask,\
                         CN_DRAWDIRECTION_ROTATE90, Rot90, check_src_bounds);    \
        break;\
    case CN_DRAWDIRECTION_ROTATE90 | CN_DRAWDIRECTION_FLIPX:\
    case CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPY:\
        bounds_draw_impl_90(src, dest, 0, 0,\
                     srcw, srch, \
                     destx, desty, offset, mask, has_mask,\
                     CN_DRAWDIRECTION_ROTATE90 | CN_DRAWDIRECTION_FLIPX,\
                         Rot90FlipX, check_src_bounds);                                  \
        break;\
    case CN_DRAWDIRECTION_ROTATE90 | CN_DRAWDIRECTION_FLIPY:\
    case CN_DRAWDIRECTION_ROTATE270 | CN_DRAWDIRECTION_FLIPX:\
        bounds_draw_impl_90(src, dest, 0, 0,\
                     srcw, srch, \
                     destx, desty, offset, mask, has_mask,\
                     CN_DRAWDIRECTION_ROTATE90 | CN_DRAWDIRECTION_FLIPY,\
                         Rot90FlipY, check_src_bounds);                                  \
        break;\
    case CN_DRAWDIRECTION_ROTATE90 | CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY:\
    case CN_DRAWDIRECTION_ROTATE270:\
        bounds_draw_impl_90(src, dest, 0, 0,\
                     srcw, srch, \
                     destx, desty, offset, mask, has_mask,\
                     CN_DRAWDIRECTION_ROTATE90 | CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY,\
                         Rot90FlipXY, check_src_bounds);                                 \
    }\
}

#define nearest_impl(src, dest, srcRect, destRect, offset, mask, has_mask, u) \
    int x_ratio##u, y_ratio##u, px##u, py##u, x##u, y##u;               \
    char pixel##u;                                                      \
    if(destRect.w == 0 || destRect.h == 0) return;                      \
    x_ratio##u = (srcRect.w<<16)/destRect.w+1 ;                         \
    y_ratio##u = (srcRect.h<<16)/destRect.h+1 ;                         \
    for (y##u = 0; y##u < destRect.h; y##u++) {                         \
        for (x##u = 0; x##u < destRect.w; x##u++) {                     \
            px##u = x##u * x_ratio##u >> 16;                            \
            py##u = y##u * y_ratio##u >> 16;                            \
            pixel##u = CN_Image_GetPixel(                               \
                src,                                                    \
                px##u + srcRect.x,                                      \
                py##u + srcRect.y);                                     \
            if(has_mask && pixel##u == mask) continue;                  \
            CN_Image_PutPixel(dest, x##u+destRect.x, y##u+destRect.y, pixel##u + offset);     \
    }                                                                   \
}

void CN_Image_Draw(CN_Image *src, CN_Image *dest, short x, short y)
{
    bounds_draw_impl(src, dest, 0, 0,
                 CN_Image_GetWidth(src), CN_Image_GetHeight(src),
                     x, y, 0, 0, 0, CN_DRAWDIRECTION_NORMAL, _, 0)
}

void CN_Image_DrawFlip(
    CN_Image *src, CN_Image *dest, short x, short y, CN_DrawDirection dir)
{
    rotate_impl(src, dest, 0, 0,
                CN_Image_GetWidth(src), CN_Image_GetHeight(src),
                x, y, 0, 0, 0, dir, 0);
}

void CN_Image_DrawMasked(CN_Image *src, CN_Image *dest, short x, short y)
{
    bounds_draw_impl(src, dest, 0, 0,
                 CN_Image_GetWidth(src), CN_Image_GetHeight(src),
                     x, y, 0, 0, 1, CN_DRAWDIRECTION_NORMAL, _, 0)
}

void CN_Image_DrawMaskedFlip(
    CN_Image *src, CN_Image *dest, short x, short y, CN_DrawDirection dir)
{
    rotate_impl(src, dest, 0, 0,
                CN_Image_GetWidth(src), CN_Image_GetHeight(src),
                x, y, 0, 0, 1, dir, 0);
}

void CN_Image_DrawOffset(CN_Image *src, CN_Image *dest, short x, short y,
    unsigned char offset)
{
    bounds_draw_impl(src, dest, 0, 0,
                 CN_Image_GetWidth(src), CN_Image_GetHeight(src),
                     x, y, offset, 0, 0, CN_DRAWDIRECTION_NORMAL, _, 0)
}

void CN_Image_DrawOffsetFlip(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    unsigned char offset,
    CN_DrawDirection dir)
{
    rotate_impl(src, dest, 0, 0,
                CN_Image_GetWidth(src), CN_Image_GetHeight(src),
                x, y, offset, 0, 0, dir, 0);
}

void CN_Image_DrawMaskedOffset(
    CN_Image *src, CN_Image *dest, short x, short y, unsigned char offset)
{
    bounds_draw_impl(src, dest, 0, 0,
                 CN_Image_GetWidth(src), CN_Image_GetHeight(src),
                     x, y, offset, 0, 1, CN_DRAWDIRECTION_NORMAL, _, 0)
}

void CN_Image_DrawMaskedOffsetFlip(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    unsigned char offset,
    CN_DrawDirection dir)
{
    rotate_impl(src, dest, 0, 0,
                CN_Image_GetWidth(src), CN_Image_GetHeight(src),
                x, y, offset, 0, 1, dir, 0);
}

void CN_Image_DrawRect(
    CN_Image *src, CN_Image *dest, CN_Rect src_rect, short x, short y)
{
    bounds_draw_impl(src, dest,
                 src_rect.x, src_rect.y, src_rect.w, src_rect.h,
                     x, y, 0, 0, 0, CN_DRAWDIRECTION_NORMAL, _, 1);
}

void CN_Image_DrawRectFlip(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect src_rect,
    short x,
    short y,
    CN_DrawDirection dir)
{
    rotate_impl(src, dest,
                src_rect.x, src_rect.y, src_rect.w, src_rect.h,
                x, y, 0, 0, 0, dir, 1);
}

void CN_Image_DrawRectMasked(
    CN_Image *src, CN_Image *dest, CN_Rect src_rect, short x, short y)
{
    bounds_draw_impl(
        src, dest,
        src_rect.x, src_rect.y, src_rect.w, src_rect.h, x, y, 0, 0, 1, CN_DRAWDIRECTION_NORMAL, _, 1);
}

void CN_Image_DrawRectMaskedFlip(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect src_rect,
    short x,
    short y,
    CN_DrawDirection dir)
{
    rotate_impl(src, dest,
                src_rect.x, src_rect.y, src_rect.w, src_rect.h,
                x, y, 0, 0, 1, dir, 1);
}

void CN_Image_DrawRectOffset(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect src_rect,
    short x,
    short y,
    unsigned char offset)
{
    bounds_draw_impl(
        src, dest, src_rect.x, src_rect.y, src_rect.w, src_rect.h,
        x, y, offset, 0, 0, CN_DRAWDIRECTION_NORMAL, _, 1)
}

void CN_Image_DrawRectOffsetFlip(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect src_rect,
    short x,
    short y,
    unsigned char offset,
    CN_DrawDirection dir)
{
    rotate_impl(src, dest,
                src_rect.x, src_rect.y, src_rect.w, src_rect.h,
                x, y, offset, 0, 0, dir, 1);
}

void CN_Image_DrawRectMaskedOffset(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect src_rect,
    short x,
    short y,
    unsigned char offset)
{
    bounds_draw_impl(
        src, dest, src_rect.x, src_rect.y, src_rect.w, src_rect.h,
        x, y, offset, 0, 1, CN_DRAWDIRECTION_NORMAL, _, 1)
}

void CN_Image_DrawRectMaskedOffsetFlip(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect src_rect,
    short x,
    short y,
    unsigned char offset,
    CN_DrawDirection dir)
{
    rotate_impl(src, dest,
                src_rect.x, src_rect.y, src_rect.w, src_rect.h,
                x, y, offset, 0, 1, dir, 1);
}

void CN_Image_DrawScaled(
    CN_Image *src, CN_Image *dest, CN_Rect srcDest, CN_Rect destRect)
{
    nearest_impl(src, dest, srcDest, destRect, 0, 0, CN_DRAWDIRECTION_NORMAL, _)
}

void CN_Image_DrawScaledMasked(
    CN_Image *src, CN_Image *dest, CN_Rect srcRect, CN_Rect destRect)
{
    nearest_impl(src, dest, srcRect, destRect, 0, 0, CN_DRAWDIRECTION_NORMAL, _)
}

void CN_Image_DrawScaledOffset(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect srcRect,
    CN_Rect destRect,
    unsigned char offset)
{
    nearest_impl(src, dest, srcRect, destRect, offset, 0, CN_DRAWDIRECTION_NORMAL, _)
}


void CN_Image_DrawScaledMaskedOffset(
    CN_Image *src,
    CN_Image *dest,
    CN_Rect srcRect,
    CN_Rect destRect,
    unsigned char offset)
{
    nearest_impl(src, dest, srcRect, destRect, offset, 0, 1, _)
}

void CN_Rect_DrawSolid(CN_Image *dest, CN_Rect rect, unsigned char color)
{
    bounds_check_impl_rect(
        rect.x,
        rect.y,
        rect.w,
        rect.h,
        CN_Image_GetWidth(dest),
        CN_Image_GetHeight(dest));
    for(rect.h -= 1; rect.h >= 0; rect.h--) {
        int rectW = rect.w - 1;
        for(; rectW >= 0; rectW--) {
            CN_Image_PutPixel(dest, rect.x+rectW, rect.y+rect.h, color);
        }
    }
}
