################################################################################
# A few notes about this Makefile                                              #
# ---------------------------------------------------------------------------- #
#                                                                              #
# This Makefile only builds tests for CNgine. Due to the way CNgine is, CNgine #
# is normally built by including it as a folder in a project. An easy way to   #
# do this is to use it as a git submodule.                                     #
#                                                                              #
# This Makefile by default  will assume that at least Allegro 4, SDL1.2 and    #
# SDL2.0 are installed. If only one is installed, you can run that build       #
# manually by running, for example "make test_sdl2".                           #
#                                                                              #
# The "dummy" binds were not made to pass tests, they were made to compile.    #
################################################################################

################################################################################
# Vars                                                                         #
################################################################################

OPTIONS_NEW_GCC = -Wextra -Winit-self -Wuninitialized -Wmissing-declarations -ansi

NO_LUA_CFLAGS	= -DCN_NO_LUA


CC		= gcc
DEBUG		= -g -DCN_DEBUG
CFILES:= $(wildcard *.c) $(wildcard ds/*.c) ${wildcard test/*.c}
OBJ_FILES := $(addprefix test/obj/,$(subst ,,$(CFILES:.c=.o)))
BIN_DIR 	= test/bin/
RUNTEST		= ./test/bin/runtest

#SDL 1.2
SDL1_2_LINKER_M   = -lSDL
SDL1_2_IMAGE_LINK = -lSDL_image
SDL1_2_MIX_LINKER = -lSDL_mixer -lmikmod -lwinmm -lm
SDL1_2_MW_LINKER  = -lmingw32 -lSDLmain -lSDL -mwindows 
SDL1_2_CFLAGS     = -DCN_SDL1_2 -DCN_NO_MIXER
SDL1_2_LINKER	= $(SDL1_2_LINKER_M) $(SDL1_2_IMAGE_LINK) $(SDL_1_2_MIX_LINKER)

#SDL 2
SDL2_CFLAGS     = -DCN_SDL2 -DCN_NO_MIXER
SDL2_LINKER     = -lSDL2 -lSDL2_image -lSDL2_mixer

#Allegro 4.4/4.2
ALLEGRO44_LINKER= `pkg-config --cflags --libs allegrogl allegro ` -lm
ALLEGRO44_MW_L  = -lallegro-4.4.2-md -lallegrogl-0.4.4-md
ALLEGRO42_LINKER= -lm -lalleg
LALGIF_LINKER   = -lalgif
DUMB_LINKER 	= -laldmd -ldumbd
JGMOD_LINKER	= -ljgmod
ALLEGRO4_CFLAGS = -DCN_ALLEGRO4
JGMOD_CFLAGS	= -DUSE_JGMOD_
DUMB_CFLAGS    	= -DUSE_DUMB_
ALLEGRO4_SPLINT	= .splintrc

#Libretro
LIBRETRO_CFLAGS = -DCN_LIBRETRO


################################################################################
# OS-specific automatic target                                                 #
################################################################################

ifeq ($(OS),Windows_NT)
all: sdl2mw
else
all: sdl2
endif

###############################################################################
# Setup                                                                       #
###############################################################################

mkdirs:
	mkdir -p test/obj/ds
	mkdir -p test/bin
	mkdir -p test/obj/test

################################################################################
# Building                                                                     #
################################################################################

.PHONY: dummy
dummy: mkdirs dummy_c runtests

dummy_c: OPTIONS += $(OPTIONS_NEW_GCC)
dummy_c: CFLAGS += $(NO_LUA_CFLAGS)

dummy_c: $(OBJ_FILES)
	$(CC) -o $(BIN_DIR)runtest $^

.PHONY: sdl1_2
sdl1_2: mkdirs sdl1_2_c runtests

sdl1_2_c: OPTIONS += $(OPTIONS_NEW_GCC)
sdl1_2_c: CFLAGS += $(NO_LUA_CFLAGS) $(SDL1_2_CFLAGS)

sdl1_2_c: $(OBJ_FILES)
	$(CC) -o $(BIN_DIR)runtest $^ $(SDL1_2_LINKER)

.PHONY: allegro
allegro: mkdirs allegro_c runtests

allegro_c: OPTIONS += $(OPTIONS_NEW_GCC)
allegro_c: CFLAGS += $(NO_LUA_CFLAGS) $(ALLEGRO4_CFLAGS)

allegro_c: $(OBJ_FILES)
	$(CC) -o $(BIN_DIR)runtest $^ $(ALLEGRO44_LINKER)


.PHONY: sdl2mw
sdl2mw: mkdirs sdl2mwc runtests

sdl2mwc: SDL_LINKER_MW = -lmingw32 -lSDL2main
sdl2mwc: mkdirs sdl2c

.PHONY: sdl2
sdl2: SDL_LINKER_MW = -lc
sdl2: mkdirs sdl2c runtests

sdl2c: OPTIONS += $(OPTIONS_NEW_GCC)
sdl2c: CFLAGS += $(SDL2_CFLAGS) $(NO_LUA_CFLAGS)
sdl2c: $(OBJ_FILES)
	$(CC) -o $(BIN_DIR)runtest $^ $(SDL_LINKER_MW) $(SDL2_LINKER)


.PHONY: libretro
libretro: mkdirs libretroc

libretroc: OPTIONS += $(OPTIONS_NEW_GCC)
libretroc: CFLAGS += $(LIBRETRO_CFLAGS)$(NO_LUA_CFLAGS)
libretroc: $(OBJ_FILES)
	$(CC) -o $(BIN_DIR)runtest $^

test/obj/%.o: %.c
	$(CC) -c $(OPTIONS) $(CFLAGS) $(INCLUDE) $< $(OBJS) $(DEBUG) -o $@

################################################################################
# Tests                                                                        #
################################################################################

Test_CN_Options_FromCommandLineArgs:
	$(RUNTEST) 1
Test_CN_StringDictionary:
	$(RUNTEST) 2
Test_CN_VectorList:
	$(RUNTEST) 3
Test_CN_Init_CN_Deinit:
	$(RUNTEST) 4
Test_CN_Image_Create:
	$(RUNTEST) 5
Test_CN_Image_PutPixel:
	$(RUNTEST) 6
Test_CN_Image_Draw:
	$(RUNTEST) 7
Test_CN_Image_DrawMasked:
	$(RUNTEST) 8
Test_CN_Image_DrawOffset:
	$(RUNTEST) 9
Test_CN_Image_DrawMaskedOffset:
	$(RUNTEST) 10
Test_CN_Image_DrawRect:
	$(RUNTEST) 11
Test_CN_Image_DrawRectMasked:
	$(RUNTEST) 12
Test_CN_Image_DrawRectOffset:
	$(RUNTEST) 13
Test_CN_Image_DrawRectMaskedOffset:
	$(RUNTEST) 14
Test_CN_Image_DrawScaled:
	$(RUNTEST) 15
Test_CN_Image_DrawScaledMasked:
	$(RUNTEST) 16
Test_CN_Image_DrawScaledMaskedOffset:
	$(RUNTEST) 17
Test_CN_Image_DrawHLineNoGuard:
	$(RUNTEST) 18
Test_CN_Image_DrawVLineNoGuard:
	$(RUNTEST) 19
Test_CN_Image_DrawFlip:
	$(RUNTEST) 20
Test_CN_Image_DrawMaskedFlip:
	$(RUNTEST) 21
Test_CN_Image_DrawOffsetFlip:
	$(RUNTEST) 22
Test_CN_Image_DrawMaskedOffsetFlip:
	$(RUNTEST) 23
Test_CN_Image_DrawRectFlip:
	$(RUNTEST) 24
Test_CN_Image_DrawRectMaskedFlip:
	$(RUNTEST) 25
Test_CN_Image_DrawRectOffsetFlip:
	$(RUNTEST) 26
Test_CN_Image_DrawRectMaskedOffsetFlip:
	$(RUNTEST) 27
Test_CN_Image_DrawSheared:
	$(RUNTEST) 28
Test_CN_Image_DrawShearedMasked:
	$(RUNTEST) 29
TEST_CN_Image_DrawShearedMaskedOffset:
	$(RUNTEST) 30

runtests: Test_CN_Options_FromCommandLineArgs Test_CN_StringDictionary Test_CN_VectorList Test_CN_Init_CN_Deinit Test_CN_Image_Create Test_CN_Image_PutPixel Test_CN_Image_Draw Test_CN_Image_DrawMasked Test_CN_Image_DrawOffset Test_CN_Image_DrawMaskedOffset Test_CN_Image_DrawRect Test_CN_Image_DrawRectMasked Test_CN_Image_DrawRectOffset Test_CN_Image_DrawRectMaskedOffset Test_CN_Image_DrawHLineNoGuard Test_CN_Image_DrawVLineNoGuard Test_CN_Image_DrawFlip

################################################################################
# Misc                                                                         #
################################################################################
.PHONY: clean
clean:
	rm -rf test/obj
	rm -rf test/bin

check-syntax: sdl2
