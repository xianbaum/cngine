/**
 * @file sdl1_2.h
 * @author Christian Michael Baum
 * @brief Implementation used if compiling with SDL 1.2
 * Refer to cngine.h for API
 **/

#ifndef SDL12_BINDS_H_
#define SDL12_BINDS_H_

#ifdef CN_SDL1_2

#include <SDL/SDL.h>

#ifndef CN_NO_IMAGE
#include <SDL/SDL_image.h>
#endif

#ifndef CN_NO_MIXER
#include <SDL/SDL_mixer.h>
#endif

#include <stdio.h>

#include "../core/chelpers.h"
#include "../core/cnerror.h"
#include "../core/cnmem.h"

#define END_OF_MAIN()
#define NUMBER_OF_SDL_SCANCODES 285

typedef Uint8 CN_MouseButton_Impl;
typedef SDL_Color CN_RGB_Impl;
typedef SDL_Surface CN_Image_Impl;
typedef struct SDLBackend {
    int audio_backend;
    SDL_Surface *SDL_screen;
    SDL_Surface *black_screen;
    Uint8 *keypress_state;
    Uint8 *keyboard_state;
    Uint8 *keyup_state;
    unsigned char tickCounter;
    unsigned char mouseCurrentTick;
    Uint8 lastMouseState;
    int lastMouseX;
    int lastMouseY;
} SDLBackend;
/**
 * @brief A key code. Actual values vary based on implementation.
 *
 */
typedef SDLKey CN_Key_Impl;
#ifndef CN_NO_MIXER
typedef Mix_Chunk CN_SoundByte_Impl;
typedef Mix_Music CN_MOD_Impl;
typedef Mix_Music CN_MIDI_Impl;
typedef Mix_Music CN_Music_Impl;
#else
typedef void CN_SoundByte_Impl;
typedef void CN_MOD_Impl;
typedef void CN_MIDI_Impl;
typedef void CN_Music_Impl;
#endif

/*Input macros*/
#define CN_KEY__BACKSPACE SDLK_BACKSPACE
#define CN_KEY__TAB SDLK_TAB
#define CN_KEY__ENTER SDLK_RETURN
#define CN_KEY__PAUSE SDLK_PAUSE
#define CN_KEY__ESC SDLK_ESCAPE
#define CN_KEY__SPACE SDLK_SPACE
#define CN_KEY__MINUS SDLK_MINUS
#define CN_KEY__PERIOD SDLK_PERIOD
#define CN_KEY__SLASH SDLK_SLASH
#define CN_KEY__COMMA SDLK_COMMA
#define CN_KEY__0 SDLK_0
#define CN_KEY__1 SDLK_1
#define CN_KEY__2 SDLK_2
#define CN_KEY__3 SDLK_3
#define CN_KEY__4 SDLK_4
#define CN_KEY__5 SDLK_5
#define CN_KEY__6 SDLK_6
#define CN_KEY__7 SDLK_7
#define CN_KEY__8 SDLK_8
#define CN_KEY__9 SDLK_9
#define CN_KEY__SEMICOLON SDLK_SEMICOLON
#define CN_KEY__LBRACKET SDLK_LBRACKET
#define CN_KEY__RBRACKET SDLK_RBRACKET
#define CN_KEY__BACKSLASH SDLK_BACKSLASH
#define CN_KEY__A SDLK_a
#define CN_KEY__B SDLK_b
#define CN_KEY__C SDLK_c
#define CN_KEY__D SDLK_d
#define CN_KEY__E SDLK_e
#define CN_KEY__F SDLK_f
#define CN_KEY__G SDLK_g
#define CN_KEY__H SDLK_h
#define CN_KEY__I SDLK_i
#define CN_KEY__J SDLK_j
#define CN_KEY__K SDLK_k
#define CN_KEY__L SDLK_l
#define CN_KEY__M SDLK_m
#define CN_KEY__N SDLK_n
#define CN_KEY__O SDLK_o
#define CN_KEY__P SDLK_p
#define CN_KEY__Q SDLK_q
#define CN_KEY__R SDLK_r
#define CN_KEY__S SDLK_s
#define CN_KEY__T SDLK_t
#define CN_KEY__U SDLK_u
#define CN_KEY__V SDLK_v
#define CN_KEY__W SDLK_w
#define CN_KEY__X SDLK_x
#define CN_KEY__Y SDLK_y
#define CN_KEY__Z SDLK_z
#define CN_KEY__DELETE SDLK_DELETE
#define CN_KEY__0_PAD SDLK_KP0
#define CN_KEY__1_PAD SDLK_KP1
#define CN_KEY__2_PAD SDLK_KP2
#define CN_KEY__3_PAD SDLK_KP3
#define CN_KEY__4_PAD SDLK_KP4
#define CN_KEY__5_PAD SDLK_KP5
#define CN_KEY__6_PAD SDLK_KP6
#define CN_KEY__7_PAD SDLK_KP7
#define CN_KEY__8_PAD SDLK_KP8
#define CN_KEY__9_PAD SDLK_KP9
#define CN_KEY__PERIOD_PAD SDLK_KP_PERIOD
#define CN_KEY__SLASH_PAD SDLK_KP_DIVIDE
#define CN_KEY__ASTERICK_PAD SDLK_KP_MULTIPLY
#define CN_KEY__MINUS_PAD SDLK_KP_MULTIPLY
#define CN_KEY__PLUS_PAD SDLK_KP_PLUS
#define CN_KEY__ENTER_PAD SDLK_KP_ENTER
#define CN_KEY__UP SDLK_UP
#define CN_KEY__DOWN SDLK_DOWN
#define CN_KEY__RIGHT SDLK_RIGHT
#define CN_KEY__LEFT SDLK_LEFT
#define CN_KEY__INSERT SDLK_INSERT
#define CN_KEY__HOME SDLK_HOME
#define CN_KEY__END SDLK_END
#define CN_KEY__PAGEUP SDLK_PAGEUP
#define CN_KEY__PAGEDOWN SDLK_PAGEDOWN
#define CN_KEY__F1 SDLK_F1
#define CN_KEY__F2 SDLK_F2
#define CN_KEY__F3 SDLK_F3
#define CN_KEY__F4 SDLK_F4
#define CN_KEY__F5 SDLK_F5
#define CN_KEY__F6 SDLK_F6
#define CN_KEY__F7 SDLK_F7
#define CN_KEY__F8 SDLK_F8
#define CN_KEY__F9 SDLK_F9
#define CN_KEY__F10 SDLK_F10
#define CN_KEY__F11 SDLK_F11
#define CN_KEY__F12 SDLK_F12
#define CN_KEY__NUMLOCK SDLK_NUMLOCK
#define CN_KEY__CAPSLOCK SDLK_CAPSLOCK
#define CN_KEY__SCROLLOCK SDLK_SCROLLOCK
#define CN_KEY__RSHIFT SDLK_RSHIFT
#define CN_KEY__LSHIFT SDLK_LSHIFT
#define CN_KEY__RCTRL SDLK_RCTRL
#define CN_KEY__LCTRL SDLK_LCTRL
#define CN_KEY__RALKT SDLK_RALT
#define CN_KEY__LALT SDLK_LALT
#define CN_KEY__LSUPER SDLK_LSUPER
#define CN_KEY__RSUPER SDLK_RSUPER
#define CN_KEY__PRINTSCREEN SDLK_PRINTSCREEN
#define CN_KEY__MENU SDLK_MENU

#endif
#endif /*SDL12_BINDS_H_*/
