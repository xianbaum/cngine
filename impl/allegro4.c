#ifdef CN_ALLEGRO4
#include "../cngine.h"

Allegro4_Backend *allegro4_backend = NULL;

CN_bool CN_exit;

static void CN_close_button()
{
    CN_exit = CN_true;
} END_OF_FUNCTION(CN_close_button)

static void keyboardcallback(int scancode)
{
    if(scancode & 0b10000000 && allegro4_backend->keydown[scancode] == 1) {
        allegro4_backend->keyup[scancode] = 1;
        allegro4_backend->keydown[scancode] = 0;
    } else if(allegro4_backend->keyup[scancode] == 1) {
        allegro4_backend->keyup[scancode] = 0;
        allegro4_backend->keydown[scancode] = 1;
    }
} END_OF_FUNCTION(keyboardcallback)

static void CN__load_allegro_properties_file(CN_Options *o)
{
    FILE *properties_file = fopen("allegro.csv", "r");
    char buffer[1024];
    char *substr;
    int gfx_mode = 0;
    if(properties_file == NULL) {
        allegro4_backend->audio_backend = CN_AUDIO_NONE;
        return;
    }
    fgets(buffer,sizeof(buffer), properties_file);
    substr = strtok(buffer, ",");
    allegro4_backend->audio_backend = (int)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    gfx_mode = (int)strtod(substr, NULL);
    if(o->mode == CN_MODE_USE_PROPERTIES) {
        o->mode = gfx_mode;
    }
    fclose(properties_file);
    if(allegro4_backend->audio_backend < 0 ||
       allegro4_backend->audio_backend > 2) {
        allegro4_backend->audio_backend = CN_AUDIO_NONE;
    }
}

#ifdef CN_DUMB
static DumbProperties CN__load_dumb_properties_file()
{
    FILE *properties_file = fopen("dumb.csv", "r");
    char buffer[1024];
    char *substr;
    DumbProperties properties;
    CN_Assert(properties_file != NULL);
    fgets(buffer,sizeof(buffer), properties_file);
    substr = strtok(buffer, ",");
    properties.channels = (int)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    properties.volume = (float)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    properties.buffersize = (long)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    properties.frequency = (int)strtod(substr, NULL);
    substr = strtok(NULL, ",");
    properties.resample_quality = (int)strtod(substr, NULL);
    dumb_resampling_quality = properties.resample_quality;
    fclose(properties_file);
    return properties;
}
#endif

CN_bool CN_Backend_Init(CN_Options *options)
{
    int errors = allegro_init(), mode;
    if(options == NULL) {
        CN_options = options = CN_Options_CreateBlank();
    } else {
        CN_options = malloc(sizeof(CN_Options));
        memcpy(CN_options, options, sizeof(CN_Options));
    }
    if(!CN_Logger_Init(options->loggingLevel)) {
        CN_LogWarning(("Unable to initialize logger"));
    }
    CN_Assert(allegro4_backend == NULL);
    allegro4_backend = CN_malloc(sizeof(*allegro4_backend));
    CN_Assert(allegro4_backend != NULL);
    CN__load_allegro_properties_file(options);

    switch(options->mode) {
    case CN_MODE_FULLSCREEN:
        mode = GFX_AUTODETECT_FULLSCREEN;
        break;
    case CN_MODE_WINDOWED:
    case CN_MODE_WINDOWED_BORDERLESS:
        mode = GFX_AUTODETECT_WINDOWED;
    case CN_MODE_ALLEGRO_GFX_SAFE:
        mode = GFX_SAFE;
        break;
    case CN_MODE_TEXT:
        mode = GFX_TEXT;
        break;
    case CN_MODE_ALLEGRO_AUTODETECT:
    default:
        mode = GFX_AUTODETECT;
        break;
    }

    allegro4_backend->CN_msecs = 0;
    allegro4_backend->CN_ticks = 0;

    CN_Expect(errors == 0, allegro_error);
    /*set_window_title("City Night");*/
    set_color_depth(8);
    errors += install_keyboard() + install_timer() +
        set_gfx_mode(mode, options->width, options->height, 0, 0);
    CN_Expect(errors == 0, allegro_error);
    CN_Assert(allegro4_backend != NULL);

    allegro4_backend->buffer = create_bitmap(options->width, options->height);
    CN_register_pointer(allegro4_backend->buffer);

/*    allegro4_backend->buffer = screen;*/
    CN_Assert(allegro4_backend->buffer != NULL);
#ifdef CN_ALGIF
    algif_init();
#endif
    LOCK_VARIABLE(allegro4_backend->CN_msecs);
    LOCK_FUNCTION(CN_IncrementTimer);
    LOCK_VARIABLE(allegro4_backend->CN_ticks);
    LOCK_VARIABLE(allegro4_backend->keyup);
    LOCK_VARIABLE(allegro4_backend->keydown);
    LOCK_FUNCTION(CN_IncrementTicker);
    LOCK_FUNCTION(CN_close_button);
    LOCK_FUNCTION(keyboardcallback);
    keyboard_lowlevel_callback = keyboardcallback;
    memset(allegro4_backend->keyup, 1, KEY_MAX - 1);
    memset(allegro4_backend->keydown, 1, KEY_MAX - 1);
    set_keyboard_rate(250, 100);
    set_close_button_callback(CN_close_button);
    install_int_ex(CN_IncrementTimer, MSEC_TO_TIMER(MSEC_DIV_60));
    install_int_ex(CN_IncrementTicker, BPS_TO_TIMER(FRAME_TIME));
    if(allegro4_backend->audio_backend == CN_AUDIO_MOD) {
#ifdef CN_DUMB
        allegro4_backend->dumb_properties = CN__load_dumb_properties_file();
        dumb_register_stdfiles();
        allegro4_backend->duh_player = NULL;
#elif defined CN_JGMOD
        reserve_voices (8, -1);
#endif
        install_sound(DIGI_AUTODETECT, MIDI_NONE, NULL);
#ifdef CN_JGMOD
        install_mod(8);
#endif
    } else {
        install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL);
#ifdef CN_DUMB
        allegro4_backend->duh_player = NULL;
#endif
    }
    allegro4_backend->music_playing = CN_false;
    return CN_true;
}

void CN_Backend_Update()
{
#ifdef CN_DUMB
    if(allegro4_backend->audio_backend == CN_AUDIO_MOD &&
       allegro4_backend->duh_player != NULL)
    {
        al_poll_duh(allegro4_backend->duh_player);
    }
#endif
}

void CN_Backend_FinalizeFrame()
{
    if((CN_bool)keyboard_needs_poll()) poll_keyboard();
}

CN_bool CN_Backend_Deinit(void)
{
    CN_Assert(allegro4_backend != NULL);
    /*
    CN_Image_Free(allegro4_backend->buffer);
    */
    if(allegro4_backend->audio_backend == CN_AUDIO_MOD)
    {
#ifdef CN_DUMB
        dumb_exit();
#endif
    }
    remove_int(CN_IncrementTimer);
    remove_int(CN_IncrementTicker);
    CN_free(allegro4_backend);
    CN_memory_atexit();
    allegro4_backend = NULL;
    return CN_true;
}

CN_bool CN_Backend_CanReinit(void)
{
	return CN_false;
}

CN_bool CN_ExitCondition()
{
    return CN_exit;
}

/*Image*/

CN_Image *CN_Image_Load(const char *path)
{
    int length = strlen(path);
    CN_Image *image = NULL;
    if(path[length-3] == 'g' &&
		path[length-2] == 'i' &&
		path[length-1] == 'f')
    {
#ifdef CN_ALGIF        
        image = load_gif(path, NULL);
#endif
    }
    else
    {
        image = load_pcx(path, NULL);
    }

    CN_Try(image != NULL,
           allegro_error);
    CN_register_pointer(image);
    return image;
}

CN_Image *CN_Image_Create(int width, int height)
{
    BITMAP *b = create_bitmap(width, height);
    clear_bitmap(b);
    CN_register_pointer(b);
    return b;
}

/* These are where the original draw functions were */
#include "../img/draw.c"

size_t CN_Image_EstimateSizeOf(const CN_Image *image)
{
    return sizeof(CN_Image) + (image->w * image->h);
}

/*void CN_Image_FastDraw(CN_Image *image, const CN_Rect *source, const CN_Rect *dest)
{
    blit(image, allegro4_backend->buffer,
         source->x, source->y, dest->x, dest->y, source->w, source->h);
}

void CN_Image_Draw(CN_Image *image, const CN_Rect *source, const CN_Rect *dest)
{
    masked_blit(image, allegro4_backend->buffer,
                source->x, source->y, dest->x, dest->y, source->w, source->h);
}

void CN_Image_DrawScaled(CN_Image *image, const CN_Rect *source,
                         const CN_Rect *dest)
{
    masked_stretch_blit(image, allegro4_backend->buffer,
                        source->x, source->y, source->w, source->h,
                        dest->x, dest->y, dest->w, dest->h);
                        }*/

void CN_Image_GetSize(const CN_Image *bitmap, int *width, int *height)
{
    if(width != NULL)
    {
        *width = bitmap->w;
    }
    if(height != NULL)
    {
        *height = bitmap->h;
    }
}

CN_RGB CN_RGB_Create(unsigned char r, unsigned char g, unsigned char b)
{
    CN_RGB pixel;
    pixel.r = r;
    pixel.g = g;
    pixel.b = b;
    return pixel;
}

CN_RGB *CN_RGB_ExtractFromImage(const char *filename)
{
    CN_Image *image;
    CN_RGB *palette = CN_malloc(sizeof(PALETTE));
    CN_Assert(palette != NULL);
    image = load_pcx(filename, palette);
    if(!CN_Try(image != NULL, allegro_error))
    {
        return NULL;
    }
    destroy_bitmap(image);
    return palette;
}

void CN_RGB_Free(CN_RGB *palette)
{
    CN_free(palette);
}

CN_bool CN_RGB_Compare(const CN_RGB *pixel1, const CN_RGB *pixel2)
{
    return pixel1->r == pixel2->r &&
        pixel1->g == pixel2->g &&
        pixel1->b == pixel2->b;
}

void CN_Cls(void)
{
    clear_bitmap(allegro4_backend->buffer);
}

void CN_Image_Free(CN_Image *image)
{
    CN_unregister_pointer(image);
    destroy_bitmap(image);
}

void CN_Flip(void)
{
    BITMAP *b = allegro4_backend->buffer;
    blit(b, screen, 0,0,0,0, b->w, b->h);
}

void CN_RGB_Set(CN_RGB *palette)
{
    memcpy(allegro4_backend->palette, palette, sizeof(RGB)*PAL_SIZE);
    set_palette(palette);
}

void CN_RGB_SetRange(CN_RGB *palette, int from, int to)
{
    int i;
    for(i = from; i <=to+1; i++)
    {
        allegro4_backend->palette[i] = palette[i-from];
    }
    set_palette_range(allegro4_backend->palette,from,to+1,1);
}

/*Audio*/

CN_SoundByte *CN_SoundByte_Load(const char *path)
{
    /*CN_register_pointer(soundbyte);*/
    (void)path;
    return NULL;
}

void CN_SoundByte_Free(CN_SoundByte *soundbyte)
{
    CN_unregister_pointer(soundbyte);
    (void)soundbyte;
}

static CN_Music* CN_load_mod(const char *path)
{
    int length = strlen(path);
    CN_MOD *mod = NULL;
    if(strcmp(path + length - 2, "it") == 0) {
#ifdef CN_DUMB
        mod = dumb_load_it_quick(path);
#elif defined CN_JGMOD
        CN_LogError(("Pretty sure JGMOD doesn't support this"));
        mod = load_mod(path);
#endif
    } else {
#ifdef CN_DUMB
        mod = dumb_load_mod_quick(path);
#elif defined CN_JGMOD
        mod = load_mod(path);
#endif
    }
    CN_Check(mod != NULL);
    CN_register_pointer(mod);
    return mod;
}

static void CN_stop_mod()
{
    if(!CN_Check(allegro4_backend->music_playing)) return;
#ifdef CN_DUMB
    if(!CN_Check(allegro4_backend->duh_player != NULL)) return;
    al_stop_duh(allegro4_backend->duh_player);
    allegro4_backend->duh_player = NULL;
#elif defined CN_JGMOD
    stop_mod();
#endif
    allegro4_backend->music_playing = CN_false;
}

static void CN_free_mod(CN_Music* music)
{
#ifdef CN_DUMB
    unload_duh((CN_MOD*)music);
#elif defined CN_JGMOD
    destroy_mod((CN_MOD*)music);
#endif
    CN_unregister_pointer(music);
}

static void CN_play_mod(CN_Music* music)
{
#ifdef CN_DUMB
    if(!CN_Check(!allegro4_backend->music_playing
                 && allegro4_backend->duh_player == NULL
           )) {
        CN_stop_mod();
    }
#elif defined CN_JGMOD
    if(!CN_Check(!allegro4_backend->music_playing)) CN_stop_mod();
#endif
    if(!CN_Check(music != NULL)) return;
#ifdef CN_DUMB
    allegro4_backend->duh_player = al_start_duh((CN_MOD*)music,
      allegro4_backend->dumb_properties.channels, 0,
      allegro4_backend->dumb_properties.volume,
      allegro4_backend->dumb_properties.buffersize,
      allegro4_backend->dumb_properties.frequency);
#elif defined CN_JGMOD
    play_mod(music, CN_true);
#endif
    allegro4_backend->music_playing = CN_true;
}

static CN_Music* CN_load_midi(char *path)
{
    int length = strlen(path);
    char *copy;
    if(path[length-1] == 't' || path[length-1] == 'T') {
        copy = CN_malloc(sizeof(char) * (length + 2));
        strcpy(copy, path);
        copy[length] = 'd';
        copy[length-1] = 'i';
        copy[length-2] = 'm';
    } else {
        copy = CN_malloc(sizeof(char) * (length+1));
        strcpy(copy, path);
        copy[length-2] = 'i';
    }
    {CN_MIDI *midi;
        midi = load_midi(copy);
        if(midi == NULL)
        {
            printf(allegro_error);
        }
        CN_free(copy);
        CN_Try(midi != NULL,
               allegro_error);
        CN_register_pointer(midi);
        return midi;
    }
}

static void CN_free_midi(CN_Music* music)
{
    destroy_midi((CN_MIDI*)music);
    CN_unregister_pointer(music);
}

static void CN_play_midi(CN_Music* music)
{
    CN_Check(play_midi((CN_MIDI*)music, 1) == 0);
}
static void CN_stop_midi()
{
    midi_pause();
}

CN_MusicType CN_GetMusicType()
{
    return allegro4_backend->audio_backend;
}

CN_Music* CN_Music_Load(const char * path)
{
    switch(allegro4_backend->audio_backend) {
    case CN_AUDIO_MIDI:
        return CN_load_midi(path);
    case CN_AUDIO_MOD:
        return CN_load_mod(path);
    default:
        return NULL;
    }
}

void CN_Music_Free(CN_Music* music)
{
    switch(allegro4_backend->audio_backend) {
    case CN_AUDIO_MIDI:
        CN_free_midi(music);
        break;
    case CN_AUDIO_MOD:
        CN_free_mod(music);
        break;
    default:
        break;
    }
}

void CN_Music_Play(CN_Music* music)
{
    switch(allegro4_backend->audio_backend) {
    case CN_AUDIO_MIDI:
        CN_play_midi(music);
        break;
    case CN_AUDIO_MOD:
        CN_play_mod(music);
        break;
    default:
        break;
    }
}

void CN_Music_Stop()
{
    switch(allegro4_backend->audio_backend) {
    case CN_AUDIO_MIDI:
        CN_stop_midi();
        break;
    case CN_AUDIO_MOD:
        CN_stop_mod();
        break;
    default:
        break;
    }
}

#ifndef CN_CUSTOM_MAIN

int main(int argc, char **argv)
{
    CN_Init(argc, argv);
    while(!CN_ExitCondition())
    {
        CN_Frame(CN_GetDeltaTime());
    }
    CN_Deinit();
}

#endif

#else
typedef int cry_all4;
#endif /*CN_ALLEGRO4*/
