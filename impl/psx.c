#ifdef CN_PSX
#include "cngine.h"

const int CN__MAX_FILESIZE = 0x5000;
volatile unsigned int prim_list[0x4000];
volatile int display_is_old = 1;
volatile int time_counter = 0;
volatile int refresh_needed=0;
volatile unsigned char file_buffer[0x30000];

char *CN_fgets(char *buffer, int size, FILE *file)
{
    char character;
    if (file == NULL || size == 0)
    {
        return NULL;
    }
    while (--size > 0 && (character = getc(file)) != EOF)
    {
        if ((*buffer++ = character) == '\n')
        {
            *buffer++ = '\0';
            return buffer;
        }
    }
    return NULL;
}

static CN_bool CN__read_entire_file(char *buffer, int max, FILE *file)
{
    int size;
    fseek(file, 0, SEEK_END);
    size = ftell(file);
    fseek(file, 0, SEEK_SET);
    if (size > max)
    {
        CN_LogError(("File too big %s", size));
        return CN_false;
    }
    fread(buffer, sizeof(char), size, file);
    return CN_true;
}

CN_bool CN_exit = CN_false;
PSXBackend *PSX_backend;
static CN_bool __CN_IsKeyDown_toggle = CN_false;

CN_bool CN_Backend_Init()
{
    PSX_backend = malloc(sizeof(PSX_backend));
    PSX_Init();
    GsInit();
    GsSetList(prim_list);
    GsClearMem();
    PSX_backend->drawing_env.x = 0;
    PSX_backend->drawing_env.y = 0;
    PSX_backend->drawing_env.w = WIDTH;
    PSX_backend->drawing_env.h = HEIGHT;
    PSX_backend->drawing_env.draw_on_display = 1; /*true*/
    PSX_backend->display_env.x = 0;
    PSX_backend->display_env.y = 0;
    GsSetDrawEnv(&(PSX_backend->drawing_env));
    GsSetDispEnv(&(PSX_backend->display_env));
    CN_set_directory("cdrom://");
    return CN_true;
}

void CN_destory_backend()
{
    free(PSX_backend);
}

CN_bool CN_ExitCondition()
{
    return CN_exit;
}

CN_Image *CN_Image_Load(const char *path)
{
    GsImage gsi;

    if (SystemLoadFile(path) == CN_false)
    {
        return CN_false;
    }

    while (GfxIsGPUBusy() == CN_true);

    gfx_busy = CN_true;

    GsImageFromTim(&gsi, SystemGetBufferAddress() );

    GsSpriteFromImage(spr, &gsi, UPLOAD_IMAGE_FLAG);

    gfx_busy = CN_false;
    return CN_true;
}

CN_RGB *CN_RGB_ExtractFromImage(const char *path)
{
    CN_assert(path != NULL);
    return malloc(sizeof(BigPoop));
}

void CN_RGB_Free(CN_RGB *palette)
{
    free(palette);
}

void CN_Image_GetSize(CN_Image *surface, /*@out@*/ /*@null@*/ int *width,
                      /*@out@*/ /*@null@*/ int *height)
{
    CN_assert(surface != NULL);
    if (width != NULL)
    {
        *width = 100;
    }
    if (height != NULL)
    {
        *height = 100;
    }
}

CN_RGB CN_Image_GetRGB(CN_Image *image, int x, int y)
{
    CN_assert(image != NULL);
    CN_RGB poop;
    poop.compare = (x % 2) + (y % 2);
    return poop;
}

CN_RGB CN_RGB_Create(int a, int b, int c)
{
    CN_RGB poop;
    return poop;
}

CN_bool CN_RGB_Compare(CN_RGB *pixel1, CN_RGB *pixel2)
{
    CN_assert(pixel1 != NULL && pixel2 != NULL);
    return pixel1->compare == pixel2->compare;
}

void CN_Image_Free(/*@only@*/ CN_Image *image)
{
    CN_assert(image != NULL);
    free(image);
}

void CN_RGB_Set(CN_RGB *palette)
{
    CN_assert(palette != NULL);
    palette = palette + 1;
}

void CN_RGB_SetRange(CN_RGB *colors, int start, int end)
{
    return;
}

void CN_Image_Draw(CN_Image *surface, CN_Rect *source, CN_Rect *dest)
{
    CN_assert(surface != NULL && source != NULL && dest != NULL);
}

int CN_GetDeltaTime(void)
{
    return 12;
}

int CN_GetFramesSinceLastCall(void)
{
    return 1;
}

void CN_Backend_Update()
{
    if (PSX_backend->current_song != NULL)
    {
        /*MODPlay(PSX_backend->current_song);*/
    }
}

CN_SoundByte *CN_SoundByte_Load(char *path)
{
    CN_assert(path != NULL);
    return malloc(sizeof(BigPoop));
}

void CN_SoundByte_Free(CN_SoundByte *soundbyte)
{
    CN_assert(soundbyte != NULL);
    free(soundbyte);
}

CN_Music *CN_Music_Load(char *path)
{
    char buffer[CN__MAX_FILESIZE];
    FILE *file = CN_fopen(path, "r");
    ModMusic *music = NULL;
    if (file == NULL)
    {
        CN_LogError(("UNable to open file: %s", path));
        return music;
    }
    if (CN__read_entire_file(buffer, CN__MAX_FILESIZE, file))
    {
        CN_LogError(("Unable to load music!"));
        return music;
    }
    music = MODLoad(buffer);
    return (CN_Music*)music;
}

void CN_Music_Free(CN_Music *music)
{
    MODUnload(music);
}

void CN_Music_Play(CN_Music* mu)
{
    CN_assert(PSX_backend->current_song == NULL);
    if (mu == NULL)
    {
        return;
    }
    PSX_backend->current_song = mu;
    return;
}
void CN_Music_Stop(CN_Music* mu)
{
    CN_assert(PSX_backend->current_song == NULL);
    PSX_backend->current_song = NULL;
    return;
}
CN_MusicType CN_GetMusicType()
{
    return 2;
}

CN_bool CN_IsKeyDown(int key)
{
    __CN_IsKeyDown_toggle = !__CN_IsKeyDown_toggle;
    if (key == CN_KEY_ESC)
    {
        return CN_false;
    }
    return __CN_IsKeyDown_toggle;
}

#else
typedef int CN_cry_psx;
#endif /*CN_PSX*/
