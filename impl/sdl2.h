/**
 * @file sdl2.h
 * @author Christian Michael Baum
 * @brief Implementation used if compiling with SDL 2
 * Refer to cngine.h for API
 **/

#ifndef SDL2_BINDS_H_
#define SDL2_BINDS_H_

#ifdef CN_SDL2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER

#include <SDL.h>

#ifndef CN_NO_IMAGE
#include <SDL_image.h>
#endif /* CN_NO_IMAGE */

#ifndef CN_NO_MIXER
#include <SDL_mixer.h>
#endif /* CN_NO_MIXER */

#else /*ifdef _MSC_VER*/

#include <SDL.h>

#ifndef CN_NO_IMAGE

#ifdef __APPLE__
#include <SDL2_image/SDL_image.h>
#else /*__APPLE__*/
#include <SDL2/SDL_image.h>
#endif /*__APPLE__*/

#endif /* CN_NO_IMAGE */

#ifndef CN_NO_MIXER

#ifdef __APPLE__
#include <SDL2_mixer/SDL_mixer.h>
#else /* __APPLE__ */
#include <SDL2/SDL_mixer.h>
#endif /* __APPLE__ */

#endif /* CN_NO_MIXER */

#endif /*ifdef _MSC_VER*/

#ifdef USE_OPENMPT_INSTEAD_
#include <libopenmpt/libopenmpt.h>
#include <libopenmpt/libopenmpt_stream_callbacks_file.h>
#endif /* USE_OPENMPT_INSTEAD_ */

#include "../core/chelpers.h"
#include "../core/cnerror.h"
#include "../core/cnmem.h"

#define END_OF_MAIN()
#define NUMBER_OF_SDL_SCANCODES 285

typedef SDL_Color CN_RGB_Impl;
typedef SDL_Surface CN_Image_Impl;
typedef struct SDLBackend {
    CN_MusicType audio_backend;
    SDL_Surface  *SDL_screen;
    SDL_Surface  *SDL2_32bitscreen;
    SDL_Window   *SDL2_window;
    SDL_Renderer *SDL2_renderer; 
    SDL_Texture  *SDL2_texture;
    SDL_Surface  *black_screen;
    const Uint8 *keyboard_state;
    Uint8 *keyup_state;
    Uint8 *keypress_state;
    unsigned char tickCounter;
    unsigned char mouseCurrentTick;
    Uint8 lastMouseState;
    int lastMouseX;
    int lastMouseY;
} SDLBackend;
#ifndef CN_NO_MIXER
typedef Mix_Chunk CN_SoundByte_Impl;
typedef Mix_Music CN_MIDI_Impl;
#else
typedef void CN_SoundByte_Impl;
typedef void CN_MOD_Impl;
typedef void CN_MIDI_Impl;
#endif
#ifdef CN_USE_OPENMPT_INSTEAD
typedef SDL_AudioSpec CN_MOD_Impl;
typedef void CN_Music_Impl;
#elif defined CN_NO_MIXER
typedef void CN_MOD_Impl;
typedef void CN_Music_Impl;
typedef void CN_MIDI_Impl;
#else
typedef Mix_Music CN_MOD_Impl;
typedef Mix_Music CN_Music_Impl;
typedef Mix_Music CN_MIDI_Impl;
#endif
typedef SDL_Scancode CN_Key_Impl;
typedef Uint8 CN_MouseButton_Impl;

/*Input macros*/
#define CN_KEY__0 SDL_SCANCODE_0
#define CN_KEY__1 SDL_SCANCODE_1
#define CN_KEY__2 SDL_SCANCODE_2
#define CN_KEY__3 SDL_SCANCODE_3
#define CN_KEY__4 SDL_SCANCODE_4
#define CN_KEY__5 SDL_SCANCODE_5
#define CN_KEY__6 SDL_SCANCODE_6
#define CN_KEY__7 SDL_SCANCODE_7
#define CN_KEY__8 SDL_SCANCODE_8
#define CN_KEY__9 SDL_SCANCODE_9
#define CN_KEY__A SDL_SCANCODE_A
#define CN_KEY__B SDL_SCANCODE_B
#define CN_KEY__BACKSLASH SDL_SCANCODE_BACKSLASH
#define CN_KEY__BACKSPACE SDL_SCANCODE_BACKSPACE
#define CN_KEY__C SDL_SCANCODE_C
#define CN_KEY__CAPSLOCK SDL_SCANCODE_CAPSLOCK
#define CN_KEY__COMMA SDL_SCANCODE_COMMA
#define CN_KEY__D SDL_SCANCODE_D
#define CN_KEY__DELETE SDL_SCANCODE_DELETE
#define CN_KEY__DOWN SDL_SCANCODE_DOWN
#define CN_KEY__E SDL_SCANCODE_E
#define CN_KEY__END SDL_SCANCODE_END
#define CN_KEY__EQUALS SDL_SCANCODE_EQUALS
#define CN_KEY__ESC SDL_SCANCODE_ESCAPE
#define CN_KEY__ SDL_SCANCODE_F
#define CN_KEY__F1  SDL_SCANCODE_F1
#define CN_KEY__F10 SDL_SCANCODE_F10
#define CN_KEY__F11 SDL_SCANCODE_F11
#define CN_KEY__F12 SDL_SCANCODE_F12
#define CN_KEY__F2 SDL_SCANCODE_F2
#define CN_KEY__F3 SDL_SCANCODE_F3
#define CN_KEY__F4 SDL_SCANCODE_F4
#define CN_KEY__F5 SDL_SCANCODE_F5
#define CN_KEY__F6 SDL_SCANCODE_F6
#define CN_KEY__F7 SDL_SCANCODE_F7
#define CN_KEY__F8 SDL_SCANCODE_F8
#define CN_KEY__F9 SDL_SCANCODE_F9
#define CN_KEY__G SDL_SCANCODE_G
#define CN_KEY__H SDL_SCANCODE_H
#define CN_KEY__HOME SDL_SCANCODE_HOME
#define CN_KEY__I SDL_SCANCODE_I
#define CN_KEY__INSERT SDL_SCANCODE_INSERT
#define CN_KEY__J SDL_SCANCODE_J
#define CN_KEY__K SDL_SCANCODE_K
#define CN_KEY__0_PAD SDL_SCANCODE_KP_0
#define CN_KEY__1_PAD SDL_SCANCODE_KP_1
#define CN_KEY__2_PAD SDL_SCANCODE_KP_2
#define CN_KEY__3_PAD SDL_SCANCODE_KP_3
#define CN_KEY__4_PAD SDL_SCANCODE_KP_4
#define CN_KEY__5_PAD SDL_SCANCODE_KP_5
#define CN_KEY__6_PAD SDL_SCANCODE_KP_6
#define CN_KEY__7_PAD SDL_SCANCODE_KP_7
#define CN_KEY__8_PAD SDL_SCANCODE_KP_8
#define CN_KEY__9_PAD SDL_SCANCODE_KP_9
#define CN_KEY__ENTER_PAD SDL_SCANCODE_KP_ENTER
#define CN_KEY__EQUALS_PAD SDL_SCANCODE_KP_EQUALS
#define CN_KEY__SLASH_PAD SDL_SCANCODE_KP_DIVIDE
#define CN_KEY__ASTERICK_PAD SDL_SCANCODE_KP_MULTIPLY
#define CN_KEY__MINUS_PAD SDL_SCANCODE_KP_MINUS
#define CN_KEY__PERIOD_PAD SDL_SCANCODE_KP_PERIOD
#define CN_KEY__PLUS_PAD SDL_SCANCODE_KP_PLUS
#define CN_KEY__L SDL_SCANCODE_L
#define CN_KEY__LALT SDL_SCANCODE_LALT
#define CN_KEY__LCTRL SDL_SCANCODE_LCTRL
#define CN_KEY__LEFT SDL_SCANCODE_LEFT
#define CN_KEY__LBRACKET SDL_SCANCODE_LEFTBRACKET
#define CN_KEY__LSUPER SDL_SCANCODE_LGUI
#define CN_KEY__LSHIFT SDL_SCANCODE_LSHIFT
#define CN_KEY__M SDL_SCANCODE_M
#define CN_KEY__MENU SDL_SCANCODE_MENU
#define CN_KEY__MINUS SDL_SCANCODE_MINUS
#define CN_KEY__N SDL_SCANCODE_N
#define CN_KEY__O SDL_SCANCODE_O
#define CN_KEY__P SDL_SCANCODE_P
#define CN_KEY__PAGEDOWN SDL_SCANCODE_PAGEDOWN
#define CN_KEY__PAGEUP SDL_SCANCODE_PAGEUP
#define CN_KEY__PAUSE SDL_SCANCODE_PAUSE
#define CN_KEY__PERIOD SDL_SCANCODE_PERIOD
#define CN_KEY__PRINTSCREEN SDL_SCANCODE_PRINTSCREEN
#define CN_KEY__Q SDL_SCANCODE_Q
#define CN_KEY__R SDL_SCANCODE_R
#define CN_KEY__RALT SDL_SCANCODE_RALT
#define CN_KEY__RCTRLSDL_SCANCODE_RCTRL
#define CN_KEY__ENTER SDL_SCANCODE_RETURN
#define CN_KEY__RSUPER SDL_SCANCODE_RGUI
#define CN_KEY__RIGHT SDL_SCANCODE_RIGHT
#define CN_KEY__RBRACKET SDL_SCANCODE_RIGHTBRACKET
#define CN_KEY__RSHIFT SDL_SCANCODE_RSHIFT
#define CN_KEY__S SDL_SCANCODE_S
#define CN_KEY__SCROLLOCK SDL_SCANCODE_SCROLLOCK
#define CN_KEY__SEMICOLON SDL_SCANCODE_SEMICOLON
#define CN_KEY__SLASH SDL_SCANCODE_SLASH
#define CN_KEY__SPACE SDL_SCANCODE_SPACE
#define CN_KEY__T SDL_SCANCODE_T
#define CN_KEY__TAB SDL_SCANCODE_TAB
#define CN_KEY__U SDL_SCANCODE_U
#define CN_KEY__UP SDL_SCANCODE_UP
#define CN_KEY__V SDL_SCANCODE_V
#define CN_KEY__W SDL_SCANCODE_W
#define CN_KEY__X SDL_SCANCODE_X
#define CN_KEY__Y SDL_SCANCODE_Y
#define CN_KEY__Z SDL_SCANCODE_Z
#define CN__MOUSE1 SDL_BUTTON_LEFT
#define CN__MOUSE2 SDL_BUTTON_RIGHT
#define CN__MOUSE3 SDL_BUTTON_MIDDLE

#endif /*USE_SDL2_*/
#endif /*SDL2_BINDS_H_*/
