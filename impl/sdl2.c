#ifdef CN_SDL2

#include "../cngine.h"

#ifdef CN_MIKMOD_HACK
#include <mikmod.h> 
#endif

/*what the heck msvc
  it didn't respect #elif defined*/
#ifdef CN_MODPLUG_HACK
#include <libmodplug/modplug.h>
#endif

#include <string.h>

#include "part/sdl.c"


SDLBackend *sdl_backend = NULL;


CN_bool CN_exit = CN_false;

static void CN__load_sdl2_properties_file(void)
{
    FILE *properties_file = fopen("sdl2.csv", "r");
    char buffer[1024];
    char *substr;
    CN_Assert(sdl_backend != NULL);
    if(properties_file == NULL) return;
    CN_Expect(fgets(buffer,sizeof(buffer), properties_file) != NULL,
          "Error reading properties file");
    substr = strtok(buffer, ",");
    sdl_backend->audio_backend = (int)strtod(substr, NULL);
    CN_Expect(fclose(properties_file) == 0,
          "Error closing properties file");
    if(sdl_backend->audio_backend > 2) sdl_backend->audio_backend = 2;
}


CN_bool CN_Backend_Init(CN_Options *options)
{
    if(options == NULL) {
        CN_options = options = CN_Options_CreateBlank();
    } else {
        CN_options = malloc(sizeof(CN_Options));
        memcpy(CN_options, options, sizeof(CN_Options));
    }
#if defined(__MINGW32__) || defined(__CYGWIN__)
    SDL_SetHint(SDL_HINT_WINDOWS_DISABLE_THREAD_NAMING, "1");
#elif defined __linux__
    SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0");
#endif
    CN_Assert(sdl_backend == NULL);
    CN_Logger_Init(options->loggingLevel);
    if(!CN_Try(SDL_Init(SDL_INIT_EVERYTHING) == 0,
               SDL_GetError())) {
        return CN_false;
    }
    
    sdl_backend = CN_malloc(sizeof(*sdl_backend));
    sdl_backend->tickCounter = 0;
    sdl_backend->mouseCurrentTick = 0;
    sdl_backend->lastMouseState = 0;
    sdl_backend->lastMouseX = 0;
    sdl_backend->lastMouseY = 0;
    sdl_backend->black_screen = NULL;
    sdl_backend->audio_backend = options->musicType;
    CN__load_sdl2_properties_file();

    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);

    if(options->mode != CN_MODE_TEXT) {
        int modeFlags;
        switch(options->mode) {
        case CN_MODE_FULLSCREEN: modeFlags = SDL_WINDOW_FULLSCREEN; break;
        case CN_MODE_WINDOWED_BORDERLESS: modeFlags=SDL_WINDOW_BORDERLESS;break;
        case CN_MODE_SDL_FLAGS: modeFlags = options->modeFlags; break;
        case CN_MODE_WINDOWED:
          /* fall through */
        default: modeFlags = SDL_WINDOW_RESIZABLE; break;
        }
        sdl_backend->SDL2_window = SDL_CreateWindow(options->windowTitle,
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            options->width, options->height, modeFlags);
        sdl_backend->SDL2_renderer = SDL_CreateRenderer(sdl_backend->SDL2_window,
                             -1, SDL_RENDERER_SOFTWARE);
    } else {
        sdl_backend->SDL2_window = NULL;
        sdl_backend->SDL2_renderer = NULL;
    }

    sdl_backend->SDL_screen = SDL_CreateRGBSurface(0, options->width, CN_options->height, 8,
                                                     0, 0, 0, 0);
    sdl_backend->SDL2_32bitscreen = SDL_CreateRGBSurface(0, options->width, CN_options->height, 32,
                                                          0, 0, 0, 0);
    CN_Expect(sdl_backend->SDL_screen != NULL, SDL_GetError());
    
    if(sdl_backend->SDL2_renderer != NULL) {
        sdl_backend->SDL2_texture = SDL_CreateTexture(sdl_backend->SDL2_renderer,
            SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, CN_options->width, CN_options->height);
    } else sdl_backend->SDL2_texture = NULL;

    sdl_backend->keyboard_state = SDL_GetKeyboardState(NULL);
    sdl_backend->keyup_state = malloc(sizeof(Uint8) * NUMBER_OF_SDL_SCANCODES);
    memset(sdl_backend->keyup_state, 0, NUMBER_OF_SDL_SCANCODES-1);
    sdl_backend->keypress_state = malloc(sizeof(Uint8) * NUMBER_OF_SDL_SCANCODES);

    if(options->mouseHidden) {
        SDL_ShowCursor(SDL_DISABLE);
    } else {
        SDL_ShowCursor(SDL_ENABLE);
    }
    
    memset(sdl_backend->keypress_state, 0, NUMBER_OF_SDL_SCANCODES-1);
#ifndef CN_NO_MIXER
    CN_Try((Mix_Init(MIX_INIT_MOD) & MIX_INIT_MOD) == MIX_INIT_MOD,
       Mix_GetError());
    CN_Try(Mix_OpenAudio(48000, MIX_DEFAULT_FORMAT, 2, 4096) == 0,
       Mix_GetError());
#endif
    /*Mixer*/
#ifdef CN_MODPLUG_HACK
    {ModPlug_Settings settings;
        ModPlug_GetSettings(&settings);
        settings.mLoopCount = -1;
        ModPlug_SetSettings(&settings);
    }
#endif
    return CN_true;
}

CN_Image *CN_Image_Load(const char * path)
{
    CN_Image *image_surface;
#ifndef CN_NO_IMAGE
    image_surface = IMG_Load(path);
#else
    image_surface = SDL_LoadBMP(path);
#endif
    CN_register_pointer(image_surface);
    
    if(!CN_Try(image_surface != NULL,
           SDL_GetError())) {
        return NULL;
    }
    CN_Assert(sdl_backend != NULL);
    return image_surface;
}

CN_bool CN_Backend_Deinit(void)
{
    if(sdl_backend == NULL) CN_false;
    SDL_FreeSurface( sdl_backend->SDL_screen );
    SDL_FreeSurface( sdl_backend->SDL2_32bitscreen );
    if(sdl_backend->black_screen != NULL) {
        SDL_FreeSurface(sdl_backend->black_screen);
    }
    if(sdl_backend->SDL2_texture != NULL) {
        SDL_DestroyTexture(sdl_backend->SDL2_texture);
    }
    if(sdl_backend->SDL2_renderer != NULL) {
        SDL_DestroyRenderer(sdl_backend->SDL2_renderer);
    }
    if(sdl_backend->SDL2_window != NULL) {
        SDL_DestroyWindow(sdl_backend->SDL2_window);
    }
#ifndef CN_NO_MIXER
    Mix_Quit();
#endif
#ifndef CN_NO_IMAGE
    IMG_Quit();
#endif
    SDL_Quit();
    CN_Logger_Destroy();
    CN_free(sdl_backend);
    CN_memory_atexit();
    sdl_backend = NULL;
    return CN_false;
}

CN_bool CN_CanReinit(void)
{
    return CN_false;
}

void CN_RGB_SetRange(CN_RGB *palette, int start, int end)
{
    CN_Assert(sdl_backend != NULL);
    if(palette == NULL) return;
    CN_Try(SDL_SetPaletteColors(sdl_backend->SDL_screen->format->palette,
                palette, start, end+1-start) == 0, SDL_GetError());
}

CN_RGB *CN_RGB_ExtractFromImage(const char *path)
{
    CN_Image *image = NULL;
    SDL_Color *copied_colors = NULL;
#ifndef CN_NO_IMAGE
    image = IMG_Load(path);
#else
    image = SDL_LoadBMP(path);
#endif
    if(!CN_Try(image != NULL, SDL_GetError())) return NULL;
    copied_colors = CN_malloc(sizeof(SDL_Color)*
                  image->format->palette->ncolors);
    memcpy(copied_colors, image->format->palette->colors,
       sizeof(SDL_Color)*image->format->palette->ncolors);
    CN_Image_Free(image);
    return copied_colors;
}

void CN_Flip(void)
{
    void *pixels;
    int pitch = 1,
        window_w = CN_options->width,
        window_h = CN_options->height;
    SDL_Rect dest;
    const double aspect_ratio = ((double)CN_options->width)/((double)CN_options->height);
    CN_Assert(sdl_backend != NULL);
    if(sdl_backend->SDL2_window != NULL) {
        SDL_GetWindowSize(sdl_backend->SDL2_window, &window_w, &window_h);
    }
    if(((double)window_w/(double)window_h) >= (double)aspect_ratio) {
        dest.y = 0;
        dest.h = window_h;
        dest.w = (int)(window_h * aspect_ratio);
        dest.x = (window_w - dest.w)/2;
    } else {
        dest.x = 0;
        dest.w = window_w;
        dest.h = (int)(window_w/aspect_ratio);
        dest.y = (window_h - dest.h)/2;
    }
    SDL_BlitSurface(sdl_backend->SDL_screen, NULL,
            sdl_backend->SDL2_32bitscreen, NULL);
    if(sdl_backend->SDL2_texture != NULL) {
        SDL_LockTexture(sdl_backend->SDL2_texture, NULL, &pixels, &pitch);
        SDL_ConvertPixels(sdl_backend->SDL2_32bitscreen->w,
                          sdl_backend->SDL2_32bitscreen->h,
                          sdl_backend->SDL2_32bitscreen->format->format,
                          sdl_backend->SDL2_32bitscreen->pixels,
                          sdl_backend->SDL2_32bitscreen->pitch,
                          SDL_PIXELFORMAT_ARGB8888,
                          pixels, pitch);
        SDL_UnlockTexture(sdl_backend->SDL2_texture);
        if(sdl_backend->SDL2_renderer != NULL) {
            SDL_RenderClear(sdl_backend->SDL2_renderer);
            SDL_RenderCopy(sdl_backend->SDL2_renderer,
                           sdl_backend->SDL2_texture, NULL, &dest);
            SDL_RenderPresent(sdl_backend->SDL2_renderer);
        }
    }
}

/* These are where the original draw functions were */
#include "../img/draw.c"

void CN_Cls(void)
{
    CN_Assert(sdl_backend != NULL);
    if(sdl_backend->black_screen == NULL) {
        sdl_backend->black_screen = SDL_CreateRGBSurface(0, CN_options->width, CN_options->height, 8,
                                                          0, 0, 0, 0);
        CN_Try(SDL_SetPaletteColors(sdl_backend->black_screen->format->palette,
                                    sdl_backend->SDL_screen->
                                    format->palette->colors, 0, 256) == 0,
               SDL_GetError());
    }
    CN_Image_Draw(sdl_backend->black_screen, sdl_backend->SDL_screen, 0, 0);
}

void CN_Backend_Update(void)
{
    sdl_backend->tickCounter++;
    _pollSDLEvents();
}

#ifndef CN_CUSTOM_MAIN

int main(int argc, char* argv[])
{
    CN_Init(argc, argv);
    while(!CN_ExitCondition())
    {
        CN_Frame(CN_GetDeltaTime());
    }
    CN_Deinit();
    return 0;
}

#endif

#else
typedef int cry_sdl2;
#endif /*CN_SDL2*/
