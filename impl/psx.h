/**
 * @file psx.h
 * @author Christian Michael Baum
 * @brief Incomplete implementation of PSX port
 **/

#ifndef PSX_BINDS_H_
#define PSX_BINDS_H_

#if defined CN_PSX

#include <stdio.h>

#include "../core/chelpers.h"
#include "../core/cnerror.h"
#include "../core/cnmem.h"

#include <modplay.h>
#include <psx.h>
#include <psxgpu.h>

#define END_OF_MAIN()

typedef ModMusic CN_Music_Impl;

typedef struct PSXBackend {
    GsDrawEnv drawing_env;
    GsDispEnv display_env;
    CN_Music *current_song;
} PSXBackend;

typedef struct PoopObj {
    int compare;
    int whatever[256];
} PoopObj;
typedef struct BigPoop {
    int whatever[123456];
} BigPoop;

typedef PoopObj CN_RGB_Impl;

typedef GsImage CN_Image_Impl;

typedef BigPoop CN_SoundByte_Impl;


/*Input macros*/
#define CN_KEY_0 0
#define CN_KEY_1 1
#define CN_KEY_2 2
#define CN_KEY_3 3
#define CN_KEY_4 4
#define CN_KEY_5 5
#define CN_KEY_6 6
#define CN_KEY_7 7
#define CN_KEY_8 8
#define CN_KEY_9 9
#define CN_KEY_A 10
#define CN_KEY_B 11
#define CN_KEY_BACKSLASH 12
#define CN_KEY_BACKSPACE 13
#define CN_KEY_C 14
#define CN_KEY_CAPSLOCK 15
#define CN_KEY_COMMA 16
#define CN_KEY_D 17
#define CN_KEY_DELETE 18
#define CN_KEY_DOWN 19
#define CN_KEY_E 20
#define CN_KEY_END 21
#define CN_KEY_EQUALS 22
#define CN_KEY_ESC 23
#define CN_KEY_ 24
#define CN_KEY_F1  25
#define CN_KEY_F10 26
#define CN_KEY_F11 27
#define CN_KEY_F12 28
#define CN_KEY_F2 29
#define CN_KEY_F3 30
#define CN_KEY_F4 31
#define CN_KEY_F5 32
#define CN_KEY_F6 33
#define CN_KEY_F7 34
#define CN_KEY_F8 35
#define CN_KEY_F9 36
#define CN_KEY_G 37
#define CN_KEY_H 38
#define CN_KEY_HOME 39
#define CN_KEY_I 40
#define CN_KEY_INSERT 41
#define CN_KEY_J 42
#define CN_KEY_K 43
#define CN_KEY_0_PAD 44
#define CN_KEY_1_PAD 45
#define CN_KEY_2_PAD 46
#define CN_KEY_3_PAD 47
#define CN_KEY_4_PAD 48
#define CN_KEY_5_PAD 49
#define CN_KEY_6_PAD 50
#define CN_KEY_7_PAD 51
#define CN_KEY_8_PAD 52
#define CN_KEY_9_PAD 53
#define CN_KEY_ENTER_PAD 54
#define CN_KEY_EQUALS_PAD 55
#define CN_KEY_SLASH_PAD 56
#define CN_KEY_ASTERICK_PAD 57
#define CN_KEY_MINUS_PAD 58
#define CN_KEY_PERDIO_PAD 59
#define CN_KEY_PLUS_PAD 60
#define CN_KEY_L 61
#define CN_KEY_LALT 62
#define CN_KEY_LCTRL 63
#define CN_KEY_LEFT 64
#define CN_KEY_LBRACKET 65
#define CN_KEY_LSUPER 66
#define CN_KEY_LSHIFT 67
#define CN_KEY_M 68
#define CN_KEY_MENU 69
#define CN_KEY_MINUS 70
#define CN_KEY_N 71
#define CN_KEY_O 72
#define CN_KEY_P 73
#define CN_KEY_PAGEDOWN 74
#define CN_KEY_PAGEUP 75
#define CN_KEY_PAUSE 76
#define CN_KEY_PERIOD 77
#define CN_KEY_PRINTSCREEN 78
#define CN_KEY_Q 79
#define CN_KEY_R 80
#define CN_KEY_RALT 81
#define CN_KEY_RCTRL 82
#define CN_KEY_ENTER 83
#define CN_KEY_RSUPER 84
#define CN_KEY_RIGHT 85
#define CN_KEY_RBRACKET 86
#define CN_KEY_RSHIFT 87
#define CN_KEY_S 88
#define CN_KEY_SCROLLLOCK 89
#define CN_KEY_SEMICOLON 90
#define CN_KEY_SLASH 91
#define CN_KEY_SPACE 92
#define CN_KEY_T 93
#define CN_KEY_TAB 94
#define CN_KEY_U 95
#define CN_KEY_UP 96
#define CN_KEY_V 97
#define CN_KEY_W 98
#define CN_KEY_X 99
#define CN_KEY_Y 100
#define CN_KEY_Z 101

/*ok?*/
#define fgets CN_fgets
char *CN_fgets(char *buffer, int size, FILE *file);

extern PSXBackend *PSX_backend;
#endif
#endif /*POOP_BINDS_H_*/
