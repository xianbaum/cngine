extern Allegro4_Backend *allegro4_backend;
static const int MSEC_DIV_60 = 17;
static const int FRAME_TIME = 60;

static CN_inline CN_bool CN_IsKeyUp(int key_code)
{
    return (CN_bool)key[(int)key_code] &&
        allegro4_backend->keydown[key_code]++ == 1;
}

/*TODO: test this*/
static CN_inline char CN_CharKeyDown()
{
    int x = readkey();
    return (x && 0xff);
}

static CN_inline unsigned int CN_GetDeltaTime(void)
{
    int temp = allegro4_backend->CN_msecs;
    if(allegro4_backend->CN_msecs > 0)
    {
        allegro4_backend->CN_msecs = 0;
    }
    return temp;
}

static CN_inline int CN_GetFramesSinceLastCall(void)
{
    int temp = allegro4_backend->CN_ticks;
    if(allegro4_backend->CN_ticks >= 1)
    {
        allegro4_backend->CN_ticks = 0;
    }
    return temp;
}

static CN_inline CN_bool CN_IsKeyPressed(int key_code)
{
    return (CN_bool)key[(int)key_code];
}

static CN_inline CN_bool CN_IsKeyDown(int key_code)
{
    return (CN_bool)key[(int)key_code];
}
static CN_inline void CN_IncrementTimer()
{
    allegro4_backend->CN_msecs+=MSEC_DIV_60;
} END_OF_FUNCTION(CN_IncrementTimer)

static CN_inline void CN_IncrementTicker()
{
    allegro4_backend->CN_ticks++;
} END_OF_FUNCTION(CN_IncrementTicker)

static CN_inline unsigned int CN_GetMilliseconds(void)
{
	return allegro4_backend->CN_msecs;
}

static CN_inline CN_RGB CN_Image_GetRGB(
    const CN_RGB* pal, CN_Image *image, int x, int y)
{
    int pixel= _getpixel(image, x, y);
    return pal[pixel];
}

static CN_inline unsigned char
CN_Image_GetPixel(CN_Image *image, int x, int y)
{
    return _getpixel(image, x, y);
}

static CN_inline void CN_Image_SetRGB
(const CN_Image *image, int x, int y, unsigned char color)
{
    _putpixel(image, x, y, color);
}

static CN_inline void
CN_Image_PutPixel(CN_Image *image, int x, int y, unsigned char pixel)
{
    /*image->line[y][x] = pixel;*/
    _putpixel(image, x, y, pixel);
}

static CN_inline short
CN_Image_GetWidth(const CN_Image *bitmap)
{
    return bitmap->w;
}

static CN_inline short
CN_Image_GetHeight(const CN_Image *bitmap)
{
    return bitmap->h;
}

static CN_inline CN_Image *CN_GetScreen()
{
    return allegro4_backend->buffer;
}

static CN_inline void CN_Image_DrawHLineNoGuard(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    short srcx,
    short srcy,
    short w)
{
    memcpy(dest->line[y]+x,
           src->line[srcy]+srcx,
           sizeof(unsigned char) * w);
}

static CN_inline void CN_Image_DrawVLineNoGuard(
    CN_Image *src,
    CN_Image *dest,
    short x,
    short y,
    short srcx,
    short srcy,
    short h)
{
    int tmpy;
    for(tmpy = 0; tmpy < h; tmpy++) {
        char pixel = src->line[srcy + tmpy][srcx];
        dest->line[y + tmpy][x] = pixel;
    }
}

/*Time*/

static CN_inline void CN_Sleep(unsigned int time)
{
    rest(time);
}
