static CN_inline unsigned char
CN_Image_GetPixel(CN_Image *image, int x, int y)
{
    return image->pixels[(y*image->w)+x];
}

static CN_inline void CN_Image_PutPixel(
    CN_Image *image, int x, int y, unsigned char index)
{
    image->pixels[(y*image->w)+x] = index;
}


static CN_inline void CN_Image_DrawHLineNoGuard(
    CN_Image *src, CN_Image *dest, short x, short y, short srcx, short srcy,
    short w)
{
    memcpy(dest->pixels + (y*dest->w) + x,
           src->pixels + (srcy*src->w) + srcx,
           sizeof(unsigned char) * w);
}

static CN_inline void CN_Image_DrawVLineNoGuard(
    CN_Image *src, CN_Image *dest, short x, short y, short srcx, short srcy,
    short h)
{
    int tmpy;
    for(tmpy = 0; tmpy < h; tmpy++) {
        char pixel = src->pixels[(srcy + tmpy) * src->w + srcx ];
        dest->pixels[(y + tmpy) * dest->w + x] = pixel;
    }
}

static CN_inline CN_Image *CN_GetScreen()
{
    return container.screen;
}

static CN_inline short CN_Image_GetWidth(const CN_Image *image)
{
    return image->w;
}

static CN_inline short CN_Image_GetHeight(const CN_Image *image)
{
    return image->h;
}
