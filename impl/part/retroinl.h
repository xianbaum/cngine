static CN_inline CN_Image *CN_GetScreen()
{
    return cn_backend->screen;
}

static CN_inline void
CN_Image_PutPixel(CN_Image *image, int x, int y, unsigned char index)
{
    image->pixels[(y * image->h) + x] = index;
}

static CN_inline short
CN_Image_GetWidth(const CN_Image *img)
{
    return img->w;
}

static CN_inline short
CN_Image_GetHeight(const CN_Image *img)
{
    return img->h;
}

/* stub*/
static CN_inline int CN_GetDeltaTime(void)
{
    return 1;
}

static CN_inline unsigned char
CN_Image_GetPixel(CN_Image *image, int x, int y)
{
    return image->pixels[image->w*y+x];
}

static CN_inline CN_bool CN_IsKeyDown(CN_Key x)
{
    return CN_false;
}

static CN_inline CN_bool CN_IsKeyPressed(CN_Key x)
{
    return CN_false;
}

static CN_inline void CN_Sleep(unsigned int x)
{
    
}
