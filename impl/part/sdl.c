/*With SDL1 and SDL2 there are quite a few common functions.*/

extern SDLBackend *sdl_backend;

int seconds_passed;
char charKeyDown = '\0';
long cnTimer = 0;



#define nearest_impl_old(src, dest, srcRect, destRect, offset, mask, has_mask) \
    int x_ratio, y_ratio, px, py, x, y,                                 \
        endX = destRect.x+destRect.w, endY = destRect.y + destRect.h;   \
    Uint8 pixel;                                                        \
    if(destRect.w == 0 || destRect.h == 0) return;                      \
    x_ratio = (int)((srcRect.w<<16)/dest->w)+1 ;                       \
    y_ratio = (int)((srcRect.h<<16)/dest->h)+1 ;                       \
    for (y = destRect.y; y < endY; y++) {                              \
    for (x = destRect.x; x < endX; x++) {                              \
        px = (int)(x * x_ratio >> 16);                                  \
        py = (int)(y * y_ratio >> 16);                                  \
        pixel = ((Uint8*)src->pixels)[(int)(                            \
                (src->pitch*(py+srcRect.y))+                           \
                srcRect.x+                                             \
                px)];                                                   \
        if(has_mask && pixel == mask) continue;                         \
        ((Uint8*)dest->pixels)[(y*dest->pitch)+x] = pixel + offset;     \
    }                                                                   \
    }


#define shifted_impl_old(src, dest, srcx, srcy, srcw, srch, destx, desty, offest, mask, has_mask) \
    int tmpy, tmpx;                                                     \
    for(tmpy = 0; tmpy < srch; tmpy++) {                                \
        for(tmpx = 0; tmpx < srcw; tmpx++) {                            \
            Uint8 pixel = ((Uint8*)src->pixels)[                        \
                (srcy+tmpy)*src->pitch +                                \
                srcx + tmpx                                             \
                ];                                                      \
            if(has_mask && pixel == mask) continue;                     \
            ((Uint8*)dest->pixels)[                                     \
                (desty+ tmpy) *dest->pitch +                            \
                destx + tmpx                                            \
                ] = pixel + offset;                                     \
        }                                                               \
    }


size_t CN_Image_EstimateSizeOf(const CN_Image *image)
{
    size_t size = 0;
    if(image == NULL) return 0;
    size += image->pitch * image->h;
    size += sizeof(image);
    return size;
}

void CN_Backend_FinalizeFrame()
{
}

CN_Image *CN_Image_Create(int width, int height)
{
    return SDL_CreateRGBSurface(0, width, height, 8, 0, 0, 0, 0);
}

CN_Image *CN_Image_Clone(CN_Image *input)
{
    SDL_Surface *c = SDL_CreateRGBSurface(0, input->w, input->h, 8, 0, 0, 0,0);
    SDL_BlitSurface(input, NULL, c, NULL);
    return c;
}

void CN_Image_ShiftColors(CN_Image *image, int number)
{
    int i = 0, pixels = image->h * image->pitch;
    for(; i < pixels; i++) ((Uint8*)image->pixels)[i]+=number;
}

CN_RGB CN_RGB_Create(unsigned char r, unsigned char g, unsigned char b)
{
    CN_RGB pixel;
    pixel.r = (Uint8)r;
    pixel.g = (Uint8)g;
    pixel.b = (Uint8)b;
    return pixel;
}

void CN_RGB_Free(CN_RGB *palette)
{
    CN_free(palette);
}

CN_bool CN_RGB_Compare(const CN_RGB *pixel1, const CN_RGB *pixel2)
{
    return pixel1->r == pixel2->r &&
        pixel1->g == pixel2->g &&
        pixel1->b == pixel2->b;
}

void CN_Image_Free(CN_Image *image)
{
    CN_unregister_pointer(image);
    SDL_FreeSurface(image);
}

void CN_SoundByte_Free(CN_SoundByte *sound)
{
    if(sound == NULL) return;
#ifndef CN_NO_MIXER
    CN_unregister_pointer(sound);
    Mix_FreeChunk(sound);
#endif
}

static void CN_free_mod_midi(CN_Music *music)
{
    if(music == NULL) return;
#ifndef CN_NO_MIXER
    CN_unregister_pointer(music);
    Mix_FreeMusic(music);
#endif
}


static void CN_play_mod_midi(CN_Music *music)
{
    if(music == NULL) return;
#ifndef CN_NO_MIXER
    CN_Try(Mix_PlayMusic(music, 0) == 1,
       Mix_GetError());
#endif
#ifdef CN_MIKMOD_HACK
    {MODULE *mod;
        mod = Player_GetModule();
        if (mod != NULL) mod->loop = 1;
    }
#endif
}

static void CN_stop_mod_midi(void)
{
#ifndef CN_NO_MIXER
    Mix_HaltMusic();
#endif
}

static CN_Music *CN_load_mod_midi(const char *path)
{
#ifndef CN_NO_MIXER
    CN_Music *music;
    CN_Assert(path != NULL);
    music = Mix_LoadMUS(path);
    CN_Try(music != NULL, Mix_GetError());
    CN_register_pointer(music);
#else
    CN_Music *music = NULL;
#endif
    return music;
}

CN_MusicType CN_GetMusicType(void)
{
    CN_Assert(sdl_backend != NULL);
    return sdl_backend->audio_backend;
}

CN_SoundByte *CN_SoundByte_Load(const char *path)
{
#ifndef CN_NO_MIXER
    CN_SoundByte *WAV;
    CN_Assert(path != NULL);
    WAV= Mix_LoadWAV(path);
    CN_Try(WAV != NULL,
       Mix_GetError());
    CN_register_pointer(WAV);
#else
    CN_SoundByte *WAV = NULL;
#endif
    return WAV;
}

CN_bool CN_ExitCondition()
{
    return (CN_bool)SDL_QuitRequested() || CN_exit;
}

#ifdef CN_USE_OPENMPT_INSTEAD
#define BUFFERSIZE 2048
#define SAMPLERATE 48000
static SDL_AudioDeviceID mod_audio_device;

static CN_openmpt_log_function(const char *message, void *user)
{
    CN_LogInformation((message));
}

static void CN_openmpt_module_callback(void*  userdata, Uint8* stream, int len)
{
    int playing = openmpt_module_read_interleaved_float_stereo( userdata,
                              SAMPLERATE, len/2, stream);
    if(!playing) memset(stream, 0, len);
}

static CN_Music *CN_load_openmpt_module(const char *path)
{
    SDL_AudioSpec *audio = NULL;
    FILE *file = fopen( path, "rb" );
    char error_buffer[1][100];
    openmpt_module *mod =
        openmpt_module_create2( openmpt_stream_get_file_callbacks(), file,
          CN_openmpt_log_function, NULL, NULL, NULL, NULL, error_buffer, NULL );
    fclose(file);
    if(CN_Try(mod == NULL,"Module was unable to load")) {
        audio = CN_malloc(sizeof(SDL_AudioSpec));
        audio->freq = SAMPLERATE;
        audio->format = AUDIO_F32SYS;
        audio->channels = 2;
        audio->samples = BUFFERSIZE;
        audio->userdata = mod;
        audio->silence = 0;
        audio->callback = CN_openmpt_module_callback;
    }
    return (CN_Music*)audio;
}

static void CN_free_openmpt_module(CN_Music *music)
{
    SDL_AudioSpec *audio = (SDL_AudioSpec*)music;
    if(music == NULL) return;
    openmpt_module_destroy(audio->userdata);
    CN_free(audio);
}

static void CN_play_openmpt_module(CN_Music *music)
{
    SDL_AudioSpec *audio = (SDL_AudioSpec*)music;
    if(music == NULL) return;
    mod_audio_device = SDL_OpenAudioDevice(SDL_GetAudioDeviceName(0,0),
                                           0,music, NULL, 0);
    CN_Try(mod_audio_device > 0, SDL_GetError());
    SDL_PauseAudioDevice(mod_audio_device, 0);
}

static void CN_stop_openmpt_module()
{
    SDL_PauseAudioDevice(mod_audio_device, 1);
}
#endif

static CN_Music *CN_load_midi(const char *path)
{
    unsigned int length;
    char *copy;
    CN_Music *midi;
    CN_Assert(path != NULL);
    length = strlen(path);
    copy = CN_malloc(sizeof(char) * length);
    strcpy(copy, path);
    copy[length-2] = 'i';
    midi = CN_load_mod_midi(copy);
    CN_free(copy);
    return midi;
}

CN_Music *CN_Music_Load(const char *path)
{
    CN_Assert(path != NULL);
    CN_Assert(sdl_backend  != NULL);
    switch(sdl_backend->audio_backend) {
    case CN_AUDIO_MIDI:
        return CN_load_midi(path);
    case CN_AUDIO_MOD:
#ifdef CN_USE_OPENMPT_INSTEAD
        return CN_load_openmpt_module(path);
#else
        return CN_load_mod_midi(path);
#endif
    default:
        return NULL;
    }
}

void CN_Music_Free(CN_Music* music)
{
    CN_Assert(sdl_backend != NULL);
    switch(sdl_backend->audio_backend) {
    case CN_AUDIO_MIDI:
        CN_free_mod_midi(music);
        break;
    case CN_AUDIO_MOD:
#ifdef CN_USE_OPENMPT_INSTEAD
        CN_free_openmpt_module(music);
#else
        CN_free_mod_midi(music);
#endif
        break;
    default:
        break;
    }
}

void CN_Music_Play(CN_Music* music)
{
    CN_Assert(sdl_backend != NULL);
    switch(sdl_backend->audio_backend) {
#ifdef CN_USE_OPENMPT_INSTEAD
    case CN_AUDIO_MIDI:
        CN_play_mod_midi(music);
        break;
    case CN_AUDIO_MOD:
        CN_play_openmpt_module(music);
        break;
#else
    case CN_AUDIO_MOD:
    case CN_AUDIO_MIDI:
        CN_play_mod_midi(music);
        break;
#endif
    default:
        break;
    }
}

void CN_Music_Stop(void)
{
    CN_Assert(sdl_backend != NULL);
    switch(sdl_backend->audio_backend) {
#ifdef CN_USE_OPENMPT_INSTEAD
    case CN_AUDIO_MIDI:
        CN_stop_mod_midi();
        break;
    case CN_AUDIO_MOD:
        CN_stop_openmpt_module();
        break;
#else
    case CN_AUDIO_MOD:
    case CN_AUDIO_MIDI:
        CN_stop_mod_midi();
        break;
#endif
    default:
        break;
    }
}
