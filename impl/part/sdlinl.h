#ifndef SDL_INLINE_H_
#define SDL_INLINE_H_

extern SDLBackend *sdl_backend;
extern char charKeyDown;

static CN_inline void CN_Sleep(unsigned int time)
{
#ifndef CN_EMCC
    SDL_Delay(time);
#endif
}

static CN_inline CN_Image *CN_GetScreen()
{
    return sdl_backend->SDL_screen;
}

static CN_inline short CN_Image_GetWidth(const CN_Image *image)
{
    return image->w;
}

static CN_inline short CN_Image_GetHeight(const CN_Image *image)
{
    return image->h;
}

static CN_inline CN_RGB CN_Image_GetRGB(const CN_RGB *pal,
                                        CN_Image *image,
                                        int x, int y)
{
    return pal[CN_Image_GetPixel(image, x, y)];
}

static CN_inline void CN_Image_PutPixel(
    CN_Image *image, int x, int y, unsigned char index)
{
    ((Uint8*)image->pixels)[y*image->pitch+x] = (Uint8)index;
}

static CN_inline unsigned char
CN_Image_GetPixel(CN_Image *image, int x, int y)
{
    return ((Uint8*)image->pixels)[(y * image->pitch) + x];
}

static CN_inline void CN_Image_DrawHLineNoGuard(
    CN_Image *src, CN_Image *dest, short x, short y, short srcx, short srcy,
    short w)
{
    memcpy(((Uint8*)dest->pixels) + (y*dest->pitch) + x,
           ((Uint8*)src->pixels) + ((srcy*src->pitch) + srcx),
           w);
}

static CN_inline void CN_Image_DrawVLineNoGuard(
    CN_Image *src, CN_Image *dest, short x, short y, short srcx, short srcy,
    short h)
{
    int tmpy;
    for(tmpy = 0; tmpy < h; tmpy++) {
        Uint8 pixel = ((Uint8*)src->pixels)[
            (srcy + tmpy) * src->pitch +
            srcx
            ];
        ((Uint8*)dest->pixels)[
            (y + tmpy) * dest->pitch +
            x] = pixel;
    }
}

static CN_inline void _pollSDLEvents()
{
    memset(sdl_backend->keyup_state, 0, NUMBER_OF_SDL_SCANCODES);
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type) {
        case SDL_KEYDOWN:
            sdl_backend->keypress_state[event.key.keysym.scancode]++;
            sdl_backend->keyup_state[event.key.keysym.scancode] = 0;
#ifdef CN_SDL2
			break;
	    case SDL_TEXTINPUT:
            charKeyDown = event.text.text[0];
#elif defined CN_SDL1_2
            charKeyDown = event.key.keysym.sym;
#endif
            break;
        case SDL_KEYUP:
            sdl_backend->keypress_state[event.key.keysym.scancode] = 0;
            sdl_backend->keyup_state[event.key.keysym.scancode] = 1;
            break;
/*        case SDL_MOUSEBUTTONDOWN:
            sdl_backend->mousedown_state[event.button.button]++;
            sdl_backend->mouseup_state[event.button.button] = 0;
            break;
        case SDL_MOUSEBUTTONUP:
            sdl_backend->mousedown_state[event.button.button] = 0;
            sdl_backend->mouseup_state[event.button.button] = 1;
            break;*/
        }
    }
}

static CN_inline unsigned int CN_GetMilliseconds(void)
{
    return SDL_GetTicks();
}

static unsigned int lastSeconds;
static CN_inline unsigned int CN_GetDeltaTime(void)
{
    int deltaTime = SDL_GetTicks() - lastSeconds;
    lastSeconds += deltaTime;
    return deltaTime;
}

static unsigned int frames_passed;
static CN_inline int CN_GetFramesSinceLastCall(void)
{
    int frame = SDL_GetTicks()/16 - frames_passed;
    frames_passed += frame;
    return frame;
}

extern long cnTimer;
static CN_inline void CN_IncrementTimer(void)
{
    cnTimer++;
}

static CN_inline long CN_GetTimer(void)
{
    return cnTimer;
}

static CN_inline CN_bool CN_IsKeyDown(CN_Key scan_code)
{
    return (CN_bool)sdl_backend->keyboard_state[scan_code];
}

static CN_inline CN_bool CN_IsKeyPressed(CN_Key scan_code)
{
    return sdl_backend->keypress_state[scan_code] > 0 &&
        sdl_backend->keypress_state[scan_code]++ == 1;
}

static CN_inline CN_bool CN_IsKeyUp(CN_Key scan_code)
{
    return (CN_bool)sdl_backend->keyup_state[scan_code] > 0 &&
        sdl_backend->keyup_state[scan_code]++ == 1;
}

static CN_inline char CN_CharKeyDown(void)
{
    char temp = charKeyDown;
    charKeyDown = '\0';
    return temp;
}

static CN_inline void CN___CheckUpdateMouse(CN_bool override)
{
    if(override ||
        sdl_backend->tickCounter != sdl_backend->mouseCurrentTick)
    {
        sdl_backend->mouseCurrentTick = sdl_backend->tickCounter;
        sdl_backend->lastMouseState = SDL_GetMouseState(
            &(sdl_backend->lastMouseX),
            &(sdl_backend->lastMouseY));
#ifdef CN_SDL2
        {
            const double aspect_ratio =
                ((double)CN_options->width)/((double)CN_options->height);
            int window_w = CN_options->width,
                window_h = CN_options->height;
            CN_Rect screen;
            
            if(sdl_backend->SDL2_window != NULL) {
                SDL_GetWindowSize(sdl_backend->SDL2_window, &window_w, &window_h);
            }
            
            if(((double)window_w/(double)window_h) >= (double)aspect_ratio) {
                screen.y = 0;
                screen.h = window_h;
                screen.w = (int)(window_h * aspect_ratio);
                screen.x = (window_w - screen.w)/2;
            } else {
                screen.x = 0;
                screen.w = window_w;
                screen.h = (int)(window_w/aspect_ratio);
                screen.y = (window_h - screen.h)/2;
            }
            
            if(sdl_backend->lastMouseX < screen.x) {
                sdl_backend->lastMouseX = 0; 
            }else if(sdl_backend->lastMouseX > (screen.x + screen.w)) {
                sdl_backend->lastMouseX = screen.w-4;
            } else {
                sdl_backend->lastMouseX -= screen.x;
                sdl_backend->lastMouseX = (int)
                (((double)CN_options->width) / ((double)screen.w) *
                 ((double)sdl_backend->lastMouseX));
            }
            
            if(sdl_backend->lastMouseY < screen.y) {
                sdl_backend->lastMouseY = 0;
            } else if(sdl_backend->lastMouseY > (screen.y + screen.h)) {
                sdl_backend->lastMouseY = screen.h;
            } else {
                sdl_backend->lastMouseY -= screen.y;
                sdl_backend->lastMouseY = (int)
                    (((double)CN_options->height) / ((double)screen.h) *
                     ((double)sdl_backend->lastMouseY));
            }
        }
#endif
    }
}

static CN_inline void CN_GetMouseCoords(int *x, int *y)
{
    CN___CheckUpdateMouse(CN_false);
    *x = sdl_backend->lastMouseX;
    *y = sdl_backend->lastMouseY;
}

static CN_inline CN_bool CN_IsMouseButtonDown(CN_MouseButton button)
{
    CN___CheckUpdateMouse(CN_false);
    return (sdl_backend->lastMouseState & SDL_BUTTON(button)) == button;
}

static CN_inline CN_bool CN_IsMouseButtonUp(CN_MouseButton button)
{
    if(sdl_backend->lastMouseState & button != SDL_BUTTON(button)) return CN_false;
    CN___CheckUpdateMouse(CN_true);
    return (sdl_backend->lastMouseState & button) != button;
}

#endif
