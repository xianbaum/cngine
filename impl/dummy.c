#if !defined(CN_SDL2) && !defined(CN_SDL1_2) && !defined(CN_ALLEGRO4) &&!defined(CN_LIBRETRO) && !defined(CN_PSX)

#include "../cngine.h"

#include "../img/draw.c"

CN_bool CN_exit = CN_false;
static CN_bool __CN_IsKeyDown_toggle = CN_false;
CN_DummyContainer container;

CN_bool CN_Init(CN_Options *options)
{
    container.screen = CN_Image_Create(
        options->width, options->height);
    if(!CN_Logger_Init(options->loggingLevel)) {
        fprintf(stderr,"Warning: Failed to initialize logger");
    }
    return CN_true;
}

CN_bool CN_Deinit(void)
{
    CN_Image_Free(container.screen);
    CN_Logger_Destroy();
    return CN_true;
}

CN_bool CN_CanReinit(void)
{
    return CN_true;
}


CN_bool CN_ExitCondition()
{
    return CN_exit;
}

CN_Image *CN_Image_Load(const char *path)
{
    CN_LogError(("CN_Image_Load not implemented"));
    return NULL;
}

CN_Image *CN_Image_Create(int width, int height)
{
    CN_Image *image = CN_calloc(
        sizeof(CN_Image) + (sizeof(char) * (width * height)) + 1,
        sizeof(char));
    image->w = width;
    image->h = height;
    return image;
}

size_t CN_Image_EstimateSizeOf(const CN_Image *image)
{
    if(image == NULL) return 0;
    return sizeof(CN_Image) + (sizeof(char) * (image->w * image->h)) + 1;
}

void CN_Image_Free(CN_Image *image)
{
    CN_free(image);
}

/* TODO this */
CN_RGB *CN_RGB_ExtractFromImage(const char *path)
{
    CN_Assert(path != NULL);
    CN_LogError(("CN_RGB_ExtractFromImage not implemented"));
    return malloc(sizeof(BigWhatever));
}

/* TODO this */
void CN_RGB_Free(CN_RGB *palette)
{
    CN_LogError(("CN_RGB_Free not implemented"));
    free(palette);
}

CN_RGB CN_RGB_Create(unsigned char r, unsigned char g, unsigned char b)
{
    CN_RGB rgb;
    rgb.r = r;
    rgb.g = g;
    rgb.b = b;
    return rgb;
}

CN_bool CN_RGB_Compare(const CN_RGB *p1, const CN_RGB *p2)
{
    CN_Assert(p1 != NULL && p2 != NULL);
    return p1->r == p2->r &&
        p1->g == p2->g &&
        p1->b == p2->b;
}

void CN_RGB_SetRange(CN_RGB *colors, int start, int end)
{
    CN_LogError(("CN_RGB_SetRange not implemented"));
    return;
}

int CN_GetDeltaTime(void)
{
    CN_LogError(("CN_GetDeltaTime not implemented"));
    return 12;
}

int CN_GetFramesSinceLastCall(void)
{
    CN_LogError(("CN_GetFramesSinceLastCall not implemented"));
}

CN_SoundByte *CN_SoundByte_Load(const char *path)
{
    CN_LogError(("CN_SoundByte_Load not implemented"));
    return NULL;
}

void CN_SoundByte_Free(CN_SoundByte *soundbyte)
{
    CN_LogError(("CN_SoundByte_Free not implemented"));
    CN_free(soundbyte);
}

CN_Music *CN_Music_Load(const char *path)
{
    CN_LogError(("CN_Music_Load not implemented"));
    return NULL;
}

void CN_Music_Free(CN_Music *music)
{
    CN_LogError(("CN_Music_Free not implemented"));
    free(music);
}

void CN_Music_Play(CN_Music* mu)
{
    CN_LogError(("CN_Music_Play not implemented"));
    return;
}
void CN_Music_Stop(void)
{
    CN_LogError(("CN_Music_Stop not implemented"));
    return;
}

CN_MusicType CN_GetMusicType()
{
    CN_LogError(("CN_GetMusicType not implemented"));
    return 2;
}

CN_bool CN_IsKeyDown(int key)
{
    CN_LogError(("CN_IsKeyDown not implemented"));
    __CN_IsKeyDown_toggle = !__CN_IsKeyDown_toggle;
    if(key == CN_KEY_ESC) return CN_false;
    return __CN_IsKeyDown_toggle;
}
#else
typedef int cry_null;
#endif /*USE_NULL_*/
