#ifdef CN_LIBRETRO
#include "cngine.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>

#ifndef RETRO_API_VERSION
#define RETRO_API_VERSION
#endif

struct CN_RetroArchBackend *cn_backend;

unsigned int retro_api_version(void)
{
    return RETRO_API_VERSION;
}

void retro_init(void)
{
    CN_Backend_Init(NULL);
}

void retro_deinit(void)
{
    CN_Backend_Deinit();
}

void retro_set_controller_port_device(unsigned port, unsigned device)
{

}

void retro_get_system_info(struct retro_system_info *info)
{
   memset(info, 0, sizeof(*info));
   info->library_name     = "CNGine Core";
   info->library_version  = "v1";
   info->need_fullpath    = 1;
   info->valid_extensions = NULL;
}


static retro_video_refresh_t video_cb;
static retro_audio_sample_t audio_cb;
static retro_audio_sample_batch_t audio_batch_cb;
static retro_environment_t environ_cb;
static retro_input_poll_t input_poll_cb;
static retro_input_state_t input_state_cb;



void retro_get_system_av_info(struct retro_system_av_info *info)
{
   float aspect = 4.0f / 3.0f;
   info->timing = (struct retro_system_timing) {
      .fps = 60.0,
      .sample_rate = 0.0,
   };

   info->geometry = (struct retro_game_geometry) {
      .base_width   = 320,
      .base_height  = 240,
      .max_width    = 320,
      .max_height   = 240,
      .aspect_ratio = aspect,
   };
}

void retro_set_environment(retro_environment_t cb)
{
   environ_cb = cb;
   CN_bool no_content = CN_true;
   cb(RETRO_ENVIRONMENT_SET_SUPPORT_NO_GAME, &no_content);
}

void retro_set_audio_sample(retro_audio_sample_t cb)
{
   audio_cb = cb;
}

void retro_set_audio_sample_batch(retro_audio_sample_batch_t cb)
{
   audio_batch_cb = cb;
}

void retro_set_input_poll(retro_input_poll_t cb)
{
   input_poll_cb = cb;
}

void retro_set_input_state(retro_input_state_t cb)
{
   input_state_cb = cb;
}

void retro_set_video_refresh(retro_video_refresh_t cb)
{
   video_cb = cb;
}

static unsigned x_coord;
static unsigned y_coord;
static int mouse_rel_x;
static int mouse_rel_y;

void retro_reset(void)
{
   x_coord = 0;
   y_coord = 0;
}

static void update_input(void)
{
   input_poll_cb();
   if (input_state_cb(0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_UP))
   {
      /* stub */
   }
}

void CN_Flip(void)
{

}

static void check_variables(void)
{
}

static void audio_callback(void)
{
}

void retro_run(void)
{
    CN_Frame();
}

bool retro_load_game(const struct retro_game_info *info)
{
    CN_Init();
}

void retro_unload_game(void)
{
    CN_Deinit();
}

unsigned retro_get_region(void)
{
   return RETRO_REGION_NTSC;
}

size_t retro_serialize_size(void)
{
   return 0;
}

bool retro_serialize(void *data_, size_t size)
{
    return false;
}

bool retro_unserialize(const void *data_, size_t size)
{
   return false;
}

void *retro_get_memory_data(unsigned id)
{
   (void)id;
   return NULL;
}

size_t retro_get_memory_size(unsigned id)
{
   (void)id;
   return 0;
}

void retro_cheat_reset(void)
{
    
}

void retro_cheat_set(unsigned index, bool enabled, const char *code)
{
   (void)index;
   (void)enabled;
   (void)code;
}

CN_bool CN_Backend_Init(CN_Options *options)
{
    cn_backend = malloc(sizeof(struct CN_RetroArchBackend));
    cn_backend->screenW = 320;
    cn_backend->screenH = 240;
    return CN_true;
}

CN_bool CN_Backend_Deinit(void)
{
    free(cn_backend);
}

CN_bool CN_CanReinit(void)
{
    return CN_false;
}

CN_RGB CN_RGB_Create(unsigned char r, unsigned char g, unsigned char b)
{
    return r << 8 + g << 16 + b << 24;
}

CN_bool CN_RGB_Compare(const CN_RGB *p1, const CN_RGB *p2)
{
    return *p1 == *p2;
}

CN_Image *CN_Image_Create(int width, int height)
{
    CN_Image *image = CN_calloc(
        sizeof(CN_Image) + (sizeof(char) * (width * height)) + 1,
        sizeof(char));
    image->w = width;
    image->h = height;
    return image;
}

size_t CN_Image_EstimateSizeOf(const CN_Image *image)
{
    if(image == NULL) return 0;
    return sizeof(CN_Image) + (sizeof(char) * (image->w * image->h)) + 1;
}

void CN_Image_Free(CN_Image *image)
{
    CN_free(image);
}

/* Stubs */

CN_Image *CN_Image_Load(const char *path)
{
    return NULL;
}

CN_RGB *CN_RGB_ExtractFromImage(const char *path)
{
    CN_Assert(path != NULL);
    CN_LogError(("CN_RGB_ExtractFromImage not implemented"));
    return malloc(256);
}

CN_bool CN_ExitCondition(void)
{
    return CN_false;
}


void CN_Backend_FinalizeFrame()
{
}

void CN_Backend_Update()
{
    
}

void CN_RGB_SetRange(CN_RGB *colors, int start, int end)
{
    for(; start < end; start++) {
        cn_backend->pal[start] = colors[start];
    }
}

CN_bool CN_exit;

#include "shared/draw.c"


#endif /* CN_LIBRETRO */
