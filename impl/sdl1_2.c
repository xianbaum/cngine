#ifdef CN_SDL1_2
#include "../cngine.h"

#ifndef CN_NO_MIXER
#ifdef CN_MIKMOD_HACK
#include <mikmod.h>
#elif defined CN_MODPLUG_HACK
#include <libmodplug/modplug.h>
#endif
#endif

#include "part/sdl.c"

#ifdef DREAMCAST
extern uint8 romdisk[];
KOS_INIT_FLAGS(INIT_DEFAULT);
KOS_INIT_ROMDISK(romdisk);
#endif

SDLBackend *sdl_backend = NULL;

CN_bool CN_exit;

static void CN__load_sdl1_properties_file(void)
{
    FILE *properties_file = fopen("sdl1.csv", "r");
    char buffer[1024];
    char *substr;
    if(properties_file == NULL) {
        sdl_backend->audio_backend = 1;
        return;
    }
    fgets(buffer,sizeof(buffer), properties_file);
    substr = strtok(buffer, ",");
    sdl_backend->audio_backend = (int)strtod(substr, NULL);
    fclose(properties_file);
    if(sdl_backend->audio_backend < 0 ||
       sdl_backend->audio_backend > 2) sdl_backend->audio_backend = 2;
}

CN_bool CN_Init(CN_Options *options)
{
#ifdef DREAMCAST
    CN_set_directory("/rd/");
#endif
    if(options == NULL) {
        CN_options = options = CN_Options_CreateBlank();
    } else {
        CN_options = malloc(sizeof(CN_Options));
        memcpy(CN_options, options, sizeof(CN_Options));
    }
    if(!CN_Logger_Init(options->loggingLevel)) {
        fprintf(stderr,"Warning: Failed to initialize logger");
    }
    CN_Assert(sdl_backend == NULL);
    sdl_backend = CN_malloc(sizeof(*sdl_backend));
    CN_Assert(sdl_backend != NULL);
    sdl_backend->tickCounter = 0;
    sdl_backend->mouseCurrentTick = 0;
    sdl_backend->lastMouseState = 0;
    sdl_backend->lastMouseX = 0;
    sdl_backend->lastMouseY = 0;
    CN__load_sdl1_properties_file();
    CN_Expect(SDL_Init(SDL_INIT_VIDEO) == 0, SDL_GetError());
    seconds_passed = 0;
    if(options->mode != CN_MODE_TEXT) {
        int modeFlags = (SDL_HWSURFACE | SDL_HWPALETTE);
        switch(options->mode) {
        case CN_MODE_FULLSCREEN:
            modeFlags = modeFlags | SDL_FULLSCREEN;
            break;
        case CN_MODE_WINDOWED_BORDERLESS:
            modeFlags = modeFlags | SDL_NOFRAME;
            break;
        case CN_MODE_SDL_FLAGS:
            modeFlags = options->modeFlags;
            break;
        case CN_MODE_WINDOWED:
        default:
            modeFlags = modeFlags | SDL_RESIZABLE;
            break;
        }
        sdl_backend->SDL_screen =
            SDL_SetVideoMode(options->width, options->height, 8, modeFlags);
    }
#ifndef CN_EMCC
    sdl_backend->keyboard_state = SDL_GetKeyState(NULL);
#else
    sdl_backend->keyboard_state = SDL_GetKeyboardState(NULL);
#endif
    /*Mixer*/
#ifndef CN_NO_MIXER
    Mix_Init(MIX_INIT_MOD);
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096);
#ifdef CN_MODPLUG_HACK
    {
    ModPlug_Settings settings;
    ModPlug_GetSettings(&settings);
    settings.mLoopCount = -1;
    ModPlug_SetSettings(&settings);
    }
#endif
#endif
    return CN_true;
}

/*Image*/

CN_Image *CN_Image_Load( const char * path)
{
    CN_Image* image_surface = NULL;
    CN_RGB mask;
    char *buffer = CN_create_temp_buffer(path);
#ifdef CN_NO_IMAGE
    int strlen_image = strlen(path);
    char* temp = CN_malloc(sizeof(char) * strlen_image);
    temp = strcpy(temp, path);
    temp[strlen_image-3] = 'b';
    temp[strlen_image-2] = 'm';
    temp[strlen_image-1] = 'p';
    image_surface = SDL_LoadBMP(temp);
    CN_free(temp);
#else
    image_surface = IMG_Load(buffer);
#endif
    CN_free_temp_buffer(buffer);
    if(!CN_Try(image_surface != NULL,SDL_GetError())) return image_surface;
    CN_register_pointer(image_surface);
    mask = image_surface->format->palette->colors[0];
    SDL_SetColorKey(image_surface, SDL_SRCCOLORKEY,
            SDL_MapRGB(image_surface->format,
                   mask.r, mask.g, mask.b));
    SDL_ConvertSurface(image_surface, sdl_backend->SDL_screen->format, 0);
    SDL_SetPalette(image_surface, SDL_LOGPAL|SDL_PHYSPAL,
           sdl_backend->SDL_screen->format->palette->colors,
           0, 256);
    return image_surface;
}

CN_bool CN_CanReinit(void)
{
    return CN_false;
}

CN_bool CN_Deinit(void)
{
    if(sdl_backend == NULL) return;
#ifndef CN_NO_MIXER
    while(Mix_Init(0) == 1)
    {
    Mix_Quit();
    }
#endif
    CN_Image_Free( sdl_backend->SDL_screen );
#ifndef CN_NO_IMAGE
    IMG_Quit();
#endif
    CN_free(sdl_backend);
    CN_memory_atexit();
    SDL_Quit();
    sdl_backend = NULL;
    return CN_true;
}

void CN_RGB_SetRange(CN_RGB *palette, int start, int end)
{
    SDL_SetPalette(sdl_backend->SDL_screen,
                   SDL_LOGPAL|SDL_PHYSPAL,
                   palette,
                   start, end+1-start);
}

CN_RGB *CN_RGB_ExtractFromImage(const char *path)
{
    CN_Image *image;
    CN_RGB *copied_colors;
    char *buffer = CN_create_temp_buffer(path);
#ifdef CN_NO_IMAGE
    int strlen_image = strlen(path);
    char* temp = CN_malloc(sizeof(char) * strlen_image);
    temp = strcpy(temp, path);
    temp[strlen_image-3] = 'b';
    temp[strlen_image-2] = 'm';
    temp[strlen_image-1] = 'p';
    image = SDL_LoadBMP(temp);
#else
    image = IMG_Load(buffer);
#endif
    CN_free_temp_buffer(buffer);
    if(!CN_Try(image != NULL,
           SDL_GetError()))
    {
    return NULL;
    }
    copied_colors = CN_malloc(sizeof(CN_RGB)*
                  image->format->palette->ncolors);
    memcpy(copied_colors, image->format->palette->colors,
       sizeof(CN_RGB)*image->format->palette->ncolors);
    CN_Image_Free(image);
    return copied_colors;
}

void CN_Flip(void)
{
    CN_Assert(sdl_backend != NULL);
    SDL_Flip(sdl_backend->SDL_screen);
}

/* These are where the original draw functions were */
#include "shared/draw.c"

void CN_Cls(void)
{
    if(sdl_backend->black_screen == NULL) {
        sdl_backend->black_screen = SDL_CreateRGBSurface(0, CN_options->width, CN_options->height, 8,
                                                         0, 0, 0, 0);
        SDL_SetPalette(sdl_backend->black_screen,
                       SDL_LOGPAL|SDL_PHYSPAL,
                       sdl_backend->SDL_screen->format->palette->colors,
                       0, 256);
    }
    CN_Image_Draw(sdl_backend->black_screen, sdl_backend->SDL_screen, 0, 0);
}

void CN_Backend_Update()
{
    sdl2_backend->tickCounter++;
    _pollSDLEvents();
}

#ifndef CN_CUSTOM_MAIN

int main(int argc, char **argv)
{
    CN_Init(argc, argv);
    while(!CN_ExitCondition())
    {
        CN_Frame(argc, argv);
    }
    CN_Deinit(argc, argv);
}

#endif

#else
typedef int cry_sdl1;
#endif /*CN_SDL1_2*/
