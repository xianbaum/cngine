/**
 * @file allegro4.h
 * @author Christian Michael Baum
 * @brief Implementation used if compiling with Allegro4
 * Refer to cngine.h for API
 **/

#ifndef ALLEGRO4_BINDS_
#define ALLEGRO4_BINDS_

#ifdef CN_ALLEGRO4

#include <allegro.h>
#include <stdio.h>

#ifdef CN_DUMB
#include <aldumb.h>
#elif defined CN_JGMOD
#include <jgmod.h>
#endif /* CN_DUMB*/

#ifdef CN_ALGIF
#include <algif.h>
#endif /* CN_ALGIF */

#include "../core/chelpers.h"
#include "../core/cnerror.h"
#include "../core/cnmem.h"

#ifdef CN_DUMB
typedef struct DumbProperties {
    int channels;
    int frequency;
    int resample_quality;
    float volume;
    long buffersize;
} DumbProperties;
#endif /* CN_DUMB */

typedef struct Allegro4_Backend {
    CN_bool music_playing;
    int audio_backend;
    PALETTE palette;
    BITMAP *buffer;
    int gfx_mode;
#ifdef CN_DUMB
    AL_DUH_PLAYER *duh_player;
    DumbProperties dumb_properties;
#endif /* CN_DUMB */
    volatile unsigned int CN_msecs;
    volatile unsigned int CN_ticks;
    unsigned char keyup [KEY_MAX];
    unsigned char keydown [KEY_MAX];
} Allegro4_Backend;

typedef BITMAP CN_Image_Impl;
typedef RGB CN_RGB_Impl;

typedef int CN_SoundByte_Impl;

#ifdef  CN_DUMB
typedef DUH CN_MOD_Impl;
#elif defined CN_JGMOD
typedef JGMOD CN_MOD_Impl;
#else
typedef void* CN_MOD_Impl;
#endif

typedef MIDI CN_MIDI_Impl;
typedef void CN_Music_Impl;
typedef int CN_Key_Impl;

/* TODO: This is a placeholder. */
typedef char CN_MouseButton_Impl;

/*Input defines*/
#define CN_KEY__A  KEY_A
#define CN_KEY__B  KEY_B
#define CN_KEY__C  KEY_C
#define CN_KEY__D  KEY_D
#define CN_KEY__E  KEY_E
#define CN_KEY__F  KEY_F
#define CN_KEY__G  KEY_G
#define CN_KEY__H  KEY_H
#define CN_KEY__I  KEY_I
#define CN_KEY__J  KEY_J
#define CN_KEY__K  KEY_K
#define CN_KEY__L  KEY_L
#define CN_KEY__M  KEY_M
#define CN_KEY__N  KEY_N
#define CN_KEY__O  KEY_O
#define CN_KEY__P  KEY_P
#define CN_KEY__Q  KEY_Q
#define CN_KEY__R  KEY_R
#define CN_KEY__S  KEY_S
#define CN_KEY__T  KEY_T
#define CN_KEY__U  KEY_U
#define CN_KEY__V  KEY_V
#define CN_KEY__W  KEY_W
#define CN_KEY__X  KEY_X
#define CN_KEY__Y  KEY_Y
#define CN_KEY__Z  KEY_Z
#define CN_KEY__ESC  KEY_ESC
#define CN_KEY__TILDE  KEY_TILDE
#define CN_KEY__EQUALS  KEY_EQUALS
#define CN_KEY__BACKSPACE KEY_BACKSPACE
#define CN_KEY__LBRACKET KEY_OPENBRACE
#define CN_KEY__RBRACKET KEY_CLOSEBRACE
#define CN_KEY__ENTER KEY_ENTER
#define CN_KEY__COLON KEY_COLON
#define CN_KEY__QUOTE KEY_QUOTE
#define CN_KEY__BACKSLASH KEY_BACKSLASH
#define CN_KEY__COMMA KEY_COMMA
#define CN_KEY__PERIOD KEY_STOP
#define CN_KEY__SLASH KEY_SLASH
#define CN_KEY__SPACE KEY_SPACE
#define CN_KEY__INSERT KEY_INSERT
#define CN_KEY__DELETE KEY_DEL
#define CN_KEY__HOME KEY_HOME
#define CN_KEY__END KEY_END
#define CN_KEY__PAGEUP KEY_PGUP
#define CN_KEY__PAGEDOWN KEY_PGDN
#define CN_KEY__LEFT KEY_LEFT
#define CN_KEY__RIGHT KEY_RIGHT
#define CN_KEY__UP KEY_UP
#define CN_KEY__DOWN KEY_DOWN
#define CN_KEY__SLASH_PAD KEY_SLASH_PAD
#define CN_KEY__ASTERICK_PAD KEY_ASTERISK
#define CN_KEY__MINUS_PAD KEY_MINUS_PAD
#define CN_KEY__PLUS_PAD KEY_PLUS_PAD
#define CN_KEY__DELETE_PAD KEY_DEL_PAD
#define CN_KEY__ENTER_PAD KEY_ENTER_PAD
#define CN_KEY__PRINTSCREEN KEY_PRTSCR
#define CN_KEY__PAUSE KEY_PAUSE
#define CN_KEY__LSHIFT KEY_LSHIFT
#define CN_KEY__RSHIFT KEY_RSHIFT
#define CN_KEY__LCTRL KEY_LCONTROL
#define CN_KEY__RCTRL KEY_RCONTROL
#define CN_KEY__LALT KEY_ALT
#define CN_KEY__RALT KEY_ALTGR
#define CN_KEY__LSUPER KEY_LWIN
#define CN_KEY__RSUPER KEY_RWIN
#define CN_KEY__MENU KEY_MENU
#define CN_KEY__SCROLLOCK KEY_SCRLOCK
#define CN_KEY__NUMLOCK KEY_NUMLOCK
#define CN_KEY__CAPSLOCK KEY_CAPSLOCK
#define CN_KEY__EQUALS_PAD KEY_EQUALS_PAD
#define CN_KEY__BACKQUOTE KEY_BACKQUOTE
#define CN_KEY__SEMICOLON KEY_SEMICOLON
#define CN_KEY__COMMAND KEY_COMMAND
#define CN_KEY__F1 KEY_F1
#define CN_KEY__F2 KEY_F2
#define CN_KEY__F3 KEY_F3
#define CN_KEY__F4 KEY_F4
#define CN_KEY__F5 KEY_F5
#define CN_KEY__F6 KEY_F6
#define CN_KEY__F7 KEY_F7
#define CN_KEY__F8 KEY_F8
#define CN_KEY__F9 KEY_F9
#define CN_KEY__F10 KEY_F10
#define CN_KEY__F11 KEY_F11
#define CN_KEY__F12 KEY_F12
#define CN_KEY__0_PAD KEY_0_PAD
#define CN_KEY__1_PAD KEY_1_PAD
#define CN_KEY__2_PAD KEY_2_PAD
#define CN_KEY__3_PAD KEY_3_PAD
#define CN_KEY__4_PAD KEY_4_PAD
#define CN_KEY__5_PAD KEY_5_PAD
#define CN_KEY__6_PAD KEY_6_PAD
#define CN_KEY__7_PAD KEY_7_PAD
#define CN_KEY__8_PAD KEY_8_PAD
#define CN_KEY__9_PAD KEY_9_PAD
#define CN_KEY__TAB KEY_TAB

#endif /* CN_ALLEGRO4 */
#endif /*ALLEGRO4_BINDS_*/
