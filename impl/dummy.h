/**
 * @file dummy.h
 * @author Christian Michael Baum
 * @brief Implementation used if compiling with no back-end
 *
 * Refer to cngine.h for API
 * Note: The purpose of this file is to compile.
 * Without a back-end, I/O cannot work and it is likely your program will crash
 * Unit tests are not compiled with dummy.h
 **/
#ifndef WHATEVER_BINDS_H_
#define WHATEVER_BINDS_H_

#ifndef _CN_DOXYGEN_SKIP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../core/chelpers.h"
#include "../core/cnerror.h"
#include "../core/cnmem.h"

#define END_OF_MAIN()

typedef struct WhateverBackend {
    int whatever[4096];
} WhateverBackend;

typedef struct WhateverObj {
    int compare;
    int whatever[256];
} WhateverObj;

typedef struct BigWhatever {
    int whatever[123456];
} BigWhatever;

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
}CN_RGB_Impl;

typedef struct {
    short w;
    short h;
    char pixels[1];
} CN_Image_Impl;

typedef struct {
    char num_channels;
    int sample_rate;
    void *wave;
} CN_SoundByte_Impl;

typedef struct {
    CN_Image_Impl *screen;
    CN_RGB_Impl pal[256];
    CN_MusicType music_type;
} CN_DummyContainer;

extern CN_DummyContainer container;

/* Probably won't implement MIDI */
typedef BigWhatever CN_MIDI_Impl;
/* Modplay? */
typedef BigWhatever CN_MOD_Impl;
typedef CN_MOD_Impl CN_Music_Impl;

/*Backend methods*/
#define CN_Backend_Update CN_NoOp
#define CN_Backend_UpdateFrame CN_NoOp

/*Input macros*/
typedef int CN_Key_Impl;
typedef int CN_MouseButton_Impl;
#define CN_KEY__0 0
#define CN_KEY__1 1
#define CN_KEY__2 2
#define CN_KEY__3 3
#define CN_KEY__4 4
#define CN_KEY__5 5
#define CN_KEY__6 6
#define CN_KEY__7 7
#define CN_KEY__8 8
#define CN_KEY__9 9
#define CN_KEY__A 10
#define CN_KEY__B 11
#define CN_KEY__BACKSLASH 12
#define CN_KEY__BACKSPACE 13
#define CN_KEY__C 14
#define CN_KEY__CAPSLOCK 15
#define CN_KEY__COMMA 16
#define CN_KEY__D 17
#define CN_KEY__DELETE 18
#define CN_KEY__DOWN 19
#define CN_KEY__E 20
#define CN_KEY__END 21
#define CN_KEY__EQUALS 22
#define CN_KEY__ESC 23
#define CN_KEY__ 24
#define CN_KEY__F1  25
#define CN_KEY__F10 26
#define CN_KEY__F11 27
#define CN_KEY__F12 28
#define CN_KEY__F2 29
#define CN_KEY__F3 30
#define CN_KEY__F4 31
#define CN_KEY__F5 32
#define CN_KEY__F6 33
#define CN_KEY__F7 34
#define CN_KEY__F8 35
#define CN_KEY__F9 36
#define CN_KEY__G 37
#define CN_KEY__H 38
#define CN_KEY__HOME 39
#define CN_KEY__I 40
#define CN_KEY__INSERT 41
#define CN_KEY__J 42
#define CN_KEY__K 43
#define CN_KEY__0_PAD 44
#define CN_KEY__1_PAD 45
#define CN_KEY__2_PAD 46
#define CN_KEY__3_PAD 47
#define CN_KEY__4_PAD 48
#define CN_KEY__5_PAD 49
#define CN_KEY__6_PAD 50
#define CN_KEY__7_PAD 51
#define CN_KEY__8_PAD 52
#define CN_KEY__9_PAD 53
#define CN_KEY__ENTER_PAD 54
#define CN_KEY__EQUALS_PAD 55
#define CN_KEY__SLASH_PAD 56
#define CN_KEY__ASTERICK_PAD 57
#define CN_KEY__MINUS_PAD 58
#define CN_KEY__PERIOD_PAD 59
#define CN_KEY__PLUS_PAD 60
#define CN_KEY__L 61
#define CN_KEY__LALT 62
#define CN_KEY__LCTRL 63
#define CN_KEY__LEFT 64
#define CN_KEY__LBRACKET 65
#define CN_KEY__LSUPER 66
#define CN_KEY__LSHIFT 67
#define CN_KEY__M 68
#define CN_KEY__MENU 69
#define CN_KEY__MINUS 70
#define CN_KEY__N 71
#define CN_KEY__O 72
#define CN_KEY__P 73
#define CN_KEY__PAGEDOWN 74
#define CN_KEY__PAGEUP 75
#define CN_KEY__PAUSE 76
#define CN_KEY__PERIOD 77
#define CN_KEY__PRINTSCREEN 78
#define CN_KEY__Q 79
#define CN_KEY__R 80
#define CN_KEY__RALT 81
#define CN_KEY__RCTRL 82
#define CN_KEY__ENTER 83
#define CN_KEY__RSUPER 84
#define CN_KEY__RIGHT 85
#define CN_KEY__RBRACKET 86
#define CN_KEY__RSHIFT 87
#define CN_KEY__S 88
#define CN_KEY__SCROLLOCK 89
#define CN_KEY__SEMICOLON 90
#define CN_KEY__SLASH 91
#define CN_KEY__SPACE 92
#define CN_KEY__T 93
#define CN_KEY__TAB 94
#define CN_KEY__U 95
#define CN_KEY__UP 96
#define CN_KEY__V 97
#define CN_KEY__W 98
#define CN_KEY__X 99
#define CN_KEY__Y 100
#define CN_KEY__Z 101

#endif /* _CN_DOXYGEN_SKIP */

#endif /* WHATEVER_BINDS_H_ */
