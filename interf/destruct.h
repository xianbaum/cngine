/**
 * @file destruct.h
 * @author Christian Michael Baum
 * @brief Destructable interface
 **/

#ifndef _DESTRUCTABLE_H_
#define _DESTRUCTABLE_H_

/*
  not actually an interface even though it's in the interf folder
*/

typedef void (*CN_Destruct)(void *);
struct CN_Destructable{
    CN_Destruct Destruct;
};
typedef struct CN_Destructable CN_Destructable;

/*this should be used as the first base class! - in that case,
  these two defines are unnecessary
  #define CN_ToDestructable(casting) &(casting->destructable)
  #define CN_FromDestructable(type, helloable) \
  ((type*)((char*)helloable - offsetof(type, destructable)))
*/

#endif
