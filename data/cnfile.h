/**
 * @file cnfile.h
 * @author Christian Michael Baum
 * @brief Wrapper / extensions for C's file API
 **/

/**
 * @brief A struct containing file information. Not
 **/
typedef CN_File_Dev CN_File;
