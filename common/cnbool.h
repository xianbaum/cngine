/**
 * @file cnbool.h
 * @author Christian Michael Baum
 * @brief Contains definition and functions for CN_bool
 **/

#ifndef CN_BOOL_H_
#define CN_BOOL_H_

/*defining a bool type*/
#define CN_false 0
#define CN_true 1
typedef char CN_bool;

#endif
