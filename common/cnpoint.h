/**
 * @file cnpoint.h
 * @author Christian Michael Baum
 * @brief Contains definition and functions for CN_Point
 **/

#ifndef CN_POINT_H_
#define CN_POINT_H_

/** @brief A point */


typedef struct CN_Point {
    short x, y;
} CN_Point;

static CN_inline CN_Point_Equals(CN_Point a, CN_Point b)
{
    return a.x == b.x && a.y == b.y;
}

#endif
