/**
 * @file cngine.h
 * @author Christian Michael Baum
 * @brief Contains definition and functions for CN_Rect
 **/

#ifndef CN_RECT_H_
#define CN_RECT_H_

#include "inline.h"
#include "cnbool.h"

/** @brief A rectangle */
typedef struct {
    short x; /**< Rectangle X position */
    short y; /**< Rectangle Y position */
    short w; /**< Rectangle width */
    short h; /**< Rectangle height */
} CN_Rect;

/**
 * @brief Checks whether an x and y collides with a rect
 *
 * @param rect The rectangle
 * @param x X position of the point
 * @param y Y position of the point
 * @returns true if the point intersects with the rect
 **/
static CN_inline CN_bool CN_Rect_PointCollision(CN_Rect rect, short x,short  y)
{
    return x > rect.x &&
        x < rect.x + rect.w &&
        y > rect.y &&
        y < rect.y + rect.h;
}

static CN_inline CN_bool CN_Rect_Equals(CN_Rect a, CN_Rect b) {
    return a.x == b.x &&
        a.y == b.y &&
        a.w == b.w &&
        a.h == b.h;
}

#endif
