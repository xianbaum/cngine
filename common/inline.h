/**
 * @file cngine.h
 * @author Christian Michael Baum
 * @brief Attempts to create a portable inline
 **/

#ifndef CN_INLINE_H_
#define CN_INLINE_H_

#if __STDC_VERSION__ >= 199901L
#define CN_inline inline
#elif defined __GNUC__
#define CN_inline __inline__
#elif defined _MSC_VER
#define CN_inline __inline
#elif defined __clang__
#define CN_inline inline
#else
#define CN_inline
#define CN_NO_INLINE
#endif

#endif
