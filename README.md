# CNgine
CNgine is a C engine made specifically for making retro styled games. Note that CNgine's API is not yet stable and is subject to change at any time. Also, many functions are not yet implemented across all implementations.

# Usage
The easiest way to use CNgine is as a git submodule.

You will need one of the following:

* Allegro4
* SDL 1.2, SDL 1.2 Image, SDL 1.2 Mixer
* SDL 2.0, SDL 2.0 Image, SDL 2.0 Mixer
* Playstation support with PSXGNU is being considered but does not work yet.

You will also need to compile each source file with the following CFLAGS corresponding to the libraries above:

* CN_ALLEGRO4
* CN_SDL_1_2
* CN_SDL_2
* CN_PSX

Otherwise, it will use dummy.h which does nothing and returns wrong data and will likely crash your program.

An optional dependency is Lua 5.2. Lua is enabled by default. To disable 
Lua, add the CFLAG
* CN_NO_LUA

# Other CFLAGS

## Allegro 4

* CN_DUMB - Use DUMB to play MOD Files
* CN_JGMOD - Use JGMOD to play MOD files
* If none of the above are enabled, MOD files will not play.

## SDL 1.2

* CN_EMCC - Slight modifications to allow compilation with emscripten's SDL implementation
* CN_NO_IMAGE - Do not compile with  SDL_Image (images must be BMP)
* CN_NO_MIXER - Do not compile with SDL_Mixer (No MOD support)
* CN_MIKMOD_HACK - Uses a hack to enable repeats on MOD files (Requires you to additionally have mikmod.h)
* CN_MODPLUG_HACK - Uses a hack to enable repeats on MOD files (Requires you to additionally have modplug.h)
* If you have neither CN_MIKMOD_HACK nor CN_MODPLUG_HACK, MOD files will not repeat

## SDL 2

* CN_MIKMOD_HACK - Uses a hack to enable repeats on MOD files (Requires you to additionally have mikmod.h)
* CN_MODPLUG_HACK - Uses a hack to enable repeats on MOD files (Requires you to additionally have modplug.h)
* CN_USE_OPENMPT_INSTEAD - Uses libopenmpt instead of SDL_Mixer (does not work correctly yet)
* If you have neither CN_MIKMOD_HACK, CN_MODPLUG_HACK nor CN_USE_OPENMPT_INSTEAD, MOD files will not repeat
* CN_NO_IMAGE - Do not compile with SDL_Image (images must be BMP)
* CN_NO_MIXER - Do not compile with SDL_Mixer (No MOD support)

## Others

* I am working on a RetroArch port.
* I am working on a PSX port with PSXGNU
* DOS support is outdated and in need of an update.

# Projects using CNgine

* [MELODY](https://gitlab.com/xianbaum/melody)
* [Micropolis CNgine port](https://gitlab.com/xianbaum/micropolis-port)
* City Night (A retro style video game, currently private, source code available upon request)
