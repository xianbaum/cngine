#ifndef CN_LUAS_H_
#define CN_LUAS_H_

#ifndef CN_NO_LUA

#include "../common/cnbool.h"

void CN_Lua_RegisterCommands();
void CN_Lua_OpenConsole();
void CN_LuaMode_Update();
CN_bool CN_lua_keydown;

#else /* CN_NO_LUA */

#define CN_Lua_RegisterCommands()
#define CN_Lua_OpenConsole()

#endif /* CN_NO_LUA */

#endif /* CN_LUAS_H_ */
