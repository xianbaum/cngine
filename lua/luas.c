#ifndef CN_NO_LUA

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <string.h>

#include <stdlib.h>
#include "../cngine.h"

static const int FONT_HEIGHT = 12;
static const int FONT_WIDTH = 12;
static const int CONSOLE_HEIGHT = 80;
static const int CONSOLE_WIDTH = 255;
static const int INPUT_BUFFER_SIZE = 255;
char CN_lua_console_buffer[20400];
static luaL_Buffer buffer;
static char* CN_lua_console_buffer_space;
static int command_start_pos = 0;
static int current_pos = 0;
lua_State * state = NULL;
CN_bool CN_lua_keydown = CN_false;
/* lua registers */

void CN_LuaMode_Update()
{
    char key = CN_CharKeyDown();
    if(!CN_lua_keydown && CN_IsKeyDown(CN_KEY_BACKSPACE))
    {
        CN_lua_keydown = CN_true;
        if(current_pos > 0 && current_pos > command_start_pos)
        {
            current_pos--;
            CN_lua_console_buffer[current_pos] = '\0';
        }
    }
    else if(key)
    {
        CN_lua_console_buffer[current_pos] = key;
        current_pos++;
        CN_lua_console_buffer[current_pos] = '\0';
    }
    else if(!CN_lua_keydown && (CN_IsKeyDown(CN_KEY_ENTER) ||
								CN_IsKeyDown(CN_KEY_ENTER_PAD)))
    {
        CN_lua_keydown = CN_true;
        if(luaL_dostring(state, CN_lua_console_buffer+command_start_pos))
        {
            CN_LogInfo(("ERROR OR Uh"));
        }
        CN_lua_console_buffer[current_pos] = '\n';
        current_pos++;
        command_start_pos = current_pos;
    }
    else if(!CN_lua_keydown && CN_IsKeyDown(CN_KEY_LCTRL))
    {
        CN_lua_keydown = CN_false;
    }
    else if(!CN_IsKeyDown(CN_KEY_LCTRL) && !CN_IsKeyDown(CN_KEY_ENTER) &&
            !CN_IsKeyDown(CN_KEY_ENTER_PAD) &&
            !CN_IsKeyDown(CN_KEY_BACKSPACE) &&
            !CN_IsKeyDown(CN_KEY_TAB))
    {
        CN_lua_keydown = CN_false;
    }
}

static void *CN_Lua_allocator (void *ud, void *ptr, size_t osize, size_t nsize)
{
    (void)ud;  (void)osize;  /* not used */
    if (nsize == 0) {
        free(ptr);
        return NULL;
    }
    else
    {
        return realloc(ptr, nsize);
    }
}

#define CN__lua_DefineStaticLogger(type)			\
    static int CN_Lua_Log##type(lua_State *L)		\
    {												\
        int nargs = lua_gettop(L);					\
        int i;										\
        for(i = 1; i <= nargs; i++)					\
        {											\
            if(lua_isstring(L, i))					\
            {										\
                CN_Log##type((lua_tostring(L, i)));	\
            }										\
        }											\
        return 0;									\
    }
CN__lua_DefineStaticLogger(Critical)
CN__lua_DefineStaticLogger(Error)
CN__lua_DefineStaticLogger(Warning)
CN__lua_DefineStaticLogger(Info)
CN__lua_DefineStaticLogger(Debug)
CN__lua_DefineStaticLogger(Trace)
#undef CN__lua_StaticLogger

static const struct luaL_Reg print_function [] =
{
    {"CN_LogCritical", CN_Lua_LogCritical},
    {"CN_LogError", CN_Lua_LogError},
    {"CN_LogWarning", CN_Lua_LogWarning},
    {"CN_LogInfo", CN_Lua_LogInfo},
    {"CN_LogDebug", CN_Lua_LogDebug},
    {"CN_LogTrace", CN_Lua_LogTrace},
    {NULL, NULL} /* end of array */
};

void CN_Lua_RegisterCommands()
{
	state = lua_newstate(CN_Lua_allocator, NULL);
	luaL_openlibs(state);
}

void CN_Lua_OpenConsole()
{
	command_start_pos = 0;
	current_pos = 0;
	CN_lua_console_buffer[0] = '\0';
	if(state == NULL)
	{
		state = lua_newstate(CN_Lua_allocator, NULL);
		
		CN_lua_console_buffer_space = luaL_buffinitsize(
			state, CN_lua_console_buffer, CONSOLE_WIDTH*CONSOLE_HEIGHT);

	}
}

#else
void CN_Lua_OpenConsole()
{

}
void CN_Lua_RegisterCommands()
{

}
#endif
